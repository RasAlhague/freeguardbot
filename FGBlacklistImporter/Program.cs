﻿using FreeGuardBotLib;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FGBlacklistImporter
{
    /// <summary>
    /// The entry class into the program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The entry point into the program. 
        /// </summary>
        /// <param name="blacklistCsvPath">The path to the csv file.</param>
        /// <param name="guildId">The discord guild id.</param>
        /// <param name="guildName">The name of the guild.</param>
        public static void Main(string blacklistCsvPath, ulong guildId, string guildName)
        {
            if (!File.Exists(blacklistCsvPath))
            {
                Console.WriteLine("Could not find blacklist csv file!");

                return;
            }

            Console.WriteLine("Reading csv file...");
            var csvFile = File.ReadAllLines(blacklistCsvPath);

            var entries = new List<CsvEntry>();

            foreach (var line in csvFile)
            {
                entries.Add(new CsvEntry(line));
            }

            Console.WriteLine("Writing guild...");

            using FreeGuardDbContext dbContext = new();
            int gId = InsertGuildIntoDb(dbContext, guildId, guildName);

            Console.WriteLine("Writing users...");

            foreach (var entry in entries)
            {
                entry.InsertUserIntoDb(dbContext, gId);
            }

            Console.WriteLine("Writing entries...");

            foreach (var entry in entries)
            {
                entry.InsertBlacklistIntoDb(dbContext, gId);
            }

            Console.WriteLine("Finished...");
        }

        private static int InsertGuildIntoDb(FreeGuardDbContext dbContext, ulong guildId, string guildName)
        {
            var guild = dbContext.Guilds.SingleOrDefault(x => x.GuildId == guildId);

            if (guild is null)
            {
                guild = new Guild()
                {
                    GuildId = guildId,
                    InitialName = guildName,
                    CreateDate = DateTime.UtcNow,
                };

                dbContext.Guilds.Add(guild);
                dbContext.SaveChanges();
            }

            return guild.Id;
        }
    }
}
