﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands;

[SlashCommandGroup("Test", "Test")]
public class ChannelHelperAppCommand : LogableApplicationCommandModule
{
    private const ulong EVENTCHANNELID = 822924953057624094;
    private const ulong FREEGUARDROLEID = 850065546502471740;
    private const ulong GUESTROLEID = 932695627740102666;
    private const ulong DOYADMINROLEID = 756262664979742800;
    private const ulong LOGCHANNELID = 756910381812678757;

    public ChannelHelperAppCommand(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    [CheckUserAccessContextMenu(Rights.ManageBot)]
    [ContextMenu(ApplicationCommandType.UserContextMenu, "Hide event chat")]
    public async Task EventChatHideAsync(ContextMenuContext ctx)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);

        var eventChannel = ctx.Guild.GetChannel(EVENTCHANNELID);
        await DenyRoleAccess(ctx, eventChannel, FREEGUARDROLEID);
        await DenyRoleAccess(ctx, eventChannel, GUESTROLEID);
        await DenyRoleAccess(ctx, eventChannel, DOYADMINROLEID);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Channel has been hidden by '{ctx.User.Username}'."));
        await ctx.DeleteResponseAsync();

        var logChannel = ctx.Guild.GetChannel(LOGCHANNELID);
        await logChannel.SendMessageAsync($"Channel has been hidden by '{ctx.User.Username}'.");
    }

    [CheckUserAccessContextMenu(Rights.ManageBot)]
    [ContextMenu(ApplicationCommandType.UserContextMenu, "Show event chat")]
    public async Task EventChatShowAsync(ContextMenuContext ctx)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);

        var eventChannel = ctx.Guild.GetChannel(EVENTCHANNELID);
        await AllowRoleAccess(ctx, eventChannel, FREEGUARDROLEID);
        await AllowRoleAccess(ctx, eventChannel, GUESTROLEID);
        await AllowRoleAccess(ctx, eventChannel, DOYADMINROLEID);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Channel has been shown by '{ctx.User.Username}'."));
        await ctx.DeleteResponseAsync();

        var logChannel = ctx.Guild.GetChannel(LOGCHANNELID);
        await logChannel.SendMessageAsync($"Channel has been shown by '{ctx.User.Username}'.");
    }

    [CheckUserAccessContextMenu(Rights.ManageBot)]
    [ContextMenu(ApplicationCommandType.UserContextMenu, "Hide current chat")]
    public async Task HideChatAsync(ContextMenuContext ctx)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);

        await DenyRoleAccess(ctx, ctx.Channel, FREEGUARDROLEID);
        await DenyRoleAccess(ctx, ctx.Channel, GUESTROLEID);
        await DenyRoleAccess(ctx, ctx.Channel, DOYADMINROLEID);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Channel has been hidden by '{ctx.User.Username}'."));
        await ctx.DeleteResponseAsync();

        var logChannel = ctx.Guild.GetChannel(LOGCHANNELID);
        await logChannel.SendMessageAsync($"Channel has been hidden by '{ctx.User.Username}'.");
    }

    [CheckUserAccessContextMenu(Rights.ManageBot)]
    [ContextMenu(ApplicationCommandType.UserContextMenu, "Show current chat")]
    public async Task ShowChatAsync(ContextMenuContext ctx)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        
        await AllowRoleAccess(ctx, ctx.Channel, FREEGUARDROLEID);
        await AllowRoleAccess(ctx, ctx.Channel, GUESTROLEID);
        await AllowRoleAccess(ctx, ctx.Channel, DOYADMINROLEID);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Channel has been shown by '{ctx.User.Username}'."));
        await ctx.DeleteResponseAsync();

        var logChannel = ctx.Guild.GetChannel(LOGCHANNELID);
        await logChannel.SendMessageAsync($"Channel has been shown by '{ctx.User.Username}'.");
    }

    private async Task AllowRoleAccess(ContextMenuContext ctx, DSharpPlus.Entities.DiscordChannel channel, ulong roleId)
    {
        try
        {
            var role = ctx.Guild.GetRole(roleId);
            await channel.AddOverwriteAsync(role, Permissions.AccessChannels, Permissions.None);
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Error while trying to allow access to channel.");
        }
    }

    private async Task DenyRoleAccess(ContextMenuContext ctx, DSharpPlus.Entities.DiscordChannel eventChannel, ulong roleId)
    {
        try
        {
            var role = ctx.Guild.GetRole(roleId);
            await eventChannel.AddOverwriteAsync(role, Permissions.None, Permissions.AccessChannels);
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Error while trying to deny access to channel.");
        }
    }
}

