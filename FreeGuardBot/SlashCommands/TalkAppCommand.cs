﻿using DSharpPlus.Entities;
using DSharpPlus;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Services;
using System;
using System.Threading.Tasks;
using FreeGuardBot.Services.TextGeneration;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using System.Collections;
using System.Text;

namespace FreeGuardBot.SlashCommands;

[GuildOnly]
[SlashCommandGroup("talk", "Commands for interactivity with bot via text.")]
public class TalkAppCommand : LogableApplicationCommandModule
{
    private readonly ITextGenService _textGenService;

    public TalkAppCommand(ICommandLogService commandLogService, ITextGenService textGenService) : base(commandLogService)
    {
        _textGenService = textGenService;
    }

    [SlashCommand("version", "Gets the api version for the talking ai stuff.")]
    public async Task GetApiVersionAsync(InteractionContext ctx)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "version", "version");

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        try
        {
            string version = await _textGenService.GetVersionAsync();
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Api version: {version}"));
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Failed to get version of api.");
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Failed to connect to api!"));
        }
        finally
        {
            stopwatch.Stop();
            ctx.Client.Logger.LogInformation($"Getting version took {stopwatch.ElapsedMilliseconds / 1000} seconds.");
        }
    }

    [SlashCommand("story", "Generates text based on input.")]
    public async Task GenerateTextAsync(
        InteractionContext ctx,
        [Option("max-length", "Max length in chars of result text.")] long maxLength,
        [Option("situation", "The situation the story is in.")] string prefix, 
        [Option("text", "A text with which the bot can start its text generation.")] string text)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "version", "version");

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        TextGenRequest req = new(maxLength, prefix);
        req.AddText(text);

        try
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Generating text, this can take a bit."));
            TextGenResponse resp = await _textGenService.GenerateTextAsync(req);

            StringBuilder sb = new();

            foreach (var sentence in resp.Texts)
            {
                sb.AppendLine(sentence);
            }

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Your response text:{Environment.NewLine}{sb.ToString()}"));
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Failed to get generated text from api.");
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Failed to connect to api!"));
        }
        finally
        {
            stopwatch.Stop();
            ctx.Client.Logger.LogInformation($"Generating text took {stopwatch.ElapsedMilliseconds / 1000} seconds.");
        }
    }
}
