﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Builders;
using FreeGuardBot.Services;
using FreeGuardBotLib;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands;

public readonly record struct UserLeaderboardInfo(ulong userId, int leaderboardId, int position, long points);

/// <summary>
/// 
/// </summary>
[SlashCommandGroup("leaderboard", "Commands for leaderboard interaction.")]
public class LeaderboardAppCommand : LogableApplicationCommandModule
{
    public readonly ILeaderboardService _leaderboardService;
    public readonly ILeaderboardActionService _leaderboardActionService;
    public readonly ILeaderboardUpdateService _leaderboardUpdateService;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="leaderboardService"></param>
    /// <param name="leaderboardActionService"></param>
    /// <param name="commandLogService"></param>
    public LeaderboardAppCommand(ILeaderboardService leaderboardService, ILeaderboardActionService leaderboardActionService, ILeaderboardUpdateService leaderboardUpdateService, ICommandLogService commandLogService) : base(commandLogService)
    {
        _leaderboardService = leaderboardService;
        _leaderboardActionService = leaderboardActionService;
        _leaderboardUpdateService = leaderboardUpdateService;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ctx"></param>
    /// <param name="user"></param>
    /// <param name="amount"></param>
    /// <param name="reason"></param>
    /// <returns></returns>
    [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.ManageLeaderboard)]
    [SlashCommand("grant", "Grants a user a amount of points for the leaderboard.")]
    public async Task GrantPointsAsync(InteractionContext ctx,
        [Option("user", "The user who receives the points.")] DiscordUser user,
        [Option("amount", "The amount of points a user will receive.")] long amount,
        [Option("reason", "The reason for giving points to the user.")] string reason)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.LeaderboardLoggingChannel, "grant", $"user:{user.Username}#{user.Discriminator};amount:{amount};reason:{reason}");

        using FreeGuardDbContext dbContext = new();

        try
        {
            await _leaderboardActionService.GrantPointsAsync(dbContext, ctx.Guild.Id, user.Id, amount, reason, ctx.Member.Id);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{ctx.Member.Mention} granted {user.Mention} {amount} points for: `{reason}`"));
        }
        catch (ArgumentOutOfRangeException)
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Cannot grant a negativ amount of points, use the revoke command for decreasing points."));
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Could not grant points to user.");

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Could not give points to user because of an unknown error."));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ctx"></param>
    /// <param name="amount"></param>
    /// <param name="reason"></param>
    /// <param name="user1"></param>
    /// <param name="user2"></param>
    /// <param name="user3"></param>
    /// <param name="user4"></param>
    /// <param name="user5"></param>
    /// <param name="user6"></param>
    /// <param name="user7"></param>
    /// <param name="user8"></param>
    /// <returns></returns>
    [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.ManageLeaderboard)]
    [SlashCommand("grant-group", "Grants a user a amount of points for the leaderboard.")]
    public async Task GrantGroupPointsAsync(InteractionContext ctx,
        [Option("amount", "The amount of points a user will receive.")] long amount,
        [Option("reason", "The reason for giving points to the user.")] string reason,
        [Option("user1", "The user who receives the points.")] DiscordUser user1,
        [Option("user2", "The user who receives the points.")] DiscordUser user2 = null,
        [Option("user3", "The user who receives the points.")] DiscordUser user3 = null,
        [Option("user4", "The user who receives the points.")] DiscordUser user4 = null,
        [Option("user5", "The user who receives the points.")] DiscordUser user5 = null,
        [Option("user6", "The user who receives the points.")] DiscordUser user6 = null,
        [Option("user7", "The user who receives the points.")] DiscordUser user7 = null,
        [Option("user8", "The user who receives the points.")] DiscordUser user8 = null
        )
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.LeaderboardLoggingChannel, "grant-group", $"user1:{user1?.Username}#{user1?.Discriminator};user2:{user2?.Username}#{user2?.Discriminator};user3:{user3?.Username}#{user3?.Discriminator};user4:{user4?.Username}#{user4?.Discriminator};user5:{user5?.Username}#{user5?.Discriminator};user6:{user6?.Username}#{user6?.Discriminator};user7:{user7?.Username}#{user7?.Discriminator};user8:{user8?.Username}#{user8?.Discriminator};amount:{amount};reason:{reason}");

        List<DiscordUser> users = new List<DiscordUser>();

        AddUserIfNotNull(user1, users);
        AddUserIfNotNull(user2, users);
        AddUserIfNotNull(user3, users);
        AddUserIfNotNull(user4, users);
        AddUserIfNotNull(user5, users);
        AddUserIfNotNull(user6, users);
        AddUserIfNotNull(user7, users);
        AddUserIfNotNull(user8, users);

        using FreeGuardDbContext dbContext = new();

        try
        {
            await _leaderboardActionService.GroupGrantPointsAsync(dbContext, ctx.Guild.Id, users.Select(x => x.Id), amount, reason, ctx.Member.Id);

            foreach (var user in users)
            {
                await ctx.Channel.SendMessageAsync($"{ctx.Member.Mention} granted {user.Mention} {amount} points for: `{reason}`");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{ctx.Member.Mention} granted the following guards points: "));
            }
        }
        catch (ArgumentOutOfRangeException)
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Cannot grant a negativ amount of points, use the revoke command for decreasing points."));
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Could not grant points to user.");

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Could not give points to user because of an unknown error."));
        }

        void AddUserIfNotNull(DiscordUser user, List<DiscordUser> users)
        {
            if (user != null)
            {
                users.Add(user);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ctx"></param>
    /// <param name="user"></param>
    /// <param name="amount"></param>
    /// <param name="reason"></param>
    /// <returns></returns>
    [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.ManageLeaderboard)]
    [SlashCommand("revoke", "Revokes a user a amount of points for the leaderboard.")]
    public async Task RevokePointsAsync(InteractionContext ctx,
        [Option("user", "The user who looses the points.")] DiscordUser user,
        [Option("amount", "The amount of points a user will loose.")] long amount,
        [Option("reason", "The reason for removing points from the user.")] string reason)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.LeaderboardLoggingChannel, "revoke", $"user:{user.Username}#{user.Discriminator};amount:{amount};reason:{reason}");

        using FreeGuardDbContext dbContext = new();

        try
        {
            await _leaderboardActionService.RevokePointsAsync(dbContext, ctx.Guild.Id, user.Id, amount, reason, ctx.Member.Id);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{ctx.Member.Mention} revoked {user.Mention} {amount} points for: `{reason}`"));
        }
        catch (ArgumentOutOfRangeException)
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Cannot grant a negativ amount of points, use the revoke command for decreasing points."));
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Could not grant points to user.");

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Could not give points to user because of an unknown error."));
        }
    }


    [CheckUserAccessSlashCommand(Rights.ManageBot)]
    [SlashCommand("create", "Creates a new leaderboard.")]
    public async Task CreateLeaderboardAsync(InteractionContext ctx,
        [Option("name", "Name of the leadboard.")] string name,
        [Option("channel", "The channel where the leaderboard will be found.")] DiscordChannel channel,
        [Option("order-direction", "The direction in which the leaderboard should be ordered.")] FreeGuardBotLib.OrderDirection direction,
        [Option("order-field", "The field by which it should order")] OrderField orderField,
        [Option("limit", "Max displayed results.")] long limit,
        [Option("max-age", "Max age of the in days of the action.")] long? maxAgeInDays = null,
        [Option("max-months-ago", "Amount of months ago a leaderboard show messages from.")] long? maxMonthsAgo = null
        )
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.LeaderboardLoggingChannel, "create", $"leaderboardName:{name};channel:{channel.Name}");

        using FreeGuardDbContext dbContext = new();

        Guild guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var leaderboard = new Leaderboard()
        {
            Name = name,
            DiscordChannelId = channel.Id,
            CreateDate = DateTime.UtcNow,
            CreateUserId = ctx.Member.Id,
            OrderDirection = direction,
            OrderField = orderField,
            Limit = (int)limit,
            MaxAge = maxAgeInDays,
            MaxMonthsInPast = (int)maxMonthsAgo,
            GuildId = guild.Id
        };

        try
        {
            var embed = await _leaderboardUpdateService.CreateLeaderboardEmbedAsync(ctx.Client, leaderboard, Enumerable.Empty<GroupedLeaderboardAction>(), 10);
            var message = await channel.SendMessageAsync(embed);

            leaderboard.MessageId = message.Id;

            await _leaderboardService.CreateLeaderboardAsync(dbContext, leaderboard);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"The leaderboard has been created in '{channel.Mention}'."));
        }
        catch (Exception ex)
        {
            ctx.Client.Logger.LogError(ex, "Leaderboard creation failed.");
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Leaderboard creation failed."));
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ctx"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [CheckUserAccessSlashCommand(Rights.ManageBot)]
    [SlashCommand("delete", "Deletes a leaderboard.")]
    public async Task DeleteLeaderboardAsync(InteractionContext ctx,
        [Option("leaderboard-id", "The id of the leaderboard to delete.")] long id)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.LeaderboardLoggingChannel, "delete", $"lb-id:{id}");

        using FreeGuardDbContext dbContext = new();

        var leaderboard = await _leaderboardService.GetLeaderboardAsync(dbContext, ctx.Guild.Id, (int)id);

        var channel = ctx.Guild.GetChannel(leaderboard.DiscordChannelId);
        var message = await channel.GetMessageAsync(leaderboard.MessageId);
        await channel.DeleteMessageAsync(message, "leaderboard deletions");

        await _leaderboardService.DeleteLeaderboardAsync(dbContext, ctx.Guild.Id, (int)id);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"The leaderboard `{leaderboard.Name}` has been removed."));
    }

    [SlashCommand("show-position", "Shows the positions of a specific user on all leaderboards.")]
    public async Task ShowOwnPositions(InteractionContext ctx,
        [Option("user", "The user whose position you wanna see.")] DiscordUser user)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.LeaderboardLoggingChannel, "user", $"user:{user.Username}#{user.Discriminator}");

        using FreeGuardDbContext dbContext = new();

        var guildId = ctx.Guild.Id;

        var guild = await Guild.GetAsync(dbContext, guildId);
        var leaderboards = await _leaderboardService.GetLeaderboardsAsync(dbContext, guildId);
        var leaderboardActions = dbContext.LeaderboardActions.Where(x => x.GuildId == guild.Id);

        List<UserLeaderboardInfo> infos = new();

        foreach (var leaderboard in leaderboards)
        {
            var groupedActions = leaderboard.CreateBuilder()
                .AddActions(leaderboardActions)
                .SetLimitOfMessages(10000)
                .Build()
                .ToList();

            for (int indexOfUser = 0; indexOfUser < groupedActions.Count; indexOfUser++)
            {
                var tmpItem = groupedActions[indexOfUser];

                if (tmpItem.affectedUser == user.Id)
                {
                    infos.Add(new UserLeaderboardInfo(user.Id, leaderboard.Id, indexOfUser + 1, tmpItem.sumOfPoints));

                    break;
                }
            }
        }

        var embed = CreateUserLeaderboardInfo(user, infos, leaderboards);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
    }

    private DiscordEmbed CreateUserLeaderboardInfo(DiscordUser user, IEnumerable<UserLeaderboardInfo> userLeaderboardInfos, IEnumerable<Leaderboard> leaderboards)
    {

        var embedBuilder = new DiscordEmbedBuilder()
            .WithTitle($"Leaderboard info for: {user.Username}#{user.Discriminator}")
            .WithDescription("This displays all the information about all leaderboards one user can be found on.")
            .WithAuthor(user.Username, iconUrl: user.AvatarUrl);

        if (!userLeaderboardInfos.Any())
        {
            embedBuilder.AddField("User has no leaderboard info yet", "\u200b");
        }
        else
        {
            foreach (var userInfo in userLeaderboardInfos)
            {
                var lbName = leaderboards.Where(x => x.Id == userInfo.leaderboardId).Select(x => x.Name).FirstOrDefault();

                embedBuilder.AddField($"Leaderboard: {lbName}", "\u200b")
                    .AddField("Position", userInfo.position.ToString(), inline: true)
                    .AddField("Points", userInfo.points.ToString(), inline: true);
            }
        }

        embedBuilder.WithFooter($"Created: {DateTime.UtcNow} (UTC)");

        return embedBuilder.Build();
    }
}
