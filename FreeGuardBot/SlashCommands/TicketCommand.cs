﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    [SlashCommandGroup("ticket", "Commands for ticket creation and management.")]
    public class TicketCommand : LogableApplicationCommandModule
    {
        private readonly ITicketService _ticketService;

        public TicketCommand(ITicketService ticketService, ICommandLogService commandLogService) : base(commandLogService)
        {
            _ticketService = ticketService;
        }

        [SlashCommand("create", "Creates a new ticket for the requesting person.")]
        public async Task Create(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().WithContent("Your ticket is going to be created, this message will be deleted when it finished creation!"));
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "create");
            await SetBotUserIfNotExistsAsync(ctx);

            try
            {
                DiscordChannel channel = await _ticketService.CreateTicketAsync(ctx.Guild, ctx.Member);

                using FreeGuardDbContext dbContext = new FreeGuardDbContext();
                var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
                var ticketManagers = await dbContext.Roles
                    .Where(x => x.IsTicketManager && x.GuildId == guild.Id)
                    .ToListAsync();

                foreach (var ticketManager in ticketManagers)
                {
                    var role = ctx.Guild.GetRole(ticketManager.DiscordRoleId);
                    await channel.SendMessageAsync($"{role.Mention}, a new ticket is started.");
                }
                await channel.SendMessageAsync($"{ctx.Member.Mention}, this is your ticket, if you need additional people in this ticket you can invite them with `/ticket invite <user>`.");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Channel creation error");
            }

            await Task.Delay(3000);
            await ctx.DeleteResponseAsync();
        }

        [SlashCommand("invite", "Invites more people to a ticket.")]
        public async Task Invite(InteractionContext ctx, 
            [Option("user", "User to invite!")] DiscordUser user, 
            [Option("TicketId", "The id of the ticket you want to invite people to.")] long ticketId)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().WithContent("Trying to invite person to ticket!"));
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "invite", $"user:{user.Username}#{user.Discriminator};ticketId:{ticketId}");

            using FreeGuardDbContext dbContext = new FreeGuardDbContext();
            Guild guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
            var ticket = await dbContext.Tickets.FirstOrDefaultAsync(x => x.Id == ticketId && x.GuildId == guild.Id);

            if (ticket is null)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("No ticket for this id!"));
                return;
            }

            var ticketManagers = await dbContext.Roles
                .Where(x => x.GuildId == guild.Id && x.IsTicketManager)
                .ToListAsync();

            if (!ctx.Member.Roles.Any(x => ticketManagers.Any(y => x.Id == y.DiscordRoleId)) && ctx.Member.Id != ticket.CreateUserId)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You have no right to invite people to this ticket!"));
                return;
            }

            if (ticket.IsArchived)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The ticket is already closed!"));
                return;
            }

            await _ticketService.InviteToTicketAsync(ctx.Guild, user as DiscordMember, (int)ticketId);
            DiscordChannel channel = ctx.Guild.GetChannel(ticket.TicketChannelId);

            await channel.SendMessageAsync($"{user.Mention}, you have been invited to this ticket channel.");
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{user.Username} has been added to the ticket!"));
        }

        [SlashCommand("close", "Closes the current ticket and removes non ticket managers from accessing it.")]
        public async Task Close(InteractionContext ctx, [Option("ticketId", "The id of the ticket you want to invite people to.")] long ticketId)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().WithContent("The ticket will be closed soon."));
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "close", $"ticketId:{ticketId}");

            using FreeGuardDbContext dbContext = new FreeGuardDbContext();
            Guild guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
            var ticket = await dbContext.Tickets.FirstOrDefaultAsync(x => x.Id == ticketId && x.GuildId == guild.Id);

            if (ticket is null)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("No ticket for this id!"));
                return;
            }

            var ticketManagers = await dbContext.Roles
                .Where(x => x.GuildId == guild.Id && x.IsTicketManager)
                .ToListAsync();

            if (!ctx.Member.Roles.Any(x => ticketManagers.Any(y => x.Id == y.DiscordRoleId)) && ctx.Member.Id != ticket.CreateUserId)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You have no right to invite people to this ticket!"));
                return;
            }

            if (ticket.IsArchived)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The ticket is already closed!"));
                return;
            }

            DiscordChannel ticketChannel = ctx.Guild.GetChannel(ticket.TicketChannelId);

            await _ticketService.ArchiveTicketAsync(ctx.Guild, ctx.Member, ticketChannel);
            await _ticketService.CloseTicketAsync(ctx.Guild, ctx.Member, ticketChannel);
            ticket = await dbContext.Tickets.FindAsync(ticket.Id);
            await SendClosedTicketDmInfoAsync(ctx, ticket);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The ticket has been closed!"));
        }

        private async Task SendClosedTicketDmInfoAsync(InteractionContext ctx, Ticket ticket)
        {
            DiscordMember ticketOwner = await ctx.Guild.GetMemberAsync(ticket.CreateUserId);
            DiscordMember ticketCloser = ctx.Member;
            DiscordChannel dmChannel = await ticketOwner.CreateDmChannelAsync();

            await dmChannel.SendMessageAsync($"Your ticket has been closed by {ticketCloser.Username}. In total it contained `{ticket.Messages}` messages.");
        }

        private async Task SetBotUserIfNotExistsAsync(InteractionContext ctx)
        {
            if (_ticketService.BotUser is null)
            {
                _ticketService.BotUser = await ctx.Guild.GetMemberAsync(ctx.Client.CurrentUser.Id);
            }
        }
    }
}
