﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    public class MiscAppModule : LogableApplicationCommandModule
    {
        public MiscAppModule(ICommandLogService commandLogService) : base(commandLogService)
        {
        }

        [SlashCommand("channelreaction", "Exports channel reactions to a csv file.")]
        public async Task GetReactionsToMessage(InteractionContext ctx,
            [Option("channel", "The channel from which the message is.")] DiscordChannel channel,
            [Option("messageid", "The id of the message you want to get reaction from.")] string messageId,
            [Option("title", "The title of the output file.")] string fileTitle)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "channelreaction", $"channel:{channel.Name};messageId:{messageId};fileTitle:{fileTitle}");

            var message = await channel.GetMessageAsync(ulong.Parse(messageId));
            var reactions = message.Reactions;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("emoji;count");

            foreach (var reaction in reactions)
            {
                sb.Append(reaction.Emoji.GetDiscordName());
                sb.Append(";");
                sb.AppendLine(reaction.Count.ToString());
            }

            string filename = $"./{fileTitle}.csv";
            await File.WriteAllTextAsync(filename, sb.ToString());
            using var fs = File.OpenRead(filename);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddFile($"{fileTitle}.csv", fs));
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("say", "Lets the bot say a message into a channel.")]
        public async Task SayAsync(InteractionContext ctx, 
            [Option("channel", "The channel were to send the message to.")] DiscordChannel channel,
            [Option("message", "The message walter should say.")] string message)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "say", $"Channel:{channel.Name};Message:{message};");

            try
            {
                await channel.SendMessageAsync(message);
                await ctx.DeleteResponseAsync();
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while trying to send message");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Failed to send message!"));
            }            

        }

    }

}
