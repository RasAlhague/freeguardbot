﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{

    [SlashCommandGroup("management", "Commands for the management of the bot.")]
    public class ManagementAppCommand : LogableApplicationCommandModule
    {
        private readonly IManagementService _managementService;

        public ManagementAppCommand(IManagementService managementService, ICommandLogService commandLogService) : base(commandLogService)
        {
            _managementService = managementService;
        }

        [CheckUserAccessSlashCommand(Rights.ViewRights, Rights.ManageBot)]
        [SlashCommand("view-rights", "Shows all rights with the associated roles.")]
        public async Task ViewRightsAsync(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "view-rights");

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetOrCreateGuildAsync(dbContext, ctx.Guild.Id, ctx.Guild.Name);
            var rights = await _managementService.GetRightsAsync(dbContext);

            var pages = new List<Page>();

            foreach (var right in rights)
            {
                DiscordEmbedBuilder embedBuilder = await CreateRightEntryEmbedAsync(ctx, dbContext, guild.GuildId, right);

                pages.Add(new Page(embed: embedBuilder));
            }

            await ctx.DeleteResponseAsync();
            await ctx.Channel.SendPaginatedMessageAsync(ctx.Member, pages);
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("set-rights", "Sets the rights for a specific role.")]
        public async Task SetRightsAsync(InteractionContext ctx,
            [Option("role", "The role which should get the right.")] DiscordRole role,
            [Option("right", "The right the role should gain.")] long right)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "set-rights", $"role:{role.Name};right:{(Rights)right}");

            using FreeGuardDbContext dbContext = new();
            var rolesPerRight = await _managementService.GetRolesForRightAsync(dbContext, ctx.Guild.Id, (Rights)right);
            var dbRight = await _managementService.GetRightAsync(dbContext, (Rights)right);

            if (!rolesPerRight.Any(x => x.DiscordRoleId == role.Id))
            {
                try
                {
                    await _managementService.SetRightForRoleAsync(dbContext, ctx.Guild.Id, (Rights)right, role.Id, ctx.Member.Id);
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"The role '{role.Name}' has now the '{dbRight.Name}' rights."));
                }
                catch (Exception ex)
                {
                    ctx.Client.Logger.LogError(ex, "Error while setting rights for role '{0}'", role.Name);
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Failed to set role because of an unknonw error!"));
                }
            }
            else
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("This role already posses this right!"));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("update-channel", "Updates the channel where to send specific messages to.")]
        public async Task UpdateChannelOnGuild(InteractionContext ctx,
            [Option("channel-type", "The channel type which should log to this channel.")] Services.ChannelType channelType,
            [Option("channel", "The channel to log to. Leave it empty to make it send no logs into channels.")] DiscordChannel channel = null)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "update-channel", $"channeltype:{channelType};channel:{channel.Name}/{channel.Id}");

            using FreeGuardDbContext dbContext = new();

            try
            {
                await _managementService.UpdateChannelAsync(dbContext, ctx.Guild.Id, channel?.Id, channelType);

                if (channel != null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Channel {channel.Mention} has been set as a '{channelType}'."));
                }
                else
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Channel of type '{channelType}' has been unset."));
                }
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while updating logging channel.");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Channel could not be set because of an error!"));
            }
        }

        private async Task<DiscordEmbedBuilder> CreateRightEntryEmbedAsync(InteractionContext ctx, FreeGuardDbContext dbContext, ulong guildId, Botright right)
        {
            var embedBuilder = new DiscordEmbedBuilder()
                                .WithColor(new DiscordColor(255, 131, 0))
                                .WithAuthor(ctx.Client.CurrentUser.Username)
                                .WithTitle("Bot rights")
                                .WithDescription("Displays the bots current right system. Admins and guild owners always have access to all commands.")
                                .AddField($"{right.Id}. Right: {right.Name}", right.Description);

            foreach (var roleRight in right.Rolerights)
            {
                if (ctx.Guild.Roles.TryGetValue(roleRight.Role.DiscordRoleId, out DiscordRole discordRole))
                {
                    embedBuilder.AddField("Role name", discordRole.Name);
                }
                else
                {
                    embedBuilder.AddField("Role Id:", roleRight.Role.DiscordRoleId.ToString());
                }

                embedBuilder.AddField("Create date: ", roleRight.CreateDate.ToString(), true);

                if (ctx.Guild.Members.TryGetValue(roleRight.CreateUserId, out DiscordMember member))
                {
                    embedBuilder.AddField("Create user: ", member.DisplayName, true);
                }
                else
                {
                    embedBuilder.AddField("Create user: ", roleRight.CreateUserId.ToString(), true);
                }

                ctx.Client.Logger.LogDebug($"Role: {roleRight.Role.Id}");
            }

            return embedBuilder;
        }
    }
}