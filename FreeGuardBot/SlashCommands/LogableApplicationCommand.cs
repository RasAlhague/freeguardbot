﻿using DSharpPlus.SlashCommands;
using FreeGuardBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands;

public abstract class LogableApplicationCommandModule : ApplicationCommandModule
{
    protected readonly ICommandLogService _commandLogService;

    public LogableApplicationCommandModule(ICommandLogService commandLogService)
    {
        _commandLogService = commandLogService;
    }
}
