﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBot.Services.Infractions;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FreeGuardBotLib.Models.Infraction;

namespace FreeGuardBot.SlashCommands;

[SlashCommandGroup("infraction", "Commands for handling infractions.")]
internal class InfractionAppCommand : LogableApplicationCommandModule
{
    private readonly IInfractionService _infractionService;

    public InfractionAppCommand(ICommandLogService commandLogService, IInfractionService infractionService) : base(commandLogService)
    {
        _infractionService = infractionService;
    }

    [SlashRequireGuild]
    [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.UseTimeout)]
    [SlashCommand("warn", "Gives users a warning.")]
    public async Task WarnAsync(
        InteractionContext ctx,
        [Option("reason", "The reason why the warning was given")] string reason,
        [Option("user1", "User who gets a warning")] DiscordUser user1,
        [Option("user2", "User who gets a warning")] DiscordUser user2 = null,
        [Option("user3", "User who gets a warning")] DiscordUser user3 = null,
        [Option("user4", "User who gets a warning")] DiscordUser user4 = null,
        [Option("user5", "User who gets a warning")] DiscordUser user5 = null,
        [Option("user6", "User who gets a warning")] DiscordUser user6 = null,
        [Option("user7", "User who gets a warning")] DiscordUser user7 = null,
        [Option("user8", "User who gets a warning")] DiscordUser user8 = null)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "warn", $"user1:{user1?.Username}#{user1?.Discriminator};user2:{user2?.Username}#{user2?.Discriminator};user3:{user3?.Username}#{user3?.Discriminator};user4:{user4?.Username}#{user4?.Discriminator};user5:{user5?.Username}#{user5?.Discriminator};user6:{user6?.Username}#{user6?.Discriminator};user7:{user7?.Username}#{user7?.Discriminator};user8:{user8?.Username}#{user8?.Discriminator};reason:{reason}");

        using FreeGuardDbContext dbContext = new();

        var userList = CreateUserList(user1, user2, user3, user4, user5, user6, user7, user8);
        var idList = userList.Select(x => x.Id).ToList();

        var infractions = await _infractionService.CreateInfractionsAsync(dbContext, Infraction.PunishmentType.Warn, ctx.Guild.Id, ctx.Member.Id, reason, idList);
        await _infractionService.LogInfractionsAsync(ctx, dbContext, infractions);

        if (!await GiveWarningRolesAsync(ctx, dbContext, reason, await Guild.GetAsync(dbContext, ctx.Guild.Id), infractions))
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Could not give warning role to users, because no warning role has been set!"));
            return;
        }

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Executed warning."));
    }

    [SlashRequireGuild]
    [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.UseTimeout)]
    [SlashCommand("kick", "Gives users a kick.")]
    public async Task KickAsync(
        InteractionContext ctx,
        [Option("reason", "The reason why the warning was given")] string reason,
        [Option("user1", "User who gets a warning")] DiscordUser user1,
        [Option("user2", "User who gets a warning")] DiscordUser user2 = null,
        [Option("user3", "User who gets a warning")] DiscordUser user3 = null,
        [Option("user4", "User who gets a warning")] DiscordUser user4 = null,
        [Option("user5", "User who gets a warning")] DiscordUser user5 = null,
        [Option("user6", "User who gets a warning")] DiscordUser user6 = null,
        [Option("user7", "User who gets a warning")] DiscordUser user7 = null,
        [Option("user8", "User who gets a warning")] DiscordUser user8 = null)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "kick", $"user1:{user1?.Username}#{user1?.Discriminator};user2:{user2?.Username}#{user2?.Discriminator};user3:{user3?.Username}#{user3?.Discriminator};user4:{user4?.Username}#{user4?.Discriminator};user5:{user5?.Username}#{user5?.Discriminator};user6:{user6?.Username}#{user6?.Discriminator};user7:{user7?.Username}#{user7?.Discriminator};user8:{user8?.Username}#{user8?.Discriminator};reason:{reason}");

        using FreeGuardDbContext dbContext = new();

        var userList = CreateUserList(user1, user2, user3, user4, user5, user6, user7, user8);
        var idList = userList.Select(x => x.Id).ToList();

        var infractions = await _infractionService.CreateInfractionsAsync(dbContext, Infraction.PunishmentType.Kick, ctx.Guild.Id, ctx.Member.Id, reason, idList);
        await _infractionService.LogInfractionsAsync(ctx, dbContext, infractions);

        await KickPeopleAsync(ctx, reason, infractions);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Executed Kicks."));
    }

    [SlashRequireGuild]
    [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.UseTimeout)]
    [SlashCommand("ban", "Gives users a ban.")]
    public async Task BanAsync(
        InteractionContext ctx,
        [Option("reason", "The reason why the warning was given")] string reason,
        [Option("user1", "User who gets a warning")] DiscordUser user1,
        [Option("user2", "User who gets a warning")] DiscordUser user2 = null,
        [Option("user3", "User who gets a warning")] DiscordUser user3 = null,
        [Option("user4", "User who gets a warning")] DiscordUser user4 = null,
        [Option("user5", "User who gets a warning")] DiscordUser user5 = null,
        [Option("user6", "User who gets a warning")] DiscordUser user6 = null,
        [Option("user7", "User who gets a warning")] DiscordUser user7 = null,
        [Option("user8", "User who gets a warning")] DiscordUser user8 = null)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "ban", $"user1:{user1?.Username}#{user1?.Discriminator};user2:{user2?.Username}#{user2?.Discriminator};user3:{user3?.Username}#{user3?.Discriminator};user4:{user4?.Username}#{user4?.Discriminator};user5:{user5?.Username}#{user5?.Discriminator};user6:{user6?.Username}#{user6?.Discriminator};user7:{user7?.Username}#{user7?.Discriminator};user8:{user8?.Username}#{user8?.Discriminator};reason:{reason}");

        using FreeGuardDbContext dbContext = new();

        var userList = CreateUserList(user1, user2, user3, user4, user5, user6, user7, user8);
        var idList = userList.Select(x => x.Id).ToList();

        var infractions = await _infractionService.CreateInfractionsAsync(dbContext, Infraction.PunishmentType.Ban, ctx.Guild.Id, ctx.Member.Id, reason, idList);
        await _infractionService.LogInfractionsAsync(ctx, dbContext, infractions);

        await BanPeopleAsync(ctx, reason, infractions);

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Executed warning."));
    }

    private async Task<bool> GiveWarningRolesAsync(InteractionContext ctx, FreeGuardDbContext context, string reason, Guild guild, IEnumerable<Infraction> infractions)
    {
        var warningRole = await context.Roles
            .Where(x => x.GuildId == guild.Id && x.IsWarningRole == "True")
            .FirstOrDefaultAsync();

        if (warningRole is null)
        {
            return false;
        }

        DiscordRole role = ctx.Guild.GetRole(warningRole.DiscordRoleId);

        foreach (var infraction in infractions)
        {
            var user = await ctx.Guild.GetMemberAsync(infraction.PunishedId);
            await user.GrantRoleAsync(role, reason);

            await ctx.Channel.SendMessageAsync($"{ctx.Member.Mention} has given {user.Mention} a **Warning** for: `{reason}`");
        }

        return true;
    }

    private async Task KickPeopleAsync(InteractionContext ctx, string reason, IEnumerable<Infraction> infractions)
    {
        foreach (var infraction in infractions)
        {
            var user = await ctx.Guild.GetMemberAsync(infraction.PunishedId);


            try
            {
                var dmChannel = await user.CreateDmChannelAsync();
                await dmChannel.SendMessageAsync($"You have been kicked from '{ctx.Guild.Name}' for `{reason}`");
                await Task.Delay(1000);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, $"Could not dm member: {user.Mention}.");
                await ctx.Channel.SendMessageAsync($"Could not dm member {user.Mention}.");
            }

            try
            {
                await user.RemoveAsync(reason);
                await ctx.Channel.SendMessageAsync($"{ctx.Member.Mention} has kicked {user.Mention} for reason: `{reason}`");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, $"Could not kick this member: {user.Mention}.");
                await ctx.Channel.SendMessageAsync($"Could not kick member {user.Mention}.");
            }
        }
    }

    private async Task BanPeopleAsync(InteractionContext ctx, string reason, IEnumerable<Infraction> infractions)
    {
        foreach (var infraction in infractions)
        {
            var user = await ctx.Guild.GetMemberAsync(infraction.PunishedId);

            try
            {
                var dmChannel = await user.CreateDmChannelAsync();
                await dmChannel.SendMessageAsync($"You have been banned from '{ctx.Guild.Name}' for `{reason}`");
                await Task.Delay(1000);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, $"Could not dm member: {user.Mention}.");
                await ctx.Channel.SendMessageAsync($"Could not dm member {user.Mention}.");
            }

            try
            {
                await user.BanAsync(0, reason);
                await ctx.Channel.SendMessageAsync($"{ctx.Member.Mention} has banned {user.Mention} **Warning** for reason: `{reason}`");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, $"Could not ban this member: {user.Mention}.");
                await ctx.Channel.SendMessageAsync($"Could not ban member {user.Mention}.");
            }

        }
    }

    private IEnumerable<DiscordUser> CreateUserList(params DiscordUser[] users)
    {
        var usersList = new List<DiscordUser>();

        for (int i = 0; i < users.Length; i++)
        {
            if (users[i] != null)
            {
                usersList.Add(users[i]);
            }
        }

        return usersList;
    }
}
