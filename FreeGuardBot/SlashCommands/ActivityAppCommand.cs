﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    [SlashCommandGroup("activity", "description")]
    public class ActivityAppCommand : LogableApplicationCommandModule
    {
        private readonly IActivityService _activityService;

        public ActivityAppCommand(IActivityService activityService, ICommandLogService commandLogService) : base(commandLogService)
        {
            _activityService = activityService;
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("chat", "Shows the chat activity from a specific member. For now it only returns a csv file with the data.")]
        public async Task ChatActivityAsync(InteractionContext ctx, [Option("user", "User which you want to check log for.")] DiscordUser user)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "chat", $"user:{user.Username}#{user.Discriminator}");

            using FreeGuardDbContext dbContext = new();

            var messageLogs = await _activityService.GetChatActivityForMemberAsync(dbContext, user.Id, ctx.Guild.Id);
            var logCsv = CreateChatCsvForUser(user.Username, messageLogs);

            string tempFileName = $"./chat_activity_{DateTime.UtcNow.Ticks}.csv";

            File.WriteAllText(tempFileName, logCsv);

            using (FileStream fs = File.OpenRead(tempFileName))
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Chat activity for '{user.Username}':").AddFile($"chat_activity_{DateTime.UtcNow.Ticks}.csv", fs));
            }

            File.Delete(tempFileName);
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("chat-top", "Shows the users with the least chat activity since start of recording data.")]
        public async Task TopChatActivityAsync(
            InteractionContext ctx,
            [Option("order-direction", "The direction in which you want to order")] OrderDirection orderDirection,
            [Option("max", "Number of max entries. (max: 2147483647)")] long max)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "chat-top", $"order-dir:{orderDirection.ToString()};max:{max}");

            using FreeGuardDbContext dbContext = new();

            try
            {
                var guildMembers = await ctx.Guild.GetAllMembersAsync();
                var activityCounts = await _activityService.GetTopChatActivityAsync(dbContext, guildMembers, ctx.Guild.Id, (int)max, orderDirection);

                var logCsv = await CreateActivityCountCsvAsync(ctx, activityCounts);

                string tempFileName = $"./chat_activity_{DateTime.UtcNow.Ticks}.csv";

                File.WriteAllText(tempFileName, logCsv);

                using (FileStream fs = File.OpenRead(tempFileName))
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Chat activity for top {max} ordered {orderDirection.ToString()}:").AddFile($"top_chat_activity_{DateTime.UtcNow.Ticks}.csv", fs));
                }

                File.Delete(tempFileName);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while trying to create activity count data.");

                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"An error occured while trying to get data."));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("command-usage", "Shows the command activity of a certain command.")]
        public async Task TopChatActivityAsync(
            InteractionContext ctx,
            [Option("command-name", "Name of the command to show the logs for")] string commandName)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "command-usage", commandName);

            using FreeGuardDbContext dbContext = new();

            try
            {
                var commandInfos = await _commandLogService.GetCommandInfosWithNameAsync(dbContext, ctx.Guild.Id, commandName);
                var logCsv = await CreateCommandInfoCsvAsync(ctx, commandInfos);
                string tempFileName = $"./chat_activity_{DateTime.UtcNow.Ticks}.csv";

                File.WriteAllText(tempFileName, logCsv);

                using (FileStream fs = File.OpenRead(tempFileName))
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Command activity for '{commandName}':").AddFile($"command_activity_{DateTime.UtcNow.Ticks}.csv", fs));
                }

                File.Delete(tempFileName);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while trying to create activity count data.");

                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"An error occured while trying to get data."));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("used-command", "Shows the command activity of a certain command.")]
        public async Task TopChatActivityAsync(
            InteractionContext ctx,
            [Option("user", "The user which should be checked")] DiscordUser user)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "used-command", $"{user.Username}");

            using FreeGuardDbContext dbContext = new();

            try
            {
                var commandInfos = await _commandLogService.GetCommandInfosForUserAsync(dbContext, ctx.Guild.Id, user.Id);
                var logCsv = await CreateCommandInfoCsvAsync(ctx, commandInfos);
                string tempFileName = $"./chat_activity_{DateTime.UtcNow.Ticks}.csv";

                File.WriteAllText(tempFileName, logCsv);

                using (FileStream fs = File.OpenRead(tempFileName))
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Command activity for '{user.Username}#{user.Discriminator}':").AddFile($"command_activity_{DateTime.UtcNow.Ticks}.csv", fs));
                }

                File.Delete(tempFileName);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while trying to create activity count data.");

                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"An error occured while trying to get data."));
            }
        }

        private string CreateChatCsvForUser(string username, IEnumerable<MessageActivityLogEntry> logs)
        {
            var sb = new StringBuilder();

            foreach (var log in logs)
            {
                sb.AppendLine($"{username},{log.WriterId},{log.WriteDate}");
            }

            return sb.ToString();
        }

        private async Task<string> CreateActivityCountCsvAsync(InteractionContext ctx, IEnumerable<ActivityCount> activityCounts)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Discord name;Joined;Display Name;Role;Count Of Messages since recording");

            var members = await ctx.Guild.GetAllMembersAsync();

            foreach (var countEntry in activityCounts)
            {
                var member = members.FirstOrDefault(x => x.Id == countEntry.userId);

                if (member != null)
                {
                    string role = member.Roles.Any(x => x.Name == "Free Guard") ? "guard" : "guest";
                    sb.AppendLine($"{member.Username}#{member.Discriminator};{member.JoinedAt.UtcDateTime.ToString("yyyy-MM-dd hh:mm:ss")} utc;{member.DisplayName};{role};{countEntry.countOfMessages}");
                }
            }

            return sb.ToString();
        }

        private async Task<string> CreateCommandInfoCsvAsync(InteractionContext ctx, IEnumerable<Commandinfo> commandInfos)
        {
            var sb = new StringBuilder();
            sb.AppendLine("CommandName|Parameters|User|UserId|UseDate");

            var members = await ctx.Guild.GetAllMembersAsync();

            foreach (var commandInfo in commandInfos)
            {
                var member = members.FirstOrDefault(x => x.Id == commandInfo.CreateUserId);

                if (member != null)
                {
                    sb.AppendLine($"{commandInfo.CommandName}|{commandInfo.Parameter}|{member.Username}#{member.Discriminator}|{commandInfo.CreateUserId}|{commandInfo.CreateDate}");
                }
            }

            return sb.ToString();
        }
    }
}
