﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    [SlashCommandGroup("trivia", "Games for the FG bot.")]
    public class TriviaAppModule : LogableApplicationCommandModule
    {
        private readonly ITriviaService _triviaService;

        public TriviaAppModule(ITriviaService triviaService, ICommandLogService commandLogService) : base(commandLogService)
        {
            _triviaService = triviaService;
        }

        [SlashCommand("quiz", "Creates a new vacation for the user which used this command.")]
        public async Task GetRandomQuestion(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "quiz");

            var question = await _triviaService.GetRandomQuestionAsync();

            var embedBuilder = new DiscordEmbedBuilder()
                .WithAuthor($"{ctx.User.Username}'s trivia question", null, ctx.User.AvatarUrl)
                .AddField(question.Question, $"You have {question.AnswerTime} seconds to answer")
                .AddField("Difficulty", question.DifficultyLevel.Name)
                .AddField("Creator", question.CreateUserId.ToString());

            var discordWebhookBuilder = new DiscordWebhookBuilder()
                .AddEmbed(embedBuilder)
                .AddComponents(GetButtons(question));

            var message = await ctx.EditResponseAsync(discordWebhookBuilder);
            var response = await message.WaitForButtonAsync(ctx.Member, TimeSpan.FromSeconds(question.AnswerTime));

            if (response.TimedOut)
            {
                await ctx.EditResponseAsync(discordWebhookBuilder.WithContent("Timeout of answer time!"));
                await response.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
                return;
            }

            var answer = int.Parse(response.Result.Id);

            if (question.TriviaAnswers.Where(x => x.IsCorrect).Any(x => x.Id == answer))
            {
                await ctx.EditResponseAsync(discordWebhookBuilder.WithContent("Yes you found the correct answer!"));
                await response.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
            }
            else
            {

            }
        }

        private IEnumerable<DiscordButtonComponent> GetButtons(FreeGuardBotLib.Models.TriviaQuestion question)
        {
            var buttons = new List<DiscordButtonComponent>();

            foreach (var answer in question.TriviaAnswers)
            {
                buttons.Add(new DiscordButtonComponent(ButtonStyle.Primary, answer.Id.ToString(), answer.Answer));
            }

            return buttons;
        }
    }
}
