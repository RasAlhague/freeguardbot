﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Exceptions;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    [SlashCommandGroup("vacation", "Commands for vacation management.")]
    public class VacationAppCommand : LogableApplicationCommandModule
    {
        private readonly IVacationService _vacationService;

        public VacationAppCommand(IVacationService vacationService, ICommandLogService commandLogService) : base(commandLogService)
        {
            _vacationService = vacationService;
        }

        [CheckUserAccessSlashCommand(Rights.UseVacation, Rights.ManageBot, Rights.ManageVacation)]
        [SlashCommand("start", "Creates a new vacation for the user which used this command.")]
        public async Task NewEntryCommand(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "start");

            var dialogue = new DiscordDialogue();
            await dialogue.InitializeAsync(ctx);

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
            var discordUser = await Discorduser.GetOrCreateDiscorduserAsync(dbContext, ctx.Member.Id);

            if (await _vacationService.CheckForExistingVacationAsync(dbContext, discordUser, guild.Id))
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You currently have a active vacation. Please end this vacation before trying to start a new one."));
                return;
            }

            InteractivityResult<ComponentInteractionCreateEventArgs> roleResult = await GetFreezeRoll(ctx, dialogue);
            var selectedId = ulong.Parse(roleResult.Result.Values[0]);

            var role = await Role.GetOrCreateRoleAsync(dbContext, selectedId, ctx.Member.Id, guild.Id);
            string reason = await GetReason(ctx, dialogue);
            await GetSteamId(dialogue, dbContext, discordUser);

            Vacation vacation = await _vacationService.AddVacationAsync(dbContext, discordUser, role.Id, guild.Id, reason);

            DiscordRole vacationRole = await GetDiscordVacationRole(ctx, dbContext, guild);
            await ctx.Member.GrantRoleAsync(vacationRole, $"vacation_created: {reason}");
            DiscordEmbed discordEmbed = await GetVacationEmbed(ctx, discordUser, role, vacation);

            if (discordEmbed != null)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(discordEmbed));
            }
            else
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The user could not be found. He probably left already."));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.ManageVacation)]
        [SlashCommand("list", "Shows a list of all people with vacation status.")]
        public async Task ShowListCommand(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "list");

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

            var vacations = await _vacationService.GetAllVacationsAsync(dbContext, guild.Id);

            if (!vacations.Any())
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("There are currently no vacations!"));
                return;
            }

            List<Page> pages = new List<Page>();

            foreach (var vacation in vacations)
            {
                var vacatedUser = await dbContext.Discordusers.FindAsync(vacation.AffectedId);
                var vacatedRole = await dbContext.Roles.FindAsync(vacation.FreezeTitleId);

                var embed = await GetVacationEmbed(ctx, vacatedUser, vacatedRole, vacation);

                if (embed != null)
                {
                    pages.Add(new Page(embed: embed));
                }
                else
                {
                    await _vacationService.RemoveVacationAsync(dbContext, vacatedUser, guild.Id);
                }
            }

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("List of vacations:"));
            await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
        }

        [CheckUserAccessSlashCommand(Rights.UseVacation, Rights.ManageBot, Rights.ManageVacation)]
        [SlashCommand("end", "Ends the vacation of a user.")]
        public async Task EndVacationCommand(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "end");

            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
            var discordUser = await Discorduser.GetOrCreateDiscorduserAsync(dbContext, ctx.Member.Id);

            if (!await _vacationService.CheckForExistingVacationAsync(dbContext, discordUser, guild.Id))
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You currently have no vacations registered!"));
                return;
            }

            await _vacationService.RemoveVacationAsync(dbContext, discordUser, guild.Id);

            DiscordRole vacationRole = await GetDiscordVacationRole(ctx, dbContext, guild);
            await ctx.Member.RevokeRoleAsync(vacationRole, $"vacation_ended: vacation ended!");

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Vacation successfully ended! Welcome back fellow guard!"));
        }

        [CheckUserAccessSlashCommand(Rights.UseVacation, Rights.ManageBot, Rights.ManageVacation)]
        [SlashCommand("show", "Shows the current vacation if one exists.")]
        public async Task ShowVacationCommand(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().WithContent("Not implemented!"));
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "show");
        }

        /// <summary>
        /// Command for exporting the blacklist entries into a csv file.
        /// </summary>
        /// <param name="ctx">The context of the command.</param>
        /// <returns>Returns just a task.</returns>
        [CheckUserAccessSlashCommand(Rights.ExportList, Rights.ManageBot)]
        [SlashCommand("export", "Exports the vacations to a csv file.")]
        public async Task ExportListCommand(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "export");

            using FreeGuardDbContext dbContext = new();
            var guild = await dbContext.Guilds
                .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

            StringBuilder csvBuilder = new StringBuilder();
            csvBuilder.AppendLine("id;affectedid;affectedName;freezetitleid;startdate;estimatedduration;reason;finished;createdate;createuserid;modifydate;modifyuserid");

            foreach (var entry in await dbContext.Vacations.Where(x => x.GuildId == guild.Id).ToListAsync())
            {
                var user = await ctx.Client.GetUserAsync(entry.AffectedId);
                csvBuilder.AppendLine($"{entry.Id};{entry.AffectedId};{user.Username}#{user.Discriminator};{entry.FreezeTitleId};{entry.StartDate};{entry.EstimatedDuration};{entry.Reason?.Replace(';', ',')};{entry.Finished};{entry.CreateDate};{entry.CreateUserId};{entry.ModifyDate};{entry.ModifyUserId}");
            }

            await File.WriteAllTextAsync("./data.csv", csvBuilder.ToString());

            using var fs = new FileStream("./data.csv", FileMode.Open);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Current recruit entries:").AddFile($"recruit_{DateTime.Now.ToString("yyyy_MM_dd")}.csv", fs));
        }

        private async static Task<DiscordEmbedBuilder> GetVacationEmbed(InteractionContext ctx, Discorduser discordUser, Role role, Vacation vacation)
        {
            DiscordEmbedBuilder embedBuilder = null;

            try
            {
                var user = await ctx.Guild.GetMemberAsync(discordUser.DiscordUserId);

                var discordRole = ctx.Guild.GetRole(role.DiscordRoleId);
                var roleName = "Unavailable";

                if (discordRole != null)
                {
                    roleName = discordRole.Name;
                }

                embedBuilder = new DiscordEmbedBuilder()
                                .WithTitle($"Vacation number: {vacation.Id}")
                                .WithAuthor(user.DisplayName, iconUrl: user.AvatarUrl)
                                .WithFooter(vacation.StartDate.ToString())
                                .AddField("Name:", user.Mention)
                                .AddField("Freeze Title:", roleName)
                                .AddField("Steam Profile:", $"https://steamcommunity.com/profiles/{discordUser.SteamId}/")
                                .AddField("Reason:", vacation.Reason);
            }
            catch (NotFoundException nfex)
            {
                ctx.Client.Logger.LogError(nfex, "The user could not be found!");
            }

            return embedBuilder;
        }

        private async Task GetSteamId(DiscordDialogue dialogue, FreeGuardDbContext dbContext, Discorduser discordUser)
        {
            if (string.IsNullOrEmpty(discordUser.SteamId))
            {
                var steamId = await dialogue
                    .SendDmDialogueAsync<string>(
                    "Please send your SteamId64. This is only required once. If you dont know where to find it paste your steam profile into this website https://steamid.io/lookup and retrieve it.",
                    x => ulong.TryParse(x, out _)
                );

                discordUser.SteamId = steamId;
                dbContext.Discordusers.Update(discordUser);
                await dbContext.SaveChangesAsync();
            }
        }

        private async Task<string> GetReason(InteractionContext ctx, DiscordDialogue dialogue)
        {
            var options = new List<DiscordSelectComponentOption>() {
                new DiscordSelectComponentOption("School/Work", "school_work"),
                new DiscordSelectComponentOption("Health Issues", "health"),
                new DiscordSelectComponentOption("On a trip", "on_a_trip"),
                new DiscordSelectComponentOption("Custom", "custom")
            };

            var dropdown = new DiscordSelectComponent("reason_dropdown", null, options, false);
            var builder = new DiscordMessageBuilder()
                .WithContent("Please choose the reason or use custom reason if nothing fits!")
                .AddComponents(dropdown);
            var message = await builder.SendAsync(dialogue.DiscordDmChannel);

            var reasonResult = await message.WaitForSelectAsync(ctx.Member, "reason_dropdown", TimeSpan.FromMinutes(5));
            if (reasonResult.TimedOut)
            {
                await message.RespondAsync("Vacation not created, reason selection timed out!");
            }

            await reasonResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
            var reason = reasonResult.Result.Values[0];

            if (reason == "custom")
            {
                reason = await dialogue.SendDmDialogueAsync<string>("Please write your reason!");
            }

            return reason;
        }

        private async Task<InteractivityResult<ComponentInteractionCreateEventArgs>> GetFreezeRoll(InteractionContext ctx, DiscordDialogue dialogue)
        {
            var roles = ctx.Member.Roles;
            var options = new List<DiscordSelectComponentOption>();

            foreach (var role in roles)
            {
                options.Add(new DiscordSelectComponentOption(role.Name, role.Id.ToString()));
            }

            var dropdown = new DiscordSelectComponent("vacation_dropdown", null, options, false);
            var builder = new DiscordMessageBuilder()
                .WithContent("Please choose the role to freeze!")
                .AddComponents(dropdown);

            var message = await builder.SendAsync(dialogue.DiscordDmChannel);

            var result = await message.WaitForSelectAsync(ctx.Member, "vacation_dropdown", TimeSpan.FromMinutes(5));

            if (result.TimedOut)
            {
                await message.RespondAsync("Vacation not created, freeze role selection timed out!");
            }

            await result.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
            return result;
        }

        private static async Task<DiscordRole> GetDiscordVacationRole(InteractionContext ctx, FreeGuardDbContext dbContext, Guild guild)
        {
            var vacationRoleInDb = await dbContext.Roles
                            .FirstOrDefaultAsync(x => x.Guild.Id == guild.Id
                                         && x.IsVacationRole == true.ToString());

            var vacationRole = ctx.Guild.GetRole(vacationRoleInDb.DiscordRoleId);
            return vacationRole;
        }
    }
}
