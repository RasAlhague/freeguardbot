﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBot.Services.Lls;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    public class LlsAppCommand : LogableApplicationCommandModule
    {
        private readonly ILlsService _llsService;

        public LlsAppCommand(ICommandLogService commandLogService, ILlsService llsService) : base(commandLogService)
        {
            _llsService = llsService;
        }

        [SlashCommand("lls", "Sends a lls image to the channel it was used in.")]
        public async Task SendLls(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "lls");

            Random r = new();
            var files = Directory.GetFiles("./images");

            var file = files[r.Next(0, files.Length)];

            using var fs = File.OpenRead(file);

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddFile("lls.gif", fs));
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("lls-upload", "Uploads a new lls image.")]
        public async Task UploadLlsImage(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "lls-upload");

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Please send a file to upload. Abuse will be punished hard!"));

            var answer = await ctx.Channel.GetNextMessageAsync(x => !x.Author.IsBot && x.Author.Id == ctx.Member.Id && x.Attachments.Any(), TimeSpan.FromSeconds(60));

            if (answer.TimedOut)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The file upload timed out. You have only 1 minute to upload a file!"));
            }

            var attachment = answer.Result.Attachments.First();

            var extension = Path.GetExtension(attachment.FileName);

            var filePath = await _llsService.UploadImageAsync(attachment.Url, extension);

            ctx.Client.Logger.LogWarning($"User `{ctx.Member.DisplayName}` with id `{ctx.Member.Id}` uploaded `{filePath}`!");

            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Uploaded file!"));
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("lls-zip", "Get a zip file with all lls images.")]
        public async Task GetImageZip(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "lls-zip");

            try
            {
                string fileName = $"llsimages_{ctx.Member.Id}.zip";
                var zipFilePath = _llsService.CreateZipAsync(fileName);

                using (var fs = File.OpenRead(zipFilePath))
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Here is your zip file!").AddFile(fileName, fs));
                }

                File.Delete(zipFilePath);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Creating or sending zip failed.");
            }
        }
    }
}
