﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    [SlashCommandGroup("roleselection", "Commands for configuration role selection.")]
    public class RoleSelectionModule : LogableApplicationCommandModule
    {
        private readonly IRoleSelectionService _roleSelectionService;

        public RoleSelectionModule(IRoleSelectionService roleSelectionService, ICommandLogService commandLogService) : base(commandLogService)
        {
            _roleSelectionService = roleSelectionService;
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("create", "Commands for creating a role selection message.")]
        public async Task CreateSelection(InteractionContext ctx, [Option("description", "The description for the message embed.")] string description)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "create");
            var message = await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Trying to create a selection message..."));

            using FreeGuardDbContext dbContext = new();

            try
            {
                (RoleSelect roleSelect, CreationResult result) = await _roleSelectionService.CreateRoleSelectionAsync(dbContext, description, ctx.Guild.Id, ctx.Channel.Id, message.Id, ctx.User.Id);

                DiscordEmbedBuilder embed = result switch
                {
                    CreationResult.Success => await CreateRoleSelectEmbedAsync(dbContext, roleSelect, ctx.Client.CurrentUser.AvatarUrl),
                    CreationResult.Duplicate => CreateDuplicatedRoleSelectEmbed(),
                    _ => CreateUnknownErrorEmbed(),
                };

                await Program.UpdateCacheAsync();
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while creating role select.");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(CreateUnknownErrorEmbed()));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("add_role", "Adds a role to the reaction message.")]
        public async Task AddRole(InteractionContext ctx, [Option("role", "The role which should be given on reaction.")] DiscordRole role, [Option("text", "The text displayed for the role")] string text)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "add_role", $"role:{role.Name}");

            var message = await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Please react with the emoji you want this role to use! (5 minutes)"));
            var result = await message.WaitForReactionAsync(ctx.User, TimeSpan.FromMinutes(5));
            var emoji = result.Result.Emoji;

            using FreeGuardDbContext dbContext = new();

            try
            {
                var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
                var roleSelect = await dbContext.RoleSelects
                    .FirstOrDefaultAsync(x => x.GuildId == guild.Id);

                if (roleSelect is null)
                {
                    await SendAndDeleteAfterAsync(ctx, 10, "No role select set up yet");
                    return;
                }
                var creationResult = await _roleSelectionService.AddRoleAsync(dbContext, text, ctx.Guild.Id, emoji.Id, emoji.GetDiscordName(), role.Id, ctx.User.Id);

                roleSelect = await dbContext.RoleSelects
                    .FirstOrDefaultAsync(x => x.GuildId == guild.Id);

                var embed = creationResult switch
                {
                    CreationResult.Success => await CreateRoleSelectEmbedAsync(dbContext, roleSelect, ctx.Client.CurrentUser.AvatarUrl),
                    CreationResult.Duplicate => CreateDuplicatedRoleSelectEmbed(),
                    _ => CreateUnknownErrorEmbed(),
                };

                var channel = ctx.Guild.GetChannel(roleSelect.ChannelId);
                var roleSelectMessage = await channel.GetMessageAsync(roleSelect.MessageId);
                await roleSelectMessage.ModifyAsync(embed.Build());

                if (emoji.Id == 0)
                {
                    await roleSelectMessage.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, emoji.GetDiscordName()));
                }
                else
                {
                    await roleSelectMessage.CreateReactionAsync(DiscordEmoji.FromGuildEmote(ctx.Client, emoji.Id));
                }
                await Program.UpdateCacheAsync();

                var messageStayTime = 10;

                for (int i = messageStayTime; i > 1; i--)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Updated role select, this message will delete itself in {i} seconds..."));
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }

                await ctx.DeleteResponseAsync();
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while a new role to role select");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(CreateUnknownErrorEmbed()));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("remove_role", "Removes a role from the reaction message.")]
        public async Task RemoveRole(InteractionContext ctx, [Option("role", "The role you want to remove from the selection.")] DiscordRole role)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "remove_role", $"role:{role.Name}");

            using FreeGuardDbContext dbContext = new();

            try
            {
                var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
                var roleSelect = await dbContext.RoleSelects
                    .Include(x => x.RoleSelectReactions)
                    .ThenInclude(x => x.Emoji)
                    .Include(x => x.RoleSelectReactions)
                    .ThenInclude(x => x.Role)
                    .FirstOrDefaultAsync(x => x.GuildId == guild.Id);

                if (roleSelect is null)
                {
                    await SendAndDeleteAfterAsync(ctx, 10, "No role select set up yet");
                    return;
                }

                var emojiToRemove = roleSelect.RoleSelectReactions.FirstOrDefault(x => x.Role.DiscordRoleId == role.Id)?.Emoji;

                if (emojiToRemove == null)
                {
                    await SendAndDeleteAfterAsync(ctx, 5, "Role to delete not found");
                    return;
                }

                var removeResult = await _roleSelectionService.RemoveRoleAsync(dbContext, ctx.Guild.Id, role.Id);
                roleSelect = await dbContext.RoleSelects
                    .FirstOrDefaultAsync(x => x.GuildId == guild.Id);

                var messageStayTime = 10;

                if (removeResult == DeletionResult.NotFound)
                {
                    await SendAndDeleteAfterAsync(ctx, messageStayTime, "Role to delete not found");
                    return;
                }

                var embed = removeResult switch
                {
                    DeletionResult.Success => await CreateRoleSelectEmbedAsync(dbContext, roleSelect, ctx.Client.CurrentUser.AvatarUrl),
                    _ => CreateUnknownErrorEmbed(),
                };

                var channel = ctx.Guild.GetChannel(roleSelect.ChannelId);
                var roleSelectMessage = await channel.GetMessageAsync(roleSelect.MessageId);
                await roleSelectMessage.ModifyAsync(embed.Build());

                if (emojiToRemove.Id == 0)
                {
                    await roleSelectMessage.DeleteReactionsEmojiAsync(DiscordEmoji.FromName(ctx.Client, emojiToRemove.Name));
                }
                else
                {
                    await roleSelectMessage.DeleteReactionsEmojiAsync(DiscordEmoji.FromGuildEmote(ctx.Client, emojiToRemove.EmojiId));
                }
                await roleSelectMessage.DeleteReactionsEmojiAsync(DiscordEmoji.FromGuildEmote(ctx.Client, emojiToRemove.EmojiId));

                await Program.UpdateCacheAsync();

                await SendAndDeleteAfterAsync(ctx, messageStayTime, "Role select has been updated");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while a new role to role select");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(CreateUnknownErrorEmbed()));
            }
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot)]
        [SlashCommand("reload", "Reloads the role select embeds.")]
        public async Task ReloadRoleSelect(InteractionContext ctx)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "reload");

            using FreeGuardDbContext dbContext = new();

            try
            {
                var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
                var roleSelect = await dbContext.RoleSelects
                    .Include(x => x.RoleSelectReactions)
                    .ThenInclude(x => x.Emoji)
                    .Include(x => x.RoleSelectReactions)
                    .ThenInclude(x => x.Role)
                    .FirstOrDefaultAsync(x => x.GuildId == guild.Id);

                if (roleSelect is null)
                {
                    await SendAndDeleteAfterAsync(ctx, 10, "No role selects found");
                }

                var channel = ctx.Guild.GetChannel(roleSelect.ChannelId);
                var roleSelectMessage = await channel.GetMessageAsync(roleSelect.MessageId);
                await roleSelectMessage.ModifyAsync((await CreateRoleSelectEmbedAsync(dbContext, roleSelect, ctx.Client.CurrentUser.AvatarUrl)).Build());

                await SendAndDeleteAfterAsync(ctx, 10, "Role select has been refreshed");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while a new role to role select");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(CreateUnknownErrorEmbed()));
            }
        }

        private static async Task SendAndDeleteAfterAsync(InteractionContext ctx, int messageStayTime, string message)
        {
            for (int i = messageStayTime; i > 1; i--)
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{message}, this message will delete itself in {i} seconds..."));
                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            await ctx.DeleteResponseAsync();
        }

        private async Task<DiscordEmbedBuilder> CreateRoleSelectEmbedAsync(FreeGuardDbContext dbContext, RoleSelect roleSelect, string iconLink)
        {
            var roleSelectReactions = await dbContext.RoleSelectReactions
                .Where(x => x.RoleSelectId == roleSelect.Id)
                .Include(x => x.Emoji)
                .ToListAsync();

            var embedBuilder = new DiscordEmbedBuilder()
                .WithTitle("Select your role")
                .WithDescription(roleSelect.Description)
                .WithFooter("Last update (UTC)", iconLink)
                .WithTimestamp(DateTime.UtcNow);

            StringBuilder sb = new StringBuilder();

            foreach (var reaction in roleSelectReactions)
            {
                sb.Append(reaction.Message)
                  .Append(": ");

                if (reaction.Emoji.Id == 0)
                {
                    sb.AppendLine($"{reaction.Emoji.Name}");
                }
                else
                {
                    sb.AppendLine($"<{reaction.Emoji.Name}{reaction.Emoji.EmojiId}>");
                }
            }

            string rolesText = sb.ToString();

            if (string.IsNullOrEmpty(rolesText))
            {
                rolesText = "No roles added yet";
            }
            embedBuilder.AddField("Roles:", rolesText);

            return embedBuilder;
        }

        private DiscordEmbedBuilder CreateDuplicatedRoleSelectEmbed()
        {
            var embedBuilder = new DiscordEmbedBuilder()
                .WithTitle("Duplicated role select!")
                .WithDescription("You can only have 1 role selection embed per server.")
                .WithTimestamp(DateTime.UtcNow);

            return embedBuilder;
        }

        private DiscordEmbedBuilder CreateUnknownErrorEmbed()
        {
            var embedBuilder = new DiscordEmbedBuilder()
                .WithTitle("Unknown problem during operation!")
                .WithDescription("Something went wrong! '*Tehe*'")
                .WithTimestamp(DateTime.UtcNow);

            return embedBuilder;
        }
    }
}
