﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Lavalink;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using GitLabApiClient.Models.Users.Responses;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands;

public class NukeAppCommand : LogableApplicationCommandModule
{
    public NukeAppCommand(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    [CheckUserAccessSlashCommand(Rights.ManageBot)]
    [SlashCommand("nuke", "Timeouts the users who wrote in the current channel in the selected timeframe.")]
    public async Task NukeAsync(InteractionContext ctx, [Option("minutes-in-past", "Timeframe of messages.")] long minutesInPast, [Option("nuclear-fallout-time", "The duration of the nuclear fallout in minutes (timeout).")] long nuclearFalloutTime)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Preparing the nukes!"));
        await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.CommandLoggingChannel, "weak", $"minutesInPast:{minutesInPast}");

        var builder = new DiscordMessageBuilder()
            .WithContent("Are you sure you want to fire the nuke?")
            .AddComponents(new DiscordComponent[]
            {
                new DiscordButtonComponent(ButtonStyle.Danger, "nuke_this_shit", null, false, new DiscordComponentEmoji(921703962455994409)),
                new DiscordButtonComponent(ButtonStyle.Secondary, "better_keep_calm", null, false, new DiscordComponentEmoji(762027583893798977)),
            });

        var message = await builder.SendAsync(ctx.Channel);

        var result = await message.WaitForButtonAsync(ctx.User);
        var interactionId = result.Result.Id;

        await result.Result.Interaction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
        await message.DeleteAsync();

        if (interactionId == "better_keep_calm")
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Nuking has been aborted!"));

            return;
        }

        for (int i = 10; i > -1; i--)
        {
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Nuke will be launched in: {i} seconds."));
            await Task.Delay(1000);
        }

        await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Launching nuke:\nhttps://cdn.discordapp.com/attachments/886626413816479785/921707040693448714/FGNuke.gif"));

        var messages = await ctx.Channel.GetMessagesAsync(500);
        var users = messages
            .Where(x => x.CreationTimestamp > DateTimeOffset.Now.AddMinutes(-1 * minutesInPast))
            .DistinctBy(x => x.Author.Id)
            .Where(x => !x.Author.IsBot)
            .Select(x => x.Author.Id);

        int nukedCount = 0;

        var sb = new StringBuilder();

        foreach (var userId in users)
        {
            try
            {
                var user = await ctx.Guild.GetMemberAsync(userId);
                await user.TimeoutAsync(DateTimeOffset.Now.AddMinutes(nuclearFalloutTime));
                sb.Append(user.Mention);
                sb.Append(" ");
                nukedCount++;
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error nuking user!");
            }
        }

        if (nukedCount > 0) {
            await new DiscordMessageBuilder()
                .WithContent($"The following members could not escape the nuke: {sb.ToString()} \n https://giphy.com/gifs/h5Dnf37npfhYQhfFLK")
                .SendAsync(ctx.Channel);
        }
        else
        {
            await new DiscordMessageBuilder()
                .WithContent($"No member were hit by the nuke!")
                .SendAsync(ctx.Channel);
        }
    }
}
