﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Attributes;
using FreeGuardBot.Services;
using FreeGuardBot.Services.Infractions;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.SlashCommands
{
    [SlashCommandGroup("timeout", "Commands for timeout management")]
    public class TimeoutAppCommand : LogableApplicationCommandModule
    {
        private readonly IInfractionService _infractionService;

        private enum TimeoutAction
        {
            Set,
            Revoke,
        }

        public TimeoutAppCommand(ICommandLogService commandLogService, IInfractionService infractionService) : base(commandLogService)
        {
            _infractionService = infractionService;
        }

        [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.UseTimeout)]
        [SlashCommand("set", "Sets a timeout for a user.")]
        public async Task TimeoutUser(InteractionContext ctx,
            [Option("member", "Member to timeout.")] DiscordUser user,
            [Option("duration", "Duration in minutes.")] long durationInMinutes,
            [Option("reason", "Reason for the timeout.")] string reason)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.TimeoutLogChannel, "set", $"user:{user.Username}#{user.Discriminator};minutes:{durationInMinutes};reason:{reason}");

            var newDate = DateTimeOffset.UtcNow.AddMinutes(durationInMinutes);
            long unixTimestamp = (long)(newDate.UtcDateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

            try
            {
                var member = await ctx.Guild.GetMemberAsync(user.Id);
                await member.TimeoutAsync(newDate, reason);
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{ctx.Member.Mention} gave {member.Mention} a timeout until <t:{unixTimestamp}:f>"));

                FreeGuardDbContext dbContext = new();

                var infractions = await _infractionService.CreateInfractionsAsync(dbContext, Infraction.PunishmentType.Timeout, ctx.Guild.Id, ctx.Member.Id, reason, new List<ulong>() { user.Id });
                await _infractionService.LogInfractionsAsync(ctx, dbContext, infractions);

                await SendNotificationAsync(ctx, user as DiscordMember, TimeoutAction.Set, durationInMinutes, reason);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "error while timeouting user");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Could not timeout user!"));
            }

        }

        [CheckUserAccessSlashCommand(Rights.ManageBot, Rights.UseTimeout)]
        [SlashCommand("revoke", "revokes a timeout")]
        public async Task RevokeTimeoutUser(InteractionContext ctx,
            [Option("member", "Member to timeout.")] DiscordUser user,
            [Option("reason", "Reason for the timeout.")] string reason)
        {
            await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
            await _commandLogService.LogCommandExecutionAsync(ctx, Services.ChannelType.TimeoutLogChannel, "revoke", $"user:{user.Username}#{user.Discriminator};reason:{reason}");

            try
            {
                var member = await ctx.Guild.GetMemberAsync(user.Id);
                await member.TimeoutAsync(DateTimeOffset.UtcNow, reason);
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"{ctx.Member.Mention} revoked {member.Mention}s timeout."));

                await SendNotificationAsync(ctx, user as DiscordMember, TimeoutAction.Revoke, 0, reason);
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "error while timeouting user");
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Could not revoke timeout of user!"));
            }

        }

        private async Task SendNotificationAsync(InteractionContext ctx, DiscordMember affectedUser, TimeoutAction timeoutAction, long duration, string reason)
        {
            var dmChannel = await affectedUser.CreateDmChannelAsync();

            switch (timeoutAction)
            {
                case TimeoutAction.Set:
                    await dmChannel.SendMessageAsync($"You have been timeouted by `{ctx.Member.Username}#{ctx.Member.Discriminator}` for `{duration}` minutes on `{ctx.Guild.Name}` with reason `{reason}`.");
                    break;
                case TimeoutAction.Revoke:
                    await dmChannel.SendMessageAsync($"Your timeout has been lifted by `{ctx.Member.Username}#{ctx.Member.Discriminator}` on `{ctx.Guild.Name}` with reason `{reason}`.");
                    break;
            }
        }
    }
}
