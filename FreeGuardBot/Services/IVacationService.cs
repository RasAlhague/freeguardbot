﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface IVacationService
    {
        Task<bool> CheckForExistingVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId);
        Task<Vacation> AddVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int freezeTitleId, int guildId, string reason, int? estimatedDuration = null);
        Task RemoveVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId);
        Task<Vacation> GetLastVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId);
        Task<IEnumerable<Vacation>> GetLastVacationsAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId, int numOfResults = 0);
        Task<IEnumerable<Vacation>> GetAllVacationsAsync(FreeGuardDbContext dbContext, int guildId);
    }
}
