using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Microsoft.Extensions.Logging;

namespace FreeGuardBot.Services
{
    public class GttsService : ITtsService
    {
        private readonly ICommandLogService _commandLogService;
        public Language DefaultLanguage { get; private set; }

        public GttsService(ICommandLogService commandLogService)
        {
            _commandLogService = commandLogService;
        }

        public string CreateTtsFile(CommandContext ctx, string text, Language language)
        {
            var guid = Guid.NewGuid();
            string ttsFileName = $"./tts/tts_{guid}.mp3";
            string args = $"\"{text}\" {Translate(language)} --output {ttsFileName}";

            if(!Directory.Exists("./tts")) 
            {
                Directory.CreateDirectory("./tts");
            }

            ctx.Client.Logger.LogInformation($"Executing 'gtts-cli {args}'...");
            using var process = new Process();
            process.StartInfo = new ProcessStartInfo()
            {
                FileName = "gtts-cli",
                Arguments = args
            };

            process.Start();;
            process.WaitForExit();

            if (process.ExitCode != 0) 
            {
                ctx.Client.Logger.LogWarning($"Tts file creating process ended with a {process.ExitCode}.");
                return null;
            }
                        
            ctx.Client.Logger.LogInformation($"Created tts file: {ttsFileName} with text {text} for user {ctx.Member.Id}:{ctx.Member.DisplayName}.");

            return ttsFileName;
        }

        public async Task SendTtsFileAsync(CommandContext ctx, string text, Language language)
        {
            await _commandLogService.LogCommandExecutionAsync(ctx);
            string ttsFileName = CreateTtsFile(ctx, text, language);

            if (string.IsNullOrEmpty(ttsFileName))
            {
                await ctx.RespondAsync("A error occured while trying to create the wav!");
                return;
            }

            using (var fs = new FileStream(ttsFileName, FileMode.Open))
            {
                var messageBuilder = new DiscordMessageBuilder()
                    .WithContent("Your text: ")
                    .WithFile("tts.wav", fs)
                    .WithReply(ctx.Message.Id);

                await messageBuilder.SendAsync(ctx.Channel);
            }

            File.Delete(ttsFileName);
        }

        private string Translate(Language lang) 
        {
            return lang switch {
                Language.Briish => "--lang en --tld co.uk",
                Language.English => "--lang en --tld com",
                Language.French => "--lang fr --tld fr",
                Language.German => "--lang de --tld de",
                Language.Italian => "--lang it --tld it",
                Language.Spanish => "--lang es --tld es",
                Language.Kebab => "--lang tr --tld com.tr",
                Language.Polish => "--lang pl --tld pl",
                Language.Japanese => "--lang ja --tld jp",
                _  => "--lang en --tld com",
            };
        }

        public void ChangeLanguage(Language language)
        {
            DefaultLanguage = language;
        }

        public void ChangeLanguage(string language)
        {
            DefaultLanguage = language switch {
                "en-GB" => Language.Briish,
                "en-US" => Language.English,
                "fr-FR" => Language.French,
                "de-DE" => Language.German,
                "it-IT" => Language.Italian,
                "es-ES" => Language.Spanish,
                "tr-TR" => Language.Kebab,
                "pl-PL" => Language.Polish,
                "ja-JP" => Language.Japanese,
                _ => Language.English,
            };
        }
    }
}