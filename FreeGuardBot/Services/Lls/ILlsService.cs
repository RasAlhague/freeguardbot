﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.Lls;

public interface ILlsService
{
    Task<string> UploadImageAsync(string url, string extension);

    string CreateZipAsync(string name);
}
