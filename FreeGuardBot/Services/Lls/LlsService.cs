﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.Lls;

public class LlsService : ILlsService
{
    private const string ImagePath = "./images";
    private const string ZipFilePath = "./zipfiles";

    public string CreateZipAsync(string name)
    {
        if (!Directory.Exists(ZipFilePath))
        {
            Directory.CreateDirectory(ZipFilePath);
        }

        string zipPath = Path.Combine(ZipFilePath, name);

        if (File.Exists(zipPath))
        {
            File.Delete(zipPath);
        }

        ZipFile.CreateFromDirectory(ImagePath, zipPath);

        return zipPath;
    }

    public async Task<string> UploadImageAsync(string url, string extension)
    {
        using var httpClient = new HttpClient();
        
        var fileBytes = await httpClient.GetByteArrayAsync(url);

        string filePath = Path.Combine(ImagePath, $"{Guid.NewGuid()}{extension}");

        File.WriteAllBytes(filePath, fileBytes);

        return filePath;
    }

    
}
