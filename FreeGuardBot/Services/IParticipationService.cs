﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface IParticipationService
    {
        Task<bool> EnrollAsync(FreeGuardDbContext dbContext, Discorduser participant, int quizId);
        Task<bool> KickAsync(FreeGuardDbContext dbContext, int participantId, int quizId);
        Task AnswerAsync(FreeGuardDbContext dbContext, int participantId, int questionId, int answerId);
    }
}
