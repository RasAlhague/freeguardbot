﻿using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    /// <summary>
    /// Record for holding data for grouped count of messages of a user.
    /// </summary>
    /// <param name="userId">The user for which the messages have been counted</param>
    /// <param name="countOfMessages">The number of messages found.</param>
    public readonly record struct ActivityCount(ulong userId, int countOfMessages);

    public interface IActivityService
    {
        Task<IEnumerable<MessageActivityLogEntry>> GetChatActivityForMemberAsync(FreeGuardDbContext dbContext, ulong discordUserId, ulong guildId);
        Task<IEnumerable<ActivityCount>> GetTopChatActivityAsync(FreeGuardDbContext dbContext, IEnumerable<DiscordUser> guildMembers, ulong guildId, int max, OrderDirection orderDirection);
    }
}
