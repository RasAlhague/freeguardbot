using System.Threading.Tasks;
using DSharpPlus.CommandsNext;

namespace FreeGuardBot.Services
{
    public enum Language 
    {
        German,
        Briish,
        English,
        Italian,
        Spanish,
        French,
        Kebab,
        Polish,
        Japanese,
    }

    public interface ITtsService
    {
        Language DefaultLanguage { get; }
        Task SendTtsFileAsync(CommandContext ctx, string text, Language language);
        string CreateTtsFile(CommandContext ctx, string text, Language language);
        void ChangeLanguage(Language language);
        void ChangeLanguage(string language);
    }
}

