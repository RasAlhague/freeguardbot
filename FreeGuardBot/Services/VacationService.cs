﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class VacationService : IVacationService
    {
        public async Task<Vacation> AddVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int freezeTitleId, int guildId, string reason, int? estimatedDuration = null)
        {
            Vacation vacation = new()
            {
                CreateDate = DateTime.UtcNow,
                CreateUserId = discorduser.DiscordUserId,
                AffectedId = discorduser.DiscordUserId,
                EstimatedDuration = estimatedDuration,
                Finished = false.ToString(),
                FreezeTitleId = freezeTitleId,
                GuildId = guildId,
                ModifyDate = null,
                ModifyUserId = null,
                Reason = reason,
                StartDate = DateTime.UtcNow
            };

            dbContext.Vacations.Add(vacation);
            await dbContext.SaveChangesAsync();

            return vacation;
        }

        public async Task<bool> CheckForExistingVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId)
        {
            return dbContext.Vacations
                .Any(x => x.Finished == false.ToString() 
                  && x.AffectedId == discorduser.DiscordUserId 
                  && x.GuildId == guildId);
        }

        public async Task<IEnumerable<Vacation>> GetAllVacationsAsync(FreeGuardDbContext dbContext, int guildId)
        {
            return await dbContext.Vacations
                .Where(x => x.GuildId == guildId)
                .Where(x => x.Finished == false.ToString())
                .ToListAsync();
        }

        public async Task<Vacation> GetLastVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId)
        {
            return await dbContext.Vacations
                .LastOrDefaultAsync(x => x.AffectedId == discorduser.DiscordUserId 
                                 && x.GuildId == guildId 
                                 && x.Finished == false.ToString());
        }

        public async Task<IEnumerable<Vacation>> GetLastVacationsAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId, int numOfResults = 0)
        {
            var vacations = await dbContext.Vacations
                .Where(x => x.AffectedId == discorduser.DiscordUserId && x.GuildId == guildId)
                .OrderByDescending(x => x.CreateDate)
                .ToListAsync();
            
            if(numOfResults >= 0)
            {
                return vacations.Take(numOfResults);
            }

            return vacations;
        }

        public async Task RemoveVacationAsync(FreeGuardDbContext dbContext, Discorduser discorduser, int guildId)
        {
            Vacation vacation = await dbContext.Vacations
                .FirstOrDefaultAsync(x => x.Finished == false.ToString() 
                                  && x.AffectedId == discorduser.DiscordUserId 
                                  && x.GuildId == guildId);

            vacation.Finished = true.ToString();
            vacation.ModifyDate = DateTime.UtcNow;
            vacation.ModifyUserId = discorduser.DiscordUserId;

            dbContext.Vacations.Update(vacation);
            await dbContext.SaveChangesAsync();
        }
    }
}
