﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class QuizResultService : IQuizResultService
    {
        public Task<IEnumerable<IQuizResult>> GetFinalResultAsync(FreeGuardDbContext dbContext, int quizId)
        {
            throw new NotImplementedException();
        }

        public Task<IQuizResult> GetResultAsync(FreeGuardDbContext dbContext, int quizId, ulong userId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IQuizResult>> GetResultsAsync(FreeGuardDbContext dbContext, int quizId)
        {
            throw new NotImplementedException();
        }
    }
}
