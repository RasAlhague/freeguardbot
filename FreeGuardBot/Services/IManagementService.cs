﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public interface IManagementService
{
    Task<IEnumerable<Botright>> GetRightsAsync(FreeGuardDbContext dbContext);
    Task<Botright> GetRightAsync(FreeGuardDbContext context, Rights right);
    Task<IEnumerable<Role>> GetRolesForRightAsync(FreeGuardDbContext dbContext, ulong guildId, Rights right);

    Task SetRightForRoleAsync(FreeGuardDbContext dbContext, ulong guildId, Rights right, ulong roleId, ulong userId);
    Task<bool> DeleteRightForRoleAsync(FreeGuardDbContext dbContext, ulong guildId, Rights right, ulong roleId, ulong userId);
    Task UpdateChannelAsync(FreeGuardDbContext context, ulong guildId, ulong? channelId, ChannelType channel);
}
