﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface IQuizResultService
    {
        Task<IQuizResult> GetResultAsync(FreeGuardDbContext dbContext, int quizId, ulong userId);
        Task<IEnumerable<IQuizResult>> GetResultsAsync(FreeGuardDbContext dbContext, int quizId);
        Task<IEnumerable<IQuizResult>> GetFinalResultAsync(FreeGuardDbContext dbContext, int quizId);
    }
}
