﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

/// <summary>
/// 
/// </summary>
public interface ILeaderboardActionService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="guildId"></param>
    /// <param name="targetUserId"></param>
    /// <param name="amount"></param>
    /// <param name="reason"></param>
    /// <param name="createUserId"></param>
    /// <returns></returns>
    Task GrantPointsAsync(FreeGuardDbContext context, ulong guildId, ulong targetUserId, long amount, string reason, ulong createUserId);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="guildId"></param>
    /// <param name="targetUserId"></param>
    /// <param name="amount"></param>
    /// <param name="reason"></param>
    /// <param name="createUserId"></param>
    /// <returns></returns>
    Task RevokePointsAsync(FreeGuardDbContext context, ulong guildId, ulong targetUserId, long amount, string reason, ulong createUserId);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="guildId"></param>
    /// <param name="targetUserIds"></param>
    /// <param name="amount"></param>
    /// <param name="reason"></param>
    /// <param name="createUserId"></param>
    /// <returns></returns>
    Task GroupGrantPointsAsync(FreeGuardDbContext context, ulong guildId, IEnumerable<ulong> targetUserIds, long amount, string reason, ulong createUserId);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    Task<IEnumerable<LeaderboardAction>> GetAllActionsAsync(FreeGuardDbContext context);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="guildId"></param>
    /// <returns></returns>
    Task<IEnumerable<LeaderboardAction>> GetAllActionsFromGuild(FreeGuardDbContext context, ulong guildId);
}
