﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public enum SocialCreditState
    {
        Positiv,
        Negativ
    }

    public interface ISocialCreditService
    {
        (long, long, string) BetSocialCredits(ulong userId);
        long GetSocialCreditInfo(ulong userId);
        long Deduct(ulong userId, int value);
    }
}
