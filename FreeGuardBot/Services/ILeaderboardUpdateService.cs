﻿using DSharpPlus;
using DSharpPlus.Entities;
using FreeGuardBot.Builders;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public interface ILeaderboardUpdateService
{
    Task RunUpdaterAsync(DiscordClient discordClient, TimeSpan interval, CancellationToken cancelSource);

    Task<DiscordEmbedBuilder> CreateLeaderboardEmbedAsync(DiscordClient client, Leaderboard leaderboard, IEnumerable<GroupedLeaderboardAction> groupedActions, int userAmount);
}
