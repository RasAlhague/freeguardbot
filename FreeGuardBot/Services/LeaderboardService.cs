﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public class LeaderboardService : ILeaderboardService
{
    public async Task<Leaderboard> CreateLeaderboardAsync(FreeGuardDbContext context, Leaderboard leaderboard)
    {
        var addedLb = await context.Leaderboards.AddAsync(leaderboard);
        await context.SaveChangesAsync();

        return addedLb.Entity;
    }

    public async Task DeleteLeaderboardAsync(FreeGuardDbContext context, ulong guildId, int leaderboardId)
    {
        var guild = await Guild.GetAsync(context, guildId);
        var leaderboard = await context.Leaderboards.FirstOrDefaultAsync(x => x.GuildId == guild.Id && x.Id == leaderboardId);

        if (leaderboard != null)
        {
            context.Leaderboards.Remove(leaderboard);
            await context.SaveChangesAsync();
        }
    }

    public async Task<Leaderboard> GetLeaderboardAsync(FreeGuardDbContext context, ulong guildId, int leaderboardId)
    {
        var guild = await Guild.GetAsync(context, guildId);
        var leaderboard = await context.Leaderboards.FirstOrDefaultAsync(x => x.GuildId == guild.Id && x.Id == leaderboardId);

        return leaderboard;
    }

    public async Task<IEnumerable<Leaderboard>> GetLeaderboardsAsync(FreeGuardDbContext context, ulong guildId)
    {
        var guild = await Guild.GetAsync(context, guildId);
        var leaderboards = await context.Leaderboards.Where(x => x.GuildId == guild.Id).ToListAsync();

        return leaderboards;
    }
}
