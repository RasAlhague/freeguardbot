﻿using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.Infractions;

public class InfractionService : IInfractionService
{
    public async Task<IEnumerable<Infraction>> CreateInfractionsAsync(FreeGuardDbContext context, Infraction.PunishmentType punishment, ulong guildId, ulong punisherId, string reason, IEnumerable<ulong> punishedIds)
    {
        var guild = await Guild.GetAsync(context, guildId);

        var infractions = new List<Infraction>();

        foreach (var punishedId in punishedIds)
        {
            Infraction infraction = await CreateInfraction(context, punishment, punisherId, reason, punishedId, guild);

            infractions.Add(infraction);
        }

        return infractions;
    }

    private async Task<Infraction> CreateInfraction(FreeGuardDbContext context, Infraction.PunishmentType punishment, ulong punisherId, string reason, ulong punishedId, Guild guild)
    {
        await Discorduser.GetOrCreateDiscorduserAsync(context, punishedId);
        await Guilduser.GetOrCreateGuildUserAsync(context, guild.Id, punishedId);

        var infraction = new Infraction()
        {
            GuildId = guild.Id,
            PunisherId = punisherId,
            Reason = reason,
            PunishedId = punishedId,
            CreateDate = DateTime.UtcNow,
            Punishment = punishment
        };

        await context.Infractions.AddAsync(infraction);
        await context.SaveChangesAsync();

        return infraction;
    }

    public async Task LogInfractionsAsync(InteractionContext ctx, FreeGuardDbContext dbCtx, IEnumerable<Infraction> infractions)
    {
        var guild = await Guild.GetAsync(dbCtx, ctx.Guild.Id);

        if (guild.InfractionLoggingChannel is null)
        {
            return;
        }

        var channel = ctx.Guild.GetChannel(guild.InfractionLoggingChannel.Value);

        foreach (var infraction in infractions)
        {
            var punished = await ctx.Client.GetUserAsync(infraction.PunishedId);
            var embed = CreateInfractionEmbed(punished, ctx.Member, infraction);

            await channel.SendMessageAsync(embed);
        }
    }

    private DiscordEmbed CreateInfractionEmbed(DiscordUser punishedUser, DiscordUser punisher, Infraction infraction)
    {
        var embedBuilder = new DiscordEmbedBuilder()
            .WithTitle($"Infraction #{infraction.Id}:")
            .AddField("Punished:", punishedUser.Mention, true)
            .AddField("Punisher:", punisher.Mention, true)
            .AddField("Punishment:", infraction.Punishment.ToString())
            .AddField("Reason:", infraction.Reason)
            .WithTimestamp(infraction.CreateDate);

        return embedBuilder.Build();
    }
}
