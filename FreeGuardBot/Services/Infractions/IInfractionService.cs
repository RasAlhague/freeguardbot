﻿using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.Infractions;

public interface IInfractionService
{
    Task<IEnumerable<Infraction>> CreateInfractionsAsync(FreeGuardDbContext context, Infraction.PunishmentType punishment, ulong guildId, ulong punisherId, string reason, IEnumerable<ulong> punishedIds);
    Task LogInfractionsAsync(InteractionContext ctx, FreeGuardDbContext dbCtx, IEnumerable<Infraction> infractions);
}
