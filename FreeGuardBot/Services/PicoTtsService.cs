using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Microsoft.Extensions.Logging;

namespace FreeGuardBot.Services
{
    public class PicoTtsService : ITtsService
    {
        private readonly ICommandLogService _commandLogService;

        public Language DefaultLanguage { get; private set; }

        public PicoTtsService(ICommandLogService commandLogService)
        {
            _commandLogService = commandLogService;
        }

        public string CreateTtsFile(CommandContext ctx, string text, Language language)
        {
            var guid = Guid.NewGuid();
            string ttsFileName = $"./tts/tts_{guid}.wav";
            string args = $"--lang={Translate(language)} --wave={ttsFileName} {EscapeTtsText(text)}";

            if(!Directory.Exists("./tts")) 
            {
                Directory.CreateDirectory("./tts");
            }

            ctx.Client.Logger.LogInformation($"Executing 'pico2wave {args}'...");
            using var process = new Process();
            process.StartInfo = new ProcessStartInfo()
            {
                FileName = "pico2wave",
                Arguments = args
            };

            process.Start();;
            process.WaitForExit();

            if (process.ExitCode != 0) 
            {
                ctx.Client.Logger.LogWarning($"Tts file creating process ended with a {process.ExitCode}.");
                return null;
            }
                        
            ctx.Client.Logger.LogInformation($"Created tts file: {ttsFileName} with text {text} for user {ctx.Member.Id}:{ctx.Member.DisplayName}.");

            return ttsFileName;
        }

        public async Task SendTtsFileAsync(CommandContext ctx, string text, Language language)
        {
            await _commandLogService.LogCommandExecutionAsync(ctx);
            string ttsFileName = CreateTtsFile(ctx, text, language);

            if (string.IsNullOrEmpty(ttsFileName))
            {
                await ctx.RespondAsync("A error occured while trying to create the wav!");
                return;
            }

            using (var fs = new FileStream(ttsFileName, FileMode.Open))
            {
                var messageBuilder = new DiscordMessageBuilder()
                    .WithContent("Your text: ")
                    .WithFile("tts.wav", fs)
                    .WithReply(ctx.Message.Id);

                await messageBuilder.SendAsync(ctx.Channel);
            }

            File.Delete(ttsFileName);
        }

        private string Translate(Language lang) 
        {
            return lang switch {
                Language.Briish => "en-GB",
                Language.English => "en-US",
                Language.French => "fr-FR",
                Language.German => "de-DE",
                Language.Italian => "it-IT",
                Language.Spanish => "es-ES",
                _  => "en-US",
            };
        }

        private string EscapeTtsText(string text) 
        {
            text = text.Replace('"', '\'');
            text = $"\"{text}\"";
            return text;
        }

        public void ChangeLanguage(Language language)
        {
            DefaultLanguage = language;
        }

        public void ChangeLanguage(string language)
        {
            DefaultLanguage = language switch {
                "en-GB" => Language.Briish,
                "en-US" => Language.English,
                "fr-FR" => Language.French,
                "de-DE" => Language.German,
                "it-IT" => Language.Italian,
                "es-ES" => Language.Spanish,
                _ => Language.English,
            };
        }
    }
}