﻿using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface IRoleSelectionService
    {
        Task<(RoleSelect, CreationResult)> CreateRoleSelectionAsync(FreeGuardDbContext context, string description, ulong guildId, ulong channelId, ulong messageId, ulong createUserId);
        Task<CreationResult> AddRoleAsync(FreeGuardDbContext context, string message, ulong guildId, ulong emojiId, string emojiName, ulong roleId, ulong createUserId);
        Task<DeletionResult> RemoveRoleAsync(FreeGuardDbContext context, ulong guildId, ulong role);
    }

    public enum CreationResult
    {
        Success,
        Duplicate,
        Error
    }

    public enum DeletionResult
    {
        Success,
        NotFound,
        Error,
    }
}
