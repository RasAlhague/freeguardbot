﻿using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class ActivityService : IActivityService
    {
        public async Task<IEnumerable<MessageActivityLogEntry>> GetChatActivityForMemberAsync(FreeGuardDbContext dbContext, ulong discordUserId, ulong guildId)
        {
            var member = await Discorduser.GetOrCreateDiscorduserAsync(dbContext, discordUserId);
            var guild = await Guild.GetAsync(dbContext, guildId);     

            return await dbContext.MessageActivityLogEntries
                .Where(x => x.WriterId == member.DiscordUserId && x.GuildId == guild.Id)
                .ToListAsync();
        }

        public async Task<IEnumerable<ActivityCount>> GetTopChatActivityAsync(FreeGuardDbContext dbContext, IEnumerable<DiscordUser> guildMembers, ulong guildId, int max, OrderDirection orderDirection)
        {
            var guild = await Guild.GetAsync(dbContext, guildId);

            var messageActivityQuery = await dbContext.MessageActivityLogEntries
                .Where(x => x.GuildId == guild.Id)
                .GroupBy(x => x.WriterId)
                .Select(x => new ActivityCount(x.Key, x.Count()))
                .ToListAsync();

            var membersNotInDb = guildMembers
                .Where(x => !messageActivityQuery.Any(y => y.userId == x.Id))
                .Select(x => new ActivityCount(x.Id, 0));

            messageActivityQuery.AddRange(membersNotInDb);

            var orderedData = orderDirection switch
            {
                OrderDirection.Ascending => messageActivityQuery.OrderBy(x => x.countOfMessages),
                OrderDirection.Descending => messageActivityQuery.OrderByDescending(x => x.countOfMessages),
            };

            return orderedData.Take(max);
        }

    }
}
