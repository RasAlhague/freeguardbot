﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public interface ILeaderboardService
{
    Task<Leaderboard> CreateLeaderboardAsync(FreeGuardDbContext context, Leaderboard leaderboard);
    Task DeleteLeaderboardAsync(FreeGuardDbContext context, ulong guildId, int leaderboardId);
    Task<Leaderboard> GetLeaderboardAsync(FreeGuardDbContext context, ulong guildId, int leaderboardId);
    Task<IEnumerable<Leaderboard>> GetLeaderboardsAsync(FreeGuardDbContext context, ulong guildId);
}
