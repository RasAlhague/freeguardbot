﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class PollService : IPollService
    {
        public async Task<Poll> CreatePollAsync(FreeGuardDbContext dbContext, Poll poll)
        {
            await dbContext.Polls.AddAsync(poll);
            await dbContext.SaveChangesAsync();

            return await dbContext.Polls
                .Include(x => x.PollEmojis)
                .ThenInclude(x => x.Emoji)
                .FirstOrDefaultAsync(x => x.Id == poll.Id);
        }

        public async Task<PollResult> CreatePollResultAsync(FreeGuardDbContext dbContext, int pollId, ulong userId, int guildId, IEnumerable<Vote> votes)
        {
            var pollResult = new PollResult();
            pollResult.CreateDate = DateTime.UtcNow;
            pollResult.CreateUserId = userId;
            pollResult.GuildId = guildId;
            pollResult.PollId = pollId;

            foreach (var vote in votes)
            {
                pollResult.Votes.Add(vote);
            }

            await dbContext.PollResults.AddAsync(pollResult);
            await dbContext.SaveChangesAsync();

            return pollResult;
        }

        public async Task<IEnumerable<Poll>> GetPollsAsync(FreeGuardDbContext dbContext, int guildId, ulong userId)
        {
            var polls = dbContext.Polls.Where(x => x.GuildId == guildId && x.CreateUserId == userId)
                .Include(x => x.PollEmojis)
                .ThenInclude(x => x.Emoji)
                .Include(x => x.PollResults);

            return await polls.ToListAsync();
        }

        public async Task<PollResult> GetResultAsync(FreeGuardDbContext dbContext, int guildId, int resultId, ulong userId)
        {
            var pollResults = dbContext.PollResults
                .Where(x => x.GuildId == guildId && x.Id == resultId && x.CreateUserId == userId)
                .Include(x => x.Poll)
                .Include(x => x.Votes)
                .ThenInclude(x => x.PollEmoji)
                .ThenInclude(x => x.Emoji);

            return await pollResults.FirstOrDefaultAsync();
        }
    }
}
