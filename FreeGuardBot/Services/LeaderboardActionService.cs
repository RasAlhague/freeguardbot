﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public class LeaderboardActionService : ILeaderboardActionService
{
    public async Task<IEnumerable<LeaderboardAction>> GetAllActionsAsync(FreeGuardDbContext context)
    {
        return await context.LeaderboardActions.ToListAsync();
    }

    public async Task<IEnumerable<LeaderboardAction>> GetAllActionsFromGuild(FreeGuardDbContext context, ulong guildId)
    {
        var guild = await Guild.GetAsync(context, guildId);

        return await context.LeaderboardActions.Where(x => x.GuildId == guild.Id).ToListAsync();
    }

    public async Task GrantPointsAsync(FreeGuardDbContext context, ulong guildId, ulong targetUserId, long amount, string reason, ulong createUserId)
    {
        if (amount < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount));
        }

        await OffsetPointsAsync(context, guildId, targetUserId, amount, reason, createUserId);
    }

    public async Task GroupGrantPointsAsync(FreeGuardDbContext context, ulong guildId, IEnumerable<ulong> targetUserIds, long amount, string reason, ulong createUserId)
    {
        foreach (var targetUserId in targetUserIds)
        {
            await GrantPointsAsync(context, guildId, targetUserId, amount, reason, createUserId);
        }
    }

    public async Task RevokePointsAsync(FreeGuardDbContext context, ulong guildId, ulong targetUserId, long amount, string reason, ulong createUserId)
    {
        if (amount < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount));
        }

        await OffsetPointsAsync(context, guildId, targetUserId, -amount, reason, createUserId);
    }

    private async Task OffsetPointsAsync(FreeGuardDbContext context, ulong guildId, ulong targetUserId, long amount, string reason, ulong createUserId)
    {
        var guild = await Guild.GetAsync(context, guildId);

        var action = new LeaderboardAction()
        {
            AffectedUserId = targetUserId,
            GuildId = guild.Id,
            Points = amount,
            Reason = reason,
            CreateUserId = createUserId,
            CreateDate = DateTime.Now,
        };

        await context.LeaderboardActions.AddAsync(action);
        await context.SaveChangesAsync();
    }
}
