﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface IQuizService
    {
        Task<Quiz> CreateAsync(FreeGuardDbContext dbContext, Quiz quiz);
        Task<Quiz> CreateFromAsync(FreeGuardDbContext dbContext, Quiz originalQuiz);
        Task<Quiz> CreateFromAsnyc(FreeGuardDbContext dbContext, int originalQuizId);
        Task<Quiz> GetSingleAsync(FreeGuardDbContext dbContext, int quizId);
        Task<IEnumerable<Quiz>> GetOwnAsync(FreeGuardDbContext dbContext, ulong userId);
        Task<IEnumerable<Quiz>> GetParticipationgAsync(FreeGuardDbContext dbContext, ulong userId);
        Task<IEnumerable<Quiz>> UpdateAsync(FreeGuardDbContext dbContext, Quiz quiz);
        Task DeleteAsync(FreeGuardDbContext dbContext, int quizId);
    }
}
