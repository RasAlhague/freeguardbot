﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class SocialCreditService : ISocialCreditService
    {
        private readonly List<(SocialCreditState, string)> _socialCreditMessages;
        private readonly Dictionary<ulong, long> _socialCreditsPerUser;
        private readonly Random _random;

        public SocialCreditService()
        {
            _socialCreditMessages = new List<(SocialCreditState, string)>();

            var lines = File.ReadAllLines("./socialcredittexts.txt");

            foreach (var line in lines)
            {
                var split = line.Split(";");

                _socialCreditMessages.Add((Enum.Parse<SocialCreditState>(split[0]), split[1]));
            }

            _socialCreditsPerUser = new Dictionary<ulong, long>();
            _random = new Random();
        }

        public (long, long, string) BetSocialCredits(ulong userId)
        {
            var randomValue = _random.Next(-10000, 10000);
            var message = GetMessage(randomValue);

            if(_socialCreditsPerUser.TryGetValue(userId, out long value))
            {
                return (value + randomValue, randomValue, message);
            }

            var newCredits = 5000 + randomValue;
            _socialCreditsPerUser.Add(userId, newCredits);

            return (newCredits, randomValue, message);
        }

        public long Deduct(ulong userId, int value)
        {
            if (_socialCreditsPerUser.TryGetValue(userId, out long currentValue))
            {
                return currentValue - value;
            }

            var newCredits = 5000 - value;
            _socialCreditsPerUser.Add(userId, newCredits);

            return newCredits;
        }

        public long GetSocialCreditInfo(ulong userId)
        {
            if (_socialCreditsPerUser.TryGetValue(userId, out long value))
            {
                return value;
            }

            var newCredits = 5000;
            _socialCreditsPerUser.Add(userId, newCredits);

            return newCredits;
        }

        private string GetMessage(int randomValue)
        {
            if (randomValue >= 0)
            {
                var filteredMessages = _socialCreditMessages.Where(x => x.Item1 == SocialCreditState.Positiv).ToList();

                return filteredMessages[_random.Next(0, filteredMessages.Count)].Item2;
            }
            else
            {
                var filteredMessages = _socialCreditMessages.Where(x => x.Item1 == SocialCreditState.Negativ).ToList();

                return filteredMessages[_random.Next(0, filteredMessages.Count)].Item2;
            }
        }
    }
}
