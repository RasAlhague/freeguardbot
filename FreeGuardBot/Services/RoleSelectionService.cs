﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class RoleSelectionService : IRoleSelectionService
    {
        public async Task<CreationResult> AddRoleAsync(FreeGuardDbContext context, string message, ulong guildId, ulong emojiId, string emojiName, ulong roleId, ulong createUserId)
        {
            var guild = await Guild.GetAsync(context, guildId);
            var roleSelect = await context.RoleSelects.FirstOrDefaultAsync(x => x.GuildId == guild.Id);
            var role = await Role.GetOrCreateRoleAsync(context, roleId, createUserId, guild.Id);

            if (roleSelect is null)
            {
                return CreationResult.Error;
            }

            var emoji = await Emoji.GetOrCreate(context, createUserId, emojiId, emojiName);

            if (context.RoleSelectReactions.Any(x => x.RoleId == role.Id && x.EmojiId == emoji.Id))
            {
                return CreationResult.Duplicate;
            }

            var roleSelection = new RoleSelectReaction()
            {
                Message = message,
                EmojiId = emoji.Id,
                RoleId = role.Id,
                RoleSelectId = roleSelect.Id,
                CreateDate = DateTime.UtcNow,
                CreateUserId = createUserId,
            };

            await context.RoleSelectReactions.AddAsync(roleSelection);
            await context.SaveChangesAsync();

            return CreationResult.Success;
        }

        public async Task<(RoleSelect, CreationResult)> CreateRoleSelectionAsync(FreeGuardDbContext context, string description, ulong guildId, ulong channelId, ulong messageId, ulong createUserId)
        {
            var guild = await Guild.GetAsync(context, guildId);

            if (context.RoleSelects.Any(x => x.GuildId == guild.Id))
            {
                return (null, CreationResult.Duplicate);
            }

            var roleSelection = new RoleSelect()
            {
                Description = description,
                GuildId = guild.Id,
                ChannelId = channelId,
                MessageId = messageId,
                CreateUserId = createUserId,
                CreateDate = DateTime.UtcNow,
            };

            await context.RoleSelects.AddAsync(roleSelection);
            await context.SaveChangesAsync();

            return (roleSelection, CreationResult.Success);
        }

        public async Task<DeletionResult> RemoveRoleAsync(FreeGuardDbContext context, ulong guildId, ulong roleId)
        {
            var guild = await Guild.GetAsync(context, guildId);
            var roleSelect = await context.RoleSelects.FirstOrDefaultAsync(x => x.GuildId == guild.Id);
            var role = await context.Roles.FirstOrDefaultAsync(x => x.GuildId == guild.Id && x.DiscordRoleId == roleId);

            if (role is null)
            {
                return DeletionResult.NotFound;
            }

            var roleSelectionReaction = await context.RoleSelectReactions.FirstOrDefaultAsync(x => x.RoleSelectId == roleSelect.Id && x.RoleId == role.Id);

            if (roleSelectionReaction is null)
            {
                return DeletionResult.NotFound;
            }

            context.RoleSelectReactions.Remove(roleSelectionReaction);
            await context.SaveChangesAsync();

            return DeletionResult.Success;
        }
    }
}
