﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public enum ChannelType
    {
        RecruitmentChannel,
        WelcomeMessageChannel,
        TimeoutLogChannel,
        LeaderboardLoggingChannel,
        CommandLoggingChannel,
        InfractionLoggingChannel,
    }
}
