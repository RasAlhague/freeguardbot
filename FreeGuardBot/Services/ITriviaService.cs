﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface ITriviaService
    {
        Task CreateQuestionAsync(TriviaQuestion triviaQuestion, IEnumerable<TriviaAnswer> answers);
        Task<TriviaQuestion> GetRandomQuestionAsync();
    }
}
