﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class TriviaService : ITriviaService
    {
        private readonly Random _r;

        public TriviaService()
        {
            _r = new Random();   
        }

        public async Task CreateQuestionAsync(TriviaQuestion triviaQuestion, IEnumerable<TriviaAnswer> answers)
        {
            using FreeGuardDbContext conn = new FreeGuardDbContext();

            await conn.TriviaQuestions.AddAsync(triviaQuestion);
            await conn.TriviaAnswers.AddRangeAsync(answers);

            await conn.SaveChangesAsync();
        }

        public async Task<TriviaQuestion> GetRandomQuestionAsync()
        {
            using FreeGuardDbContext conn = new FreeGuardDbContext();

            var count = await conn.TriviaQuestions.CountAsync();
            var index = _r.Next(1, count + 1);

            return await conn.TriviaQuestions
                .Include(x => x.DifficultyLevel)
                .Include(x => x.TriviaAnswers)
                .FirstOrDefaultAsync(x => x.Id == index);
        }
    }
}
