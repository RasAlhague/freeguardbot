﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public class ManagementService : IManagementService
{
    public async Task<bool> DeleteRightForRoleAsync(FreeGuardDbContext dbContext, ulong guildId, Rights right, ulong roleId, ulong userId)
    {
        var guild = await dbContext.Guilds
                .SingleOrDefaultAsync(x => x.GuildId == guildId);
        var role = await Role
            .GetOrCreateRoleAsync(dbContext, roleId, userId, guild.Id);
        var botright = await dbContext.Botrights
            .FindAsync((int)right);
        var roleRight = await dbContext.Rolerights
            .SingleOrDefaultAsync(x => x.RoleId == role.Id && x.BotRightId == botright.Id);

        if (roleRight != null)
        {
            dbContext.Rolerights.Remove(roleRight);
            await dbContext.SaveChangesAsync();

            return true;
        }

        return false;
    }

    public async Task<Botright> GetRightAsync(FreeGuardDbContext context, Rights right)
    {
        return await context.Botrights
            .Include(br => br.Rolerights)
            .ThenInclude(rr => rr.Role)
            .FirstOrDefaultAsync(x => x.Id == (int)right);
    }

    public async Task<IEnumerable<Botright>> GetRightsAsync(FreeGuardDbContext dbContext)
    {
        return await dbContext.Botrights
            .Include(br => br.Rolerights)
            .ThenInclude(rr => rr.Role)
            .ToListAsync();
    }

    public async Task<IEnumerable<Role>> GetRolesForRightAsync(FreeGuardDbContext dbContext, ulong guildId, Rights right)
    {
        return await dbContext.Roles
            .Include(x => x.Rolerights)
            .ThenInclude(x => x.BotRight)
            .Where(x => x.Rolerights
                .Any(y => y.BotRightId == (int)right))
            .ToListAsync();
    }

    public async Task SetRightForRoleAsync(FreeGuardDbContext dbContext, ulong guildId, Rights right, ulong roleId, ulong userId)
    {
        var guild = await Guild.GetAsync(dbContext, guildId);
        var role = await Role
            .GetOrCreateRoleAsync(dbContext, roleId, userId, guild.Id);
        var bortright = await dbContext.Botrights
            .FindAsync((int)right);
        var roleRight = await dbContext.Rolerights
            .SingleOrDefaultAsync(x => x.RoleId == role.Id && x.BotRightId == bortright.Id);

        if (roleRight is null)
        {
            roleRight = new Roleright()
            {
                RoleId = role.Id,
                BotRightId = bortright.Id,
                CreateUserId = userId,
                CreateDate = DateTime.UtcNow
            };

            await dbContext.AddAsync(roleRight);
            await dbContext.SaveChangesAsync();
        }
    }

    public async Task UpdateChannelAsync(FreeGuardDbContext context, ulong guildId, ulong? channelId, ChannelType channel)
    {
        var guild = await Guild.GetAsync(context, guildId);

        switch (channel)
        {
            case ChannelType.RecruitmentChannel:
                guild.RecruitmentChannelId = channelId;
                break;
            case ChannelType.WelcomeMessageChannel:
                guild.WelcomeMessageChannel = channelId;
                break;
            case ChannelType.TimeoutLogChannel:
                guild.TimeoutLoggingChannel = channelId;
                break;
            case ChannelType.LeaderboardLoggingChannel:
                guild.LeaderboardLoggingChannel = channelId;
                break;
            case ChannelType.CommandLoggingChannel:
                guild.GeneralCommandLoggingChannel = channelId;
                break;
            case ChannelType.InfractionLoggingChannel:
                guild.InfractionLoggingChannel = channelId;
                break;
            default:
                throw new NotImplementedException("No feature for this channel has been implemented yet.");
        }

        context.Guilds.Update(guild);
        await context.SaveChangesAsync();
    }
}
