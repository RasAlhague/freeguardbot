﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.TextGeneration;

public interface ITextGenService
{
    Task<TextGenResponse> GenerateTextAsync(TextGenRequest request);
    Task<string> GetVersionAsync();
}
