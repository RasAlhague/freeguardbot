﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.TextGeneration;

public class TextGenResponse
{
    [JsonProperty("texts")]
    public List<string> Texts { get; set; }

    public TextGenResponse()
    {
        Texts = new List<string>();
    }
}
