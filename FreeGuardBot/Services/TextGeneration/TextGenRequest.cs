﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.TextGeneration;

public class TextGenRequest
{
    private readonly List<string> _texts;
    
    [JsonProperty("max_length")]
    public long MaxLength { get; set; }
    [JsonProperty("prefix")]
    public string Prefix { get; set; }
    [JsonProperty("texts")]
    public IEnumerable<string> Texts
    {
        get => _texts;
    }

    public TextGenRequest(long maxLength, string prefix)
    {
        _texts = new List<string>();
        MaxLength = maxLength;
        Prefix = prefix;
    }

    public void AddText(string text)
    {
        _texts.Add(text);
    } 
}
