﻿using DSharpPlus.Entities;
using Newtonsoft.Json;
using Org.BouncyCastle.Utilities.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services.TextGeneration;

public class TextGenService : ITextGenService
{
    private readonly HttpClient _httpClient;

    public TextGenService(Uri baseAddress, TimeSpan timeout)
    {
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = baseAddress;
        _httpClient.Timeout = timeout;
    }

    public async Task<TextGenResponse> GenerateTextAsync(TextGenRequest request)
    {
        var json = JsonConvert.SerializeObject(request);
        var resp = await _httpClient.PostAsync("gen_text", new StringContent(json, Encoding.UTF8, "application/json"));

        if (resp.IsSuccessStatusCode)
        {
            var value = await resp.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TextGenResponse>(value);
        }

        return null;
    }

    public async Task<string> GetVersionAsync()
    {
        var resp = await _httpClient.GetAsync("version");

        if (resp.IsSuccessStatusCode)
        {
            return await resp.Content.ReadAsStringAsync();
        }

        return null;
    }
}
