﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface ITicketService
    {
        DiscordMember BotUser { get; set; }
        Task<DiscordChannel> CreateTicketAsync(DiscordGuild guild, DiscordMember user);
        Task CloseTicketAsync(DiscordGuild guild, DiscordMember ticketCloser, DiscordChannel ch);
        Task ArchiveTicketAsync(DiscordGuild guild, DiscordMember ticketArchiver, DiscordChannel ch);
        Task InviteToTicketAsync(DiscordGuild guild, DiscordMember user, int ticketId);
    }
}
