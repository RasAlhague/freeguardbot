﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class ParticipationService : IParticipationService
    {
        public Task AnswerAsync(FreeGuardDbContext dbContext, int participantId, int questionId, int answerId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> EnrollAsync(FreeGuardDbContext dbContext, Discorduser participant, int quizId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> KickAsync(FreeGuardDbContext dbContext, int participantId, int quizId)
        {
            throw new NotImplementedException();
        }
    }
}
