﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class QuizService : IQuizService
    {
        public Task<Quiz> CreateAsync(FreeGuardDbContext dbContext, Quiz quiz)
        {
            throw new NotImplementedException();
        }

        public Task<Quiz> CreateFromAsnyc(FreeGuardDbContext dbContext, int originalQuizId)
        {
            throw new NotImplementedException();
        }

        public Task<Quiz> CreateFromAsync(FreeGuardDbContext dbContext, Quiz originalQuiz)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(FreeGuardDbContext dbContext, int quizId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Quiz>> GetOwnAsync(FreeGuardDbContext dbContext, ulong userId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Quiz>> GetParticipationgAsync(FreeGuardDbContext dbContext, ulong userId)
        {
            throw new NotImplementedException();
        }

        public Task<Quiz> GetSingleAsync(FreeGuardDbContext dbContext, int quizId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Quiz>> UpdateAsync(FreeGuardDbContext dbContext, Quiz quiz)
        {
            throw new NotImplementedException();
        }
    }
}
