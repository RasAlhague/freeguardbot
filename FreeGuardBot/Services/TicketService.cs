﻿using DSharpPlus;
using DSharpPlus.Entities;
using FreeGuardBot.Exceptions;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FreeGuardBot.Services
{
    public class TicketService : ITicketService
    {
        public DiscordMember BotUser { get; set; }

        public async Task<DiscordChannel> CreateTicketAsync(DiscordGuild guild, DiscordMember user)
        {
            DiscordChannel ticketCategory = (await guild.GetChannelsAsync()).FirstOrDefault(x => x.IsCategory && x.Name == "tickets");

            if (ticketCategory is null)
            {
                ticketCategory = await guild.CreateChannelCategoryAsync("tickets", null, null);
            }

            using FreeGuardDbContext dbcontext = new();

            Ticket ticket = await Ticket.CreateTicket(dbcontext, guild, user);
            var dbGuild = await Guild.GetAsync(dbcontext, guild.Id);
            var ticketManagerRoles = await dbcontext.Roles.Where(x => x.IsTicketManager && x.GuildId == dbGuild.Id).ToListAsync();

            var overwrites = await CreateChannelOverwrites(guild, user, ticketManagerRoles);
            DiscordChannel channel = await guild.CreateChannelAsync($"ticket-{user.Username}-{ticket.Id}", DSharpPlus.ChannelType.Text, ticketCategory, $"Ticket channel for {user.Username}", null, null, overwrites);

            await SetTicketChannelAsync(dbcontext, ticket, channel);

            Console.WriteLine("Done setting overwrites!");

            return channel;
        }

        private static async Task SetTicketChannelAsync(FreeGuardDbContext dbcontext, Ticket ticket, DiscordChannel channel)
        {
            ticket.TicketChannelId = channel.Id;
            dbcontext.Tickets.Update(ticket);
            await dbcontext.SaveChangesAsync();
        }

        private async Task<IEnumerable<DiscordOverwriteBuilder>> CreateChannelOverwrites(DiscordGuild guild, DiscordMember user, IEnumerable<Role> ticketManagerRoles)
        {
            var builders = new List<DiscordOverwriteBuilder>();

            builders.Add(new DiscordOverwriteBuilder(guild.EveryoneRole).Deny(Permissions.AccessChannels));
            builders.Add(new DiscordOverwriteBuilder(user).Allow(Permissions.AccessChannels));
            builders.Add(new DiscordOverwriteBuilder(BotUser).Allow(Permissions.AccessChannels));

            foreach (var ticketManagerRole in ticketManagerRoles)
            {
                builders.Add(new DiscordOverwriteBuilder(guild.GetRole(ticketManagerRole.DiscordRoleId)).Allow(Permissions.AccessChannels));
            }

            return builders;
        }

        public async Task InviteToTicketAsync(DiscordGuild guild, DiscordMember user, int ticketId)
        {
            FreeGuardDbContext dbContext = new FreeGuardDbContext();
            Ticket ticket = await dbContext.Tickets.FindAsync(ticketId);

            if (ticket is null)
            {
                throw new TicketNotFoundException($"No ticket with id {ticketId} found!");
            }

            DiscordChannel channel = guild.GetChannel(ticket.TicketChannelId);
            await channel.AddOverwriteAsync(user, Permissions.AccessChannels, Permissions.None, "Invited to this channel by either ticker manager or ticket owner.");
        }

        public async Task ArchiveTicketAsync(DiscordGuild guild, DiscordMember tickerArchiver, DiscordChannel ch)
        {
            FreeGuardDbContext dbContext = new FreeGuardDbContext();
            Ticket ticket = await dbContext.Tickets.FirstOrDefaultAsync(x => x.TicketChannelId == ch.Id);

            if (ticket is null)
            {
                throw new TicketNotFoundException($"No ticket to archive has been found!");
            }

            DiscordChannelArchiver archiver = new DiscordChannelArchiver(ch);
            var xml = await archiver.CreateArchiveXml(100);
            xml.Root.Add(new XAttribute("ticketNr", ticket.Id));
            xml.Root.Add(new XAttribute("ticketOwner", ticket.CreateUserId));

            DiscordChannel archiveCategory = (await guild.GetChannelsAsync()).FirstOrDefault(x => x.IsCategory && x.Name == "archive");

            if (archiveCategory is null)
            {
                archiveCategory = await guild.CreateChannelCategoryAsync("archive", null, null);
            }

            await ch.ModifyAsync(x => x.Parent = archiveCategory);

            ticket.ArchivedMessages = xml.ToString(); 
            ticket.IsArchived = true;
            ticket.Messages = xml.Root.Elements("message").Count();
            ticket.ModifyDate = DateTime.UtcNow;
            ticket.ModifyUserId = tickerArchiver.Id;

            dbContext.Tickets.Update(ticket);
            await dbContext.SaveChangesAsync();
        }

        public async Task CloseTicketAsync(DiscordGuild guild, DiscordMember ticketCloser, DiscordChannel ch)
        {
            FreeGuardDbContext dbContext = new FreeGuardDbContext();
            Ticket ticket = await dbContext.Tickets.FirstOrDefaultAsync(x => x.TicketChannelId == ch.Id);

            if (ticket is null)
            {
                throw new TicketNotFoundException($"No ticket to archive has been found!");
            }

            var removeablePermissions = ch.PermissionOverwrites.Where(x => x.Type == OverwriteType.Member).ToList();

            foreach (var removeablePermission in removeablePermissions)
            {
                var memberToRemove = await removeablePermission.GetMemberAsync();

                try
                {
                    await ch.DeleteOverwriteAsync(memberToRemove);
                }
                catch (Exception)
                {
                }
            }

            ticket.ModifyUserId = ticketCloser.Id;
            ticket.ModifyDate = DateTime.UtcNow;

            dbContext.Tickets.Update(ticket);
            await dbContext.SaveChangesAsync();
        }
    }
}
