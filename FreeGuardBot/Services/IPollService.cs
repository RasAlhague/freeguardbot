﻿using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface IPollService
    {
        Task<IEnumerable<Poll>> GetPollsAsync(FreeGuardDbContext dbContext, int guildId, ulong userId);
        Task<PollResult> GetResultAsync(FreeGuardDbContext dbContext, int guildId, int resultId, ulong userId);
        Task<Poll> CreatePollAsync(FreeGuardDbContext dbContext, Poll poll);
        Task<PollResult> CreatePollResultAsync(FreeGuardDbContext dbContext, int pollId, ulong userId, int guildId, IEnumerable<Vote> votes);
    }
}
