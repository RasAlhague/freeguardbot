﻿using DSharpPlus.CommandsNext;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public interface ICommandLogService
    {
        Task<IEnumerable<Commandinfo>> GetCommandInfosWithNameAsync(FreeGuardDbContext context, ulong guildId, string commandName);
        Task<IEnumerable<Commandinfo>> GetCommandInfosForUserAsync(FreeGuardDbContext context, ulong guildId, ulong userId);
        Task LogCommandUsageAsync(FreeGuardDbContext context, string commandName, string parameters, ulong guildId, ulong createUserId);
        Task LogCommandExecutionAsync(CommandContext ctx, string parameters = null);
        Task LogCommandExecutionAsync(InteractionContext ctx, ChannelType channelType, string subCommandName = null, string parameters = null);
    }
}
