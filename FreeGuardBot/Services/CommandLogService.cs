﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Services
{
    public class CommandLogService : ICommandLogService
    {
        public async Task<IEnumerable<Commandinfo>> GetCommandInfosForUserAsync(FreeGuardDbContext context, ulong guildId, ulong userId)
        {
            var guild = Guild.GetAsync(context, guildId);

            return await context.Commandinfos.Where(x => x.GuildId == guild.Id && x.CreateUserId == userId).ToListAsync();
        }

        public async Task<IEnumerable<Commandinfo>> GetCommandInfosWithNameAsync(FreeGuardDbContext context, ulong guildId, string commandName)
        {
            var guild = Guild.GetAsync(context, guildId);

            return await context.Commandinfos.Where(x => x.GuildId == guild.Id && x.CommandName == commandName).ToListAsync();
        }

        public async Task LogCommandUsageAsync(FreeGuardDbContext context, string commandName, string parameters, ulong guildId, ulong createUserId)
        {
            var guild = await Guild.GetAsync(context, guildId);

            var commandInfo = new Commandinfo()
            {
                CommandName = commandName,
                Parameter = parameters,
                GuildId = guild.Id,
                CreateDate = DateTime.UtcNow,
                CreateUserId = createUserId,
            };

            await context.AddAsync(commandInfo);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Logs the execution of an command into the db.
        /// </summary>
        /// <param name="ctx">The context of the command which should be logged.</param>
        /// <param name="parameters">Additional string paramethers which should be logged.</param>
        /// <returns>No value.</returns>
        public async Task LogCommandExecutionAsync(CommandContext ctx, string parameters = null)
        {
            using FreeGuardDbContext dbContext = new();

            var guild = await Guild.GetOrCreateGuildAsync(dbContext, ctx.Guild.Id, ctx.Guild.Name);

            if (!dbContext.Discordusers.Any(x => x.DiscordUserId == ctx.Client.CurrentUser.Id))
            {
                var botUser = new Discorduser()
                {
                    CreateDate = DateTime.UtcNow,
                    DiscordUserId = ctx.Client.CurrentUser.Id
                };

                dbContext.Discordusers.Add(botUser);
                await dbContext.SaveChangesAsync();


                await Guilduser.GetOrCreateGuildUserAsync(dbContext, guild.Id, botUser.DiscordUserId);
            }

            try
            {
                await LogCommandUsageAsync(dbContext, ctx.Command.QualifiedName, parameters, ctx.Guild.Id, ctx.Member.Id);
                ctx.Client.Logger.LogInformation($"Executing '{ctx.Command.QualifiedName}' for user '{ctx.Member.DisplayName};{ctx.Member.Username}#{ctx.Member.Discriminator}'");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while logging command usage");
            }

            var emoji = DiscordEmoji.FromName(ctx.Client, ":white_check_mark:");
            await ctx.Message.CreateReactionAsync(emoji);
        }

        /// <summary>
        /// Logs the execution of an command into the db.
        /// </summary>
        /// <param name="ctx">The context of the command which should be logged.</param>
        /// <param name="parameters">Additional string paramethers which should be logged.</param>
        /// <returns>No value.</returns>
        public async Task LogCommandExecutionAsync(InteractionContext ctx, ChannelType channelType, string subCommandName = null, string parameters = null)
        {
            using FreeGuardDbContext dbContext = new();

            var guild = await Guild.GetOrCreateGuildAsync(dbContext, ctx.Guild.Id, ctx.Guild.Name);

            if (!dbContext.Discordusers.Any(x => x.DiscordUserId == ctx.Client.CurrentUser.Id))
            {
                var botUser = new Discorduser()
                {
                    CreateDate = DateTime.UtcNow,
                    DiscordUserId = ctx.Client.CurrentUser.Id
                };

                dbContext.Discordusers.Add(botUser);
                await dbContext.SaveChangesAsync();

                await Guilduser.GetOrCreateGuildUserAsync(dbContext, guild.Id, botUser.DiscordUserId);
            }

            try
            {
                var commandName = $"{ctx.CommandName} {subCommandName}";

                await LogCommandUsageAsync(dbContext, commandName, parameters, ctx.Guild.Id, ctx.Member.Id);
                await LogToChannelAsync(dbContext, ctx, channelType, commandName, parameters);
                ctx.Client.Logger.LogInformation($"Executing '{commandName}' for user '{ctx.Member.DisplayName};{ctx.Member.Username}#{ctx.Member.Discriminator}'");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Error while logging command usage");
            }
        }

        private async Task LogToChannelAsync(FreeGuardDbContext context, InteractionContext ctx, ChannelType channelType, string commandName, string parameters)
        {
            var guild = await Guild.GetAsync(context, ctx.Guild.Id);
            var resolvedChannel = channelType switch
            {
                ChannelType.RecruitmentChannel => guild.RecruitmentChannelId,
                ChannelType.WelcomeMessageChannel => guild.WelcomeMessageChannel,
                ChannelType.TimeoutLogChannel => guild.TimeoutLoggingChannel,
                ChannelType.LeaderboardLoggingChannel => guild.LeaderboardLoggingChannel,
                ChannelType.CommandLoggingChannel => guild.GeneralCommandLoggingChannel,
                ChannelType.InfractionLoggingChannel => guild.InfractionLoggingChannel,
                _ => throw new NotImplementedException("Feature not implemented yet for this channle."),
            };

            if (resolvedChannel == null)
            {
                return;
            }

            var channel = ctx.Guild.GetChannel(resolvedChannel.Value);

            var embed = new DiscordEmbedBuilder()
                .WithTitle("Command usage log entry: ")
                .WithDescription($"User {ctx.Member.Mention} used the following in {ctx.Channel.Mention} command.")
                .WithAuthor(ctx.Member.DisplayName, iconUrl: ctx.Member.AvatarUrl)
                .AddField("Commandname:", commandName)
                .AddField("Parameters:", $"`{parameters}`")
                .WithTimestamp(DateTime.UtcNow);

            await channel.SendMessageAsync(embed);
        }
    }
}
