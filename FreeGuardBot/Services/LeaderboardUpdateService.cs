﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;
using FreeGuardBot.Builders;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FreeGuardBot.Services;

public class LeaderboardUpdateService : ILeaderboardUpdateService
{
    private readonly ILeaderboardService _leaderboardService;

    public LeaderboardUpdateService(ILeaderboardService leaderboardService)
    {
        _leaderboardService = leaderboardService;
    }

    public async Task RunUpdaterAsync(DiscordClient client, TimeSpan interval, CancellationToken cancellationToken)
    {
        FreeGuardDbContext dbContext = new();

        while (true)
        {
            client.Logger.LogInformation("Running leaderboard update.");

            var dbGuilds = await dbContext.Guilds.ToListAsync();
            var leaderboardActions = await dbContext.LeaderboardActions.ToListAsync();

            var dsGuilds = new Dictionary<ulong, DiscordGuild>();

            foreach (var dbGuild in dbGuilds)
            {
                try
                {
                    var leaderboards = await _leaderboardService.GetLeaderboardsAsync(dbContext, dbGuild.GuildId);

                    if (!dsGuilds.TryGetValue(dbGuild.GuildId, out DiscordGuild discordGuild))
                    {
                        var tempGuild = await client.GetGuildAsync(dbGuild.GuildId);

                        dsGuilds.Add(dbGuild.GuildId, tempGuild);
                        discordGuild = tempGuild;
                    }

                    foreach (var leaderboard in leaderboards)
                    {
                        await UpdateLeaderboardAsync(client, dbGuild, discordGuild, leaderboard, leaderboardActions);
                    }
                }
                catch (Exception ex)
                {
                    client.Logger.LogError(ex, "Error in leaderboard update.");
                }
            }

            client.Logger.LogInformation("Finished leaderboard update.");

            await Task.Delay(interval, cancellationToken);

            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
        }
    }

    private async Task UpdateLeaderboardAsync(DiscordClient client, Guild dbGuild, DiscordGuild dsGuild, Leaderboard leaderboard, IEnumerable<LeaderboardAction> leaderboardActions)
    {
        var actions = leaderboardActions.Where(a => a.GuildId == dbGuild.Id);

        var channel = dsGuild.GetChannel(leaderboard.DiscordChannelId);
        var message = await channel.GetMessageAsync(leaderboard.MessageId);
        var groupedActions = leaderboard
            .CreateBuilder()
            .AddActions(actions)
            .Build();

        var embed = await CreateLeaderboardEmbedAsync(client, leaderboard, groupedActions, 10);

        await message.ModifyAsync(x => x.WithEmbed(embed));
    }

    public async Task<DiscordEmbedBuilder> CreateLeaderboardEmbedAsync(DiscordClient client, Leaderboard leaderboard, IEnumerable<GroupedLeaderboardAction> groupedActions, int userAmount)
    {
        var embed = new DiscordEmbedBuilder()
            .WithTitle(leaderboard.Name)
            .WithDescription($"**({leaderboard.Id})** View the full leaderboard at the page (should i ever finish it).");

        var sb = new StringBuilder();
        int index = 1;

        if (groupedActions.Any())
        {
            foreach (var groupedAction in groupedActions)
            {
                try
                {
                    var user = await client.GetUserAsync(groupedAction.affectedUser);

                    sb.AppendLine($"**{index++}.** {user.Mention} - `{groupedAction.sumOfPoints}`");
                }
                catch (NotFoundException ex)
                {
                    client.Logger.LogError(ex, "Could not find user.");
                }
            }

            embed.AddField("\u200b", sb.ToString());
        }

        embed.WithFooter($"Last updated (UTC): {DateTime.UtcNow}");

        return embed;
    }
}
