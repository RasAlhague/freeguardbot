using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Services;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FreeGuardBot;

public class ActivityButtonManager
{
    private readonly TimeSpan _updateInterval;
    private readonly IActivityService _activityService;
    private readonly List<ulong> _excludedPeople;

    public ActivityButtonManager(TimeSpan updateInterval, IActivityService activityService, params ulong[] excludedPeople)
    {
        _updateInterval = updateInterval;
        _activityService = activityService;
        _excludedPeople = new List<ulong>();
        _excludedPeople.AddRange(excludedPeople);
    }

    private async Task HandleInteraction(DiscordClient s, ComponentInteractionCreateEventArgs e)
    {
        if (!e.Id.StartsWith("btn_ban") && !e.Id.StartsWith("btn_kick"))
        {
            return;
        }

        await e.Interaction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);

        string idString = e.Id
            .Replace("btn_ban_", "")
            .Replace("btn_kick_", "");
        string message = await PunishMemberAsync(e, idString);

        await e.Interaction.EditOriginalResponseAsync(
            new DiscordWebhookBuilder().WithContent(message));
    }

    private static async Task<string> PunishMemberAsync(ComponentInteractionCreateEventArgs e, string idString)
    {
        string message = string.Empty;
        DiscordMember member = await e.Guild.GetMemberAsync(Convert.ToUInt64(idString));

        if (e.Id.Contains("kick"))
        {
            await member.RemoveAsync("Kicked by activity button.");
            message = $"Kicked user <@{idString}>!";
        }
        else
        {
            await e.Guild.BanMemberAsync(member, 0, "Kicked by activity button.");
            message = $"Banned user <@{idString}>!";
        }

        return message;
    }

    public async Task Run(DiscordClient client, CancellationToken cancellationToken)
    {
        FreeGuardDbContext dbContext = new();
        client.ComponentInteractionCreated += HandleInteraction;

        while (true)
        {
            client.Logger.LogInformation("Running activity update.");
            var dbGuilds = await dbContext.Guilds.ToListAsync();

            foreach (var dbGuild in dbGuilds)
            {
                try
                {
                    var dsGuild = await client.GetGuildAsync(dbGuild.GuildId);

                    var channel = dsGuild.GetChannel(Program.ActivityButtonChannelId);
                    try
                    {
                        await ClearChannelAsync(channel);
                    }
                    catch (Exception ex) 
                    {
                        client.Logger.LogError(ex, "Failed to dlear messages.");
                    }

                    var members = await dsGuild.GetAllMembersAsync();

                    foreach (var member in members)
                    {
                        if (member.IsBot)
                        {
                            continue;
                        }

                        if (_excludedPeople.Any(x => member.Id == x))
                        {
                            continue;
                        }

                        await CreateInteractionAsync(dbContext, dsGuild, channel, member);
                    }
                }
                catch (Exception ex)
                {
                    client.Logger.LogError(ex, "Failed to update.");
                }
            }


            client.Logger.LogInformation("Finished activity update.");

            await Task.Delay(_updateInterval, cancellationToken);

            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
        }
    }

    private async Task CreateInteractionAsync(FreeGuardDbContext dbContext, DiscordGuild dsGuild, DiscordChannel channel, DiscordMember member)
    {
        var activities = await _activityService.GetChatActivityForMemberAsync(dbContext, member.Id, dsGuild.Id);

        ActivityCount? count = activities
            .GroupBy(x => x.WriterId)
            .Select(x => new ActivityCount(x.Key, x.Count()))
            .FirstOrDefault();

        var lastMessage = member.JoinedAt.UtcDateTime;

        if (activities.Count() > 0)
        {
            lastMessage = activities.Max(x => x.WriteDate);
        }

        if (count != null)
        {
            await BuildButtonMessageAsync(channel, member, count, lastMessage);
        }
    }

    private static async Task BuildButtonMessageAsync(DiscordChannel channel, DiscordMember member, ActivityCount? count, DateTime lastMessage)
    {
        var kickBtnId = $"btn_kick_{member.Id}";
        var kickButton = new DiscordButtonComponent(ButtonStyle.Primary, kickBtnId, "Kick");

        var banBtnId = $"btn_ban_{member.Id}";
        var banButton = new DiscordButtonComponent(ButtonStyle.Danger, banBtnId, "Ban");

        var message = await new DiscordMessageBuilder()
            .WithContent($"{member.Mention} `{member.Username}` joined `{member.JoinedAt.UtcDateTime.ToString("yyyy-MM-dd hh:mm:ss")}` was last active `{lastMessage.ToString("yyyy-MM-dd hh:mm:ss")}` and has a total of `{count.Value.countOfMessages}` message.")
            .AddComponents(kickButton, banButton)
            .SendAsync(channel);
    }

    private static async Task ClearChannelAsync(DiscordChannel channel)
    {
        var messages = await channel.GetMessagesAsync(500);
        var filteredMessages = messages.Where(x => x.CreationTimestamp > DateTimeOffset.Now - TimeSpan.FromDays(10));
        await channel.DeleteMessagesAsync(filteredMessages);
    }
}

