﻿using FreeGuardBotLib;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders;

public readonly record struct GroupedLeaderboardAction(ulong affectedUser, long sumOfPoints);

public class GroupLeaderboardResultBuilder
{
    private readonly List<LeaderboardAction> _actions;
    private int _guildId;
    private TimeSpan? _maxAge;
    private OrderDirection? _orderDirection;
    private OrderField? _orderField;
    private int? _take;
    private int? _maxMonthsAgo;

    public GroupLeaderboardResultBuilder(int guildId)
    {
        _actions = new List<LeaderboardAction>();
        _guildId = guildId;
    }

    public GroupLeaderboardResultBuilder AddActions(IEnumerable<LeaderboardAction> actions)
    {
        _actions.AddRange(actions);

        return this;
    }

    public GroupLeaderboardResultBuilder SetMaxAge(TimeSpan? maxAge)
    {
        _maxAge = maxAge;
        _maxMonthsAgo = null;

        return this;
    }

    public GroupLeaderboardResultBuilder SetMaxMonthsAgo(int? maxMonthsAgo)
    {
        _maxMonthsAgo = maxMonthsAgo;
        _maxAge = null;

        return this;
    }

    public GroupLeaderboardResultBuilder SetOrderBy(OrderDirection? direction, OrderField? orderField)
    {
        _orderDirection = direction;
        _orderField = orderField;

        return this;
    }

    public GroupLeaderboardResultBuilder SetLimitOfMessages(int? limit)
    {
        _take = limit;

        return this;
    }

    public IEnumerable<GroupedLeaderboardAction> Build()
    {
        var actionQuery = _actions.Where(x => x.GuildId == _guildId);

        if (_maxAge != null)
        {
            var maxAgeDate = DateTime.Now.Subtract(_maxAge.Value);

            actionQuery = actionQuery.Where(x => x.CreateDate > maxAgeDate);
        }

        if (_maxMonthsAgo != null)
        {
            var maxAge = DateTime.Today.AddMonths(_maxMonthsAgo.Value * -1);
            maxAge = new DateTime(maxAge.Year, maxAge.Month, 1);

            actionQuery = actionQuery.Where(x => x.CreateDate > maxAge);
        }

        var groupedActionQuery = actionQuery
            .GroupBy(
                x => x.AffectedUserId,
                x => x.Points,
                (key, points) => new GroupedLeaderboardAction(key, points.Sum())
        );

        if (_orderDirection != null && _orderField != null)
        {
            groupedActionQuery = _orderDirection switch
            {
                OrderDirection.Ascending => _orderField switch
                {
                    OrderField.UserId => groupedActionQuery.OrderBy(x => x.affectedUser),
                    OrderField.SumOfPoints => groupedActionQuery.OrderBy(x => x.sumOfPoints),
                    _ => throw new NotImplementedException("No implementation for this order field yet done.")
                },
                OrderDirection.Descending => _orderField switch
                {
                    OrderField.UserId => groupedActionQuery.OrderByDescending(x => x.affectedUser),
                    OrderField.SumOfPoints => groupedActionQuery.OrderByDescending(x => x.sumOfPoints),
                    _ => throw new NotImplementedException("No implementation for this order field yet done.")
                },
                _ => throw new NotImplementedException("No implementation for this order direction yet done.")
            };
        }

        if (_take != null)
        {
            groupedActionQuery = groupedActionQuery.Take(_take.Value);
        }

        return groupedActionQuery;
    }
}


