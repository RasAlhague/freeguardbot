﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public interface IQuestionBuilder<T> : IDbObjectBuilder<Question, T>, IEmbedBuilder where T : DbContext
    {
        int QuestionTypeId { get; }
        string Text { get; }
        ulong CreateUserId { get; }

        IEnumerable<IAnswerBuilder<T>> AnswerBuilders { get; }
        IQuestionBuilder<T> WithText(string text);
        IQuestionBuilder<T> WithQuestionType(int id);
        IQuestionBuilder<T> AddAnswer(Action<IAnswerBuilder<T>> answerBuilder);
        IQuestionBuilder<T> ClearAnswers();
    }

}
