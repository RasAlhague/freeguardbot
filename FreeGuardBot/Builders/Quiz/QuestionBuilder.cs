﻿using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public class QuestionBuilder : IQuestionBuilder<FreeGuardDbContext>
    {
        private readonly List<IAnswerBuilder<FreeGuardDbContext>> _answerBuilders;

        public int QuestionTypeId { get; private set; }
        public string Text { get; private set; }
        public ulong CreateUserId { get; private set; }
        public IEnumerable<IAnswerBuilder<FreeGuardDbContext>> AnswerBuilders => _answerBuilders;

        public QuestionBuilder(ulong createUserId, string questionText)
        {
            CreateUserId = createUserId;
            Text = questionText;

            QuestionTypeId = 1;
            _answerBuilders = new List<IAnswerBuilder<FreeGuardDbContext>>();
        }

        public IQuestionBuilder<FreeGuardDbContext> WithText(string text)
        {
            Text = text;
            return this;
        }

        public IQuestionBuilder<FreeGuardDbContext> WithQuestionType(int id)
        {
            QuestionTypeId = id;
            return this;
        }

        public IQuestionBuilder<FreeGuardDbContext> AddAnswer(Action<IAnswerBuilder<FreeGuardDbContext>> answerBuilder)
        {
            var newAnswerBuilder = new AnswerBuilder();
            answerBuilder.Invoke(newAnswerBuilder);

            _answerBuilders.Add(newAnswerBuilder);
            return this;
        }

        public IQuestionBuilder<FreeGuardDbContext> ClearAnswers()
        {
            _answerBuilders.Clear();
            return this;
        }

        public async Task<Question> Create(FreeGuardDbContext dbContext, int guildId)
        {
            var question = new Question()
            {
                CreateUserId = CreateUserId,
                CreateDate = DateTime.UtcNow,
                GuildId = guildId,
                Text = Text
            };

            await dbContext.Questions.AddAsync(question);
            await dbContext.SaveChangesAsync();

            foreach (var answerBuilder in _answerBuilders)
            {
                answerBuilder.WithQuestionId(question.Id);
                answerBuilder.WithCreateUserId(CreateUserId);

                await answerBuilder.Create(dbContext, guildId);
            }

            return question;
        }

        public DiscordEmbed BuildEmbed(DiscordMember member)
        {
            var deb = new DiscordEmbedBuilder()
                .WithTitle($"Question created:")
                .WithDescription(Text)
                .AddField("Answers: ", DiscordConstants.EmptyField);

            foreach (var answerBuilder in _answerBuilders)
            {
                deb.AddField($"*Answer: {answerBuilder.Name}*", answerBuilder.Description)
                    .AddField($"Points:", $"{answerBuilder.Points}", true)
                    .AddField($"Is correct: ", $"{answerBuilder.Correct}", true);
            }

            return deb.Build();
        }
    }

}
