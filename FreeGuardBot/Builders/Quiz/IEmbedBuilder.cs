﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public interface IEmbedBuilder
    {
        DiscordEmbed BuildEmbed(DiscordMember member);
    }
}
