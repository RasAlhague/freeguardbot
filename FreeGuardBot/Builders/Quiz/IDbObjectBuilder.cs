﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public interface IDbObjectBuilder<T, E> where E : DbContext
    {
        Task<T> Create(E dbContext, int guildId);
    }
}
