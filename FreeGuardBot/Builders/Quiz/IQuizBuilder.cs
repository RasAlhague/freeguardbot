﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public interface IQuizBuilder<T> : IDbObjectBuilder<FreeGuardBotLib.Models.Quiz, T>, IEmbedBuilder where T : DbContext
    {
        IEnumerable<IQuestionBuilder<T>> QuestionBuilders { get; }
        IEnumerable<ulong> Participants { get; }
        IQuizBuilder<T> WithName(string name);
        IQuizBuilder<T> WithDescription(string description);
        IQuizBuilder<T> WithRetries(int retries);
        IQuizBuilder<T> WithDuration(int minutes);
        IQuizBuilder<T> WithDuration(TimeSpan minutes);
        IQuizBuilder<T> WithStartdate(DateTime startDate);
        IQuizBuilder<T> WithEnddate(DateTime endDate);
        IQuizBuilder<T> AddQuestion(Action<IQuestionBuilder<T>> questionBuilder);
        IQuizBuilder<T> AddParticipant(ulong participantId);
        IQuizBuilder<T> ClearQuestions();
        IQuizBuilder<T> ClearParticipants();
    }

}
