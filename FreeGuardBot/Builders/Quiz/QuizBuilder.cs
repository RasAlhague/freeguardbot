﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public class QuizBuilder : IQuizBuilder<FreeGuardDbContext>
    {
        private readonly List<IQuestionBuilder<FreeGuardDbContext>> _questionBuilders;
        private readonly List<ulong> _participants;

        public string Name { get; private set; }
        public string Description { get; private set; }
        public ulong CreateUserId { get; private set; }
        public int Retries { get; private set; }
        public TimeSpan Duration { get; private set; }
        public DateTime? ActiveFrom { get; private set; }
        public DateTime? ActiveUntil { get; private set; }
        public IEnumerable<IQuestionBuilder<FreeGuardDbContext>> QuestionBuilders => _questionBuilders;
        public IEnumerable<ulong> Participants => _participants;

        public QuizBuilder(ulong createUserId, string name)
        {
            CreateUserId = createUserId;
            Name = name;
            Description = string.Empty;

            Retries = 0;
            Duration = TimeSpan.Zero;
            ActiveFrom = null;
            ActiveUntil = null;

            _questionBuilders = new List<IQuestionBuilder<FreeGuardDbContext>>();
            _participants = new List<ulong>();
        }

        public IQuizBuilder<FreeGuardDbContext> WithName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name), $"The name of a quiz can not be null or empty!");
            }

            Name = name;
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> WithDescription(string description)
        {
            Description = description;
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> WithRetries(int retries)
        {
            Retries = retries;
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> WithDuration(int minutes)
        {
            Duration = TimeSpan.FromMinutes(minutes);
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> WithDuration(TimeSpan minutes)
        {
            Duration = minutes;
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> WithStartdate(DateTime startDate)
        {
            if (ActiveUntil.HasValue && startDate > ActiveUntil)
            {
                throw new ArgumentException("Start date cannot be bigger than end date!", nameof(startDate));
            }

            ActiveFrom = startDate;
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> WithEnddate(DateTime endDate)
        {
            if (ActiveFrom.HasValue && endDate < ActiveFrom)
            {
                throw new ArgumentException("End date cannot be smaller than start date!", nameof(endDate));
            }

            ActiveUntil = endDate;
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> AddQuestion(Action<IQuestionBuilder<FreeGuardDbContext>> questionBuilder)
        {
            var newQuestionBuilder = new QuestionBuilder(CreateUserId, "Empty");
            questionBuilder.Invoke(newQuestionBuilder);

            _questionBuilders.Add(newQuestionBuilder);
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> AddParticipant(ulong participantId)
        {
            if (_participants.Contains(participantId))
            {
                throw new ArgumentException("The participant already exists!", nameof(participantId));
            }

            _participants.Add(participantId);
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> ClearQuestions()
        {
            _questionBuilders.Clear();
            return this;
        }

        public IQuizBuilder<FreeGuardDbContext> ClearParticipants()
        {
            _participants.Clear();
            return this;
        }

        public async Task<FreeGuardBotLib.Models.Quiz> Create(FreeGuardDbContext dbContext, int guildId)
        {
            var quiz = new FreeGuardBotLib.Models.Quiz
            {
                CreateDate = DateTime.UtcNow,
                CreateUserId = CreateUserId,
                ActiveUntil = ActiveUntil,
                ActivFrom = ActiveFrom,
                Description = Description,
                Duration = Duration.Minutes,
                GuildId = guildId,
                Name = Name,
                Retries = Retries
            };

            await dbContext.Quizzes.AddAsync(quiz);
            await dbContext.SaveChangesAsync();

            foreach (var questionBuilder in _questionBuilders)
            {
                var question = await questionBuilder.Create(dbContext, guildId);

                var quizQuestion = new QuizQuestion
                {
                    QuestionId = question.Id,
                    QuizId = quiz.Id,
                    CreateUserId = CreateUserId
                };

                await dbContext.QuizQuestions.AddAsync(quizQuestion);
                await dbContext.SaveChangesAsync();
            }

            return await dbContext.Quizzes
                .Where(x => x.Id == quiz.Id)
                .Include(x => x.QuizParticipations)
                .Include(x => x.QuizQuestions)
                .ThenInclude(y => y.Question)
                .ThenInclude(q => q.AvailableAnswers)
                .FirstOrDefaultAsync();
        }

        public DiscordEmbed BuildEmbed(DiscordMember member)
        {
            var embedBuilder = new DiscordEmbedBuilder()
                .WithAuthor(member.DisplayName, iconUrl: member.AvatarUrl)
                .WithTitle($"Quiz: {Name}")
                .WithDescription(Description)
                .WithTimestamp(DateTime.UtcNow);

            if (Duration.TotalMinutes != 0)
            {
                embedBuilder.AddField("Duration:", Duration.TotalMinutes.ToString(), true);
            }
            embedBuilder.AddField("Retries:", Retries.ToString(), true)
                .AddField(DiscordConstants.EmptyField, DiscordConstants.EmptyField);

            if (ActiveFrom.HasValue && ActiveFrom != DateTime.MinValue)
            {
                embedBuilder.AddField("Startdate: ", ActiveFrom.Value.ToString("dd.MM.yyyy"), true);
            }
            else
            {
                embedBuilder.AddField("Startdate: ", "`None`", true);
            }

            if (ActiveUntil.HasValue && ActiveUntil.Value.Date != DateTime.MaxValue.Date)
            {
                embedBuilder.AddField("Enddate: ", ActiveUntil.Value.ToString("dd.MM.yyyy"), true);
            }
            else
            {
                embedBuilder.AddField("Enddate: ", "`None`", true);
            }

            embedBuilder.AddField("Questions: ", QuestionBuilders.Count().ToString());

            return embedBuilder.Build();
        }
    }

}
