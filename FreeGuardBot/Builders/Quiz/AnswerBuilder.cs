﻿using DSharpPlus.Entities;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public class AnswerBuilder : IAnswerBuilder<FreeGuardDbContext>
    {
        public bool Correct { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public int Points { get; private set; }
        public int QuestionId { get; private set; }
        public ulong CreateUserId { get; private set; }

        public AnswerBuilder()
        {
        }

        public async Task<AvailableAnswer> Create(FreeGuardDbContext dbContext, int guildId)
        {
            var answer = new AvailableAnswer()
            {
                Name = Name,
                Correct = Correct,
                Description = Description,
                Points = Points,
                QuestionId = QuestionId,
                CreateUserId = CreateUserId,
                CreateDate = DateTime.UtcNow,
                GuildId = guildId
            };

            await dbContext.AvailableAnswers.AddAsync(answer);
            await dbContext.SaveChangesAsync();

            return answer;
        }

        public IAnswerBuilder<FreeGuardDbContext> IsCorrect()
        {
            Correct = true;
            return this;
        }

        public IAnswerBuilder<FreeGuardDbContext> IsIncorrect()
        {
            Correct = false;
            return this;
        }

        public IAnswerBuilder<FreeGuardDbContext> WithDescription(string description)
        {
            Description = description;
            return this;
        }

        public IAnswerBuilder<FreeGuardDbContext> WithName(string name)
        {
            Name = name;
            return this;
        }

        public IAnswerBuilder<FreeGuardDbContext> WithPoints(int points)
        {
            Points = points;
            return this;
        }

        public IAnswerBuilder<FreeGuardDbContext> WithQuestionId(int id)
        {
            QuestionId = id;
            return this;
        }

        public DiscordEmbed BuildEmbed(DiscordMember member)
        {
            var deb = new DiscordEmbedBuilder()
                .WithTitle($"Answer: {Name}")
                .WithDescription(Description)
                .AddField("Points:", Points.ToString(), true)
                .AddField("Is correct:", Correct.ToString(), true);

            return deb.Build();
        }

        public IAnswerBuilder<FreeGuardDbContext> WithCreateUserId(ulong createUserId)
        {
            CreateUserId = createUserId;
            return this;
        }
    }

}
