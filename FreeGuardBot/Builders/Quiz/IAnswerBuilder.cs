﻿using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Builders.Quiz
{
    public interface IAnswerBuilder<T> : IDbObjectBuilder<AvailableAnswer, T>, IEmbedBuilder where T : DbContext
    {
        bool Correct { get; }
        string Name { get; }
        string Description { get; }
        int Points { get; }
        int QuestionId { get; }
        ulong CreateUserId { get; }

        IAnswerBuilder<T> WithName(string name);
        IAnswerBuilder<T> WithCreateUserId(ulong createUserId);
        IAnswerBuilder<T> WithDescription(string description);
        IAnswerBuilder<T> WithPoints(int points);
        IAnswerBuilder<T> WithQuestionId(int id);
        IAnswerBuilder<T> IsCorrect();
        IAnswerBuilder<T> IsIncorrect();
    }

}
