﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot
{
    /// <summary>
    /// Contains various helper functions for commands.
    /// </summary>
    public static class CommandHelpers
    {
        /// <summary>
        /// Adds a failure reaction to messages.
        /// </summary>
        /// <param name="message">The message on which should be reacted.</param>
        /// <param name="client">The current discord client.</param>
        /// <returns>No value.</returns>
        public static async Task AddFailureReaction(this DiscordMessage message, DiscordClient client)
        {
            var emoji = DiscordEmoji.FromName(client, ":x:");
            await message.CreateReactionAsync(emoji);
        }

        /// <summary>
        /// Adds a success reaction to messages.
        /// </summary>
        /// <param name="message">The message on which should be reacted.</param>
        /// <param name="client">The current discord client.</param>
        /// <returns>No value.</returns>
        public static async Task AddSuccessReaction(this DiscordMessage message, DiscordClient client)
        {
            var emoji = DiscordEmoji.FromName(client, ":white_check_mark:");
            await message.CreateReactionAsync(emoji);
        }

        /// <summary>
        /// Gets an embed for reaction messages.
        /// </summary>
        /// <param name="cmdContext">The context of the command on which this should be executed.</param>
        /// <param name="entry"></param>
        /// <returns>The embed for the blacklist entry.</returns>
        public static DiscordEmbed GetBlacklistEntryEmbed(CommandContext cmdContext, Blacklistentry entry)
        {
            string userName;
            if (cmdContext.Guild.Members.TryGetValue(entry.CreateUserId, out DiscordMember value))
            {
                userName = value.DisplayName;
            }
            else
            {
                userName = entry.CreateUserId.ToString();
            }

            string playfab = !string.IsNullOrEmpty(entry.PlayfabId) ? entry.PlayfabId : "not set";

            var embedBuilder = new DiscordEmbedBuilder()
               .WithColor(new DiscordColor(0, 49, 83))
               .WithTitle($"Blacklistentry {entry.Id}:")
               .WithAuthor(cmdContext.Client.CurrentUser.Username)
               .AddField("Steamname", entry.Username)
               .AddField("SteamId", $"http://steamcommunity.com/profiles/{entry.SteamId}")
               .AddField("PlayfabId", playfab, true)
               .AddField("Reported by", userName, true)
               .AddField("Report day", entry.CreateDate.ToString("dd.MM.yyyy"), true)
               .AddField("Reason", entry.Reason);

            return embedBuilder.Build();
        }

        public static DiscordEmbed GetRecruitEmbed(DiscordGuild dsGuild, DiscordMember member, Recruit recruit, string botName)
        {
            string userName;
            if (dsGuild.Members.TryGetValue(recruit.RecruiterId.Value, out DiscordMember value))
            {
                userName = value.DisplayName;
            }
            else
            {
                userName = recruit.RecruitId.ToString();
            }

            var embedBuilder = new DiscordEmbedBuilder()
                   .WithColor(new DiscordColor(0, 49, 83))
                   .WithTitle($"Recruit No.{recruit.Id}")
                   .WithAuthor(botName)
                   .AddField("DiscordName", member.Mention)
                   .AddField("Name", recruit.Name)
                   .AddField("SteamId", $"http://steamcommunity.com/profiles/{recruit.SteamId}");

            if (!string.IsNullOrEmpty(recruit.Notice))
            {
                embedBuilder.AddField("Notice", recruit.Notice);
            }

            embedBuilder.AddField("Recruiter", userName, true)
                .AddField("Recruit date", recruit.CreateDate.ToString("dd.MM.yyyy"), true);

            return embedBuilder.Build();
        }

        public static async Task UpdateRecruitAsync(DiscordMember m, FreeGuardDbContext dbContext, Guild guild, Recruit recruit)
        {
            var recruited = await Discorduser.GetOrCreateDiscorduserAsync(dbContext, m.Id);
            recruited.SteamId = recruit.SteamId;
            dbContext.Discordusers.Update(recruited);

            await Guilduser.GetOrCreateGuildUserAsync(dbContext, guild.Id, recruited.DiscordUserId);

            recruit.Joined = "true";
            recruit.RecruitId = recruited.DiscordUserId;
            dbContext.Recruits.Update(recruit);
            await dbContext.SaveChangesAsync();
        }

        public async static Task SendJoinMessage(DiscordMember m, DiscordGuild g, Recruit recruit, ulong recruitmentChannelId, string botName)
        {
            var embed = CommandHelpers.GetRecruitEmbed(g, m, recruit, botName);

            var channel = g.GetChannel(recruitmentChannelId);

            await new DiscordMessageBuilder()
                .WithEmbed(embed)
                .WithAllowedMention(new UserMention(m.Id))
                .SendAsync(channel);

            await m.ModifyAsync(x => x.Nickname = recruit.Name);
        }
    }
}
