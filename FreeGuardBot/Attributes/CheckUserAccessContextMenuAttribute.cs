﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.AccessManagement;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Attributes
{
    /// <summary>
    /// Checks whether a user has access to an command.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class CheckUserAccessContextMenuAttribute : ContextMenuCheckBaseAttribute
    {
        private readonly IAccessManager _accessManager;

        /// <summary>
        /// The rights needed for a command to access it.
        /// </summary>
        public IEnumerable<Rights> NeededRights { get; }

        /// <summary>
        /// Standardconstructor
        /// </summary>
        public CheckUserAccessContextMenuAttribute(params Rights[] rights)
        {
            _accessManager = new CommandAccessManager();

            NeededRights = rights;
        }

        public override async Task<bool> ExecuteChecksAsync(ContextMenuContext ctx)
        {
            if (ctx.Guild != null)
            {
                bool access = await _accessManager.CheckUserAccessAsync(ctx, NeededRights);

                return access;
            }
            return false;
        }
    }
}
