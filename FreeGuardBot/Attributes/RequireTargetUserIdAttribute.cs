﻿using DSharpPlus.SlashCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.Attributes
{
    internal class RequireTargetUserIdAttribute : ContextMenuCheckBaseAttribute
    {
        public ulong UserId;

        public RequireTargetUserIdAttribute(ulong userId)
        {
            this.UserId = userId;
        }

        public override async Task<bool> ExecuteChecksAsync(ContextMenuContext ctx)
        {
            if (ctx.TargetUser.Id == UserId)
                return true;
            else
                return false;
        }
    }
}
