﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBot.Converters;
using FreeGuardBotLib;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MusicBotLib.Commands;
using MusicBotLib.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DSharpPlus.SlashCommands;
using FreeGuardBot.Services;
using FreeGuardBot.SlashCommands;
using System.Collections.Concurrent;
using System.Text;
using DSharpPlus.Exceptions;
using FreeGuardBot.Builders;
using System.Threading;
using FreeGuardBot.Services.Lls;
using FreeGuardBot.Services.Infractions;
using FreeGuardBot.Services.TextGeneration;

namespace FreeGuardBot
{
    /// <summary>
    /// The entry class of the project.
    /// </summary>
    public static class Program
    {
        private static string _commandIdentifier = "";
        private static readonly ConcurrentDictionary<ulong, RoleSelect> _roleSelectCache = new ConcurrentDictionary<ulong, RoleSelect>();
        private static DiscordClient _discordClient;
        private static CancellationTokenSource _cancelSource;
        private static ILeaderboardActionService _leaderboardActionService;
        //public const ulong ActivityButtonChannelId = 866963479633068063;
        public const ulong ActivityButtonChannelId = 889604734015651901;

        /// <summary>
        /// The entry point into the program.
        /// </summary>
        /// <param name="commandIdentifier">The identifier used at the beginning of commands.</param>
        public static void Main(string commandIdentifier)
        {
            LLS();
            MainAsync(commandIdentifier).GetAwaiter().GetResult();
        }

        /// <summary>
        /// This simple message will strengthen and remind us of our ideals and maxims which we need to enhance
        /// the game experience day by day in matter of both small and great:Dealing with annoying abusers of
        /// the law and still following the rules or planning events and involving the community.
        /// Both needs strength, focus and patience which can be really nerve-wracking.
        /// </summary>
        static void LLS()
        {
            Console.WriteLine("▄                                  ▀▀█      ▀                                   ");
            Console.WriteLine("█       ▄▄▄   ▄ ▄▄    ▄▄▄▄           █    ▄▄▄    ▄   ▄   ▄▄▄                    ");
            Console.WriteLine("█      █▀ ▀█  █▀  █  █▀ ▀█           █      █    ▀▄ ▄▀  █▀  █                   ");
            Console.WriteLine("█      █   █  █   █  █   █           █      █     █▄█   █▀▀▀▀                   ");
            Console.WriteLine("█▄▄▄▄▄ ▀█▄█▀  █   █  ▀█▄▀█           ▀▄▄  ▄▄█▄▄    █    ▀█▄▄▀                   ");
            Console.WriteLine("                      ▄  █                                                      ");
            Console.WriteLine("                       ▀▀                                                       ");
            Console.WriteLine("                                                                                ");
            Console.WriteLine(" ▄▄▄▄    ▄           ▀▀█                    ▀        █    ▀                    ▄");
            Console.WriteLine("█▀   ▀ ▄▄█▄▄   ▄▄▄     █   ▄     ▄  ▄▄▄   ▄▄▄     ▄▄▄█  ▄▄▄     ▄▄▄   ▄▄▄▄▄    █");
            Console.WriteLine("▀█▄▄▄    █    ▀   █    █   ▀▄ ▄ ▄▀ █▀  █    █    █▀ ▀█    █    █   ▀  █ █ █    █");
            Console.WriteLine("    ▀█   █    ▄▀▀▀█    █    █▄█▄█  █▀▀▀▀    █    █   █    █     ▀▀▀▄  █ █ █    ▀");
            Console.WriteLine("▀▄▄▄█▀   ▀▄▄  ▀▄▄▀█    ▀▄▄   █ █   ▀█▄▄▀  ▄▄█▄▄  ▀█▄██  ▄▄█▄▄  ▀▄▄▄▀  █ █ █    █");
            Console.WriteLine("                                                                                ");
        }

        static async Task MainAsync(string commandIdentifier)
        {
            BotConfig config = BotConfig.GetInstance();
            await UpdateCacheAsync();

            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = config.BotToken,
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.All,
                MinimumLogLevel = config.LogLevel
            });

            _discordClient = discord;

            CommandsNextExtension commands = ConfigureCommandsClient(commandIdentifier, discord);
            SlashCommandsExtension slash = ConfigureSlashCommands(discord);
            _commandIdentifier = commandIdentifier;
            LavalinkConfiguration lavalinkConfig;
            LavalinkExtension lavalink;
            ConfigureLavalink(discord, config, out lavalinkConfig, out lavalink);

            RegisterEventHandlers(discord, commands, commandIdentifier);
            RegisterBotCommands(commands);
            RegisterSlashCommand(slash);
            RegisterConverters(commands);
            DiscordActivity activity = new DiscordActivity("Long live Stalweidism!", ActivityType.Playing);

            await discord.ConnectAsync(activity);
            if (!string.IsNullOrEmpty(config.LavalinkAddress))
            {
                await lavalink.ConnectAsync(lavalinkConfig);
            }

            await Task.Delay(-1);
        }

        private static SlashCommandsExtension ConfigureSlashCommands(DiscordClient discord)
        {
            var services = new ServiceCollection()
                    .AddSingleton<IActivityService, ActivityService>()
                    .AddSingleton<IVacationService, VacationService>()
                    .AddSingleton<ITicketService, TicketService>()
                    .AddSingleton<IRoleSelectionService, RoleSelectionService>()
                    .AddSingleton<IManagementService, ManagementService>()
                    .AddSingleton<ILeaderboardActionService, LeaderboardActionService>()
                    .AddSingleton<ICommandLogService, CommandLogService>()
                    .AddSingleton<ILeaderboardService, LeaderboardService>()
                    .AddSingleton<ILeaderboardUpdateService, LeaderboardUpdateService>()
                    .AddSingleton<ILlsService, LlsService>()
                    .AddSingleton<IInfractionService, InfractionService>()
                    .AddSingleton<ITextGenService>(s => new TextGenService(new Uri("http://127.0.0.1:8080/"), TimeSpan.FromMinutes(10)))
                    .BuildServiceProvider();

            _leaderboardActionService = services.GetService<ILeaderboardActionService>();
            var leaderBoardUpdateService = services.GetService<ILeaderboardUpdateService>();

            _cancelSource = new CancellationTokenSource();
            Task.Run(async () => await leaderBoardUpdateService.RunUpdaterAsync(discord, TimeSpan.FromMinutes(10), _cancelSource.Token));
            Task.Run(async () =>  {
                var manager = new ActivityButtonManager(TimeSpan.FromMinutes(15), new ActivityService(), ExcludeId.BirdId, ExcludeId.DennisId, ExcludeId.FrankId, ExcludeId.JoshId, ExcludeId.JosifId);
                await manager.Run(discord, _cancelSource.Token);
            });

            return discord.UseSlashCommands(new SlashCommandsConfiguration
            {
                Services = services
            });
        }

        private static void RegisterSlashCommand(SlashCommandsExtension slash)
        {
            slash.RegisterCommands<VacationAppCommand>();
            slash.RegisterCommands<LlsAppCommand>();
            slash.RegisterCommands<ActivityAppCommand>();
            slash.RegisterCommands<TicketCommand>();
            slash.RegisterCommands<RoleSelectionModule>();
            slash.RegisterCommands<MiscAppModule>();
            slash.RegisterCommands<ChannelHelperAppCommand>();
            slash.RegisterCommands<TimeoutAppCommand>();
            slash.RegisterCommands<ManagementAppCommand>();
            slash.RegisterCommands<LeaderboardAppCommand>();
            slash.RegisterCommands<InfractionAppCommand>();
            slash.RegisterCommands<NukeAppCommand>();
            slash.RegisterCommands<TalkAppCommand>();
        }

        private static void ConfigureLavalink(DiscordClient discord, BotConfig config, out LavalinkConfiguration lavalinkConfig, out LavalinkExtension lavalink)
        {
            var endpoint = new ConnectionEndpoint
            {
                Hostname = config.LavalinkAddress, // From your server configuration.
                Port = config.LavalinkPort // From your server configuration
            };

            lavalinkConfig = new LavalinkConfiguration
            {
                Password = config.LavalinkPasswort, // From your server configuration.
                RestEndpoint = endpoint,
                SocketEndpoint = endpoint
            };
            lavalink = discord.UseLavalink();
        }

        private static CommandsNextExtension ConfigureCommandsClient(string commandIdentifier, DiscordClient discord)
        {
            discord.UseInteractivity(new InteractivityConfiguration()
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(60)
            });

            var services = new ServiceCollection()
                .AddSingleton<IMusicService, MusicService>()
                .AddSingleton<ISocialCreditService, SocialCreditService>()
                .AddSingleton<ITtsService, GttsService>()
                .AddSingleton<IQuizService, QuizService>()
                .AddSingleton<IQuizResultService, QuizResultService>()
                .AddSingleton<IParticipationService, ParticipationService>()
                .AddSingleton<ICommandLogService, CommandLogService>()
                .AddSingleton<IPollService, PollService>()
                .BuildServiceProvider();

            var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefixes = new[] { commandIdentifier },
                Services = services
            });

            return commands;
        }

        private static void RegisterConverters(CommandsNextExtension commands)
        {
            commands.RegisterConverter(new RightEnumConverter());
            commands.RegisterConverter(new SearchTypeConverter());
        }

        private static void RegisterEventHandlers(DiscordClient discord, CommandsNextExtension commands, string commandIdentifier)
        {
            discord.MessageCreated += async (s, e) =>
            {
                if (e.MentionedUsers.Any(x => x.IsCurrent))
                {
                    await PrintPingMessageAsync(e.Message.Id, e.Message.Channel, commandIdentifier);
                }
                if (!e.Channel.IsPrivate)
                {
                    await ReactToMessages(e, s.Logger);
                }
            };
            discord.MessageCreated += LogMessageActivity;
            discord.MessageDeleted += LogAngelMessageDeletion;
            discord.GuildMemberRemoved += GuildMemberLeftHandler;
            discord.GuildMemberAdded += GuildMemberJoinedHandler;
            discord.MessageReactionAdded += HandleRoleSelectionReactionAdded;
            discord.MessageReactionRemoved += HandleRoleSelectionReactionRemoved;
            commands.CommandErrored += CmdErroredHandler;
        }

        private async static Task LogAngelMessageDeletion(DiscordClient sender, MessageDeleteEventArgs e)
        {
            if (e.Channel.Id == ActivityButtonChannelId) 
            {
                return;
            }

            try
            {
                if (e.Message.Author.Id == 714599597829390459)
                {

                    var user = e.Message.Author;

                    var channel = e.Guild.GetChannel(956534502572843008);
                    var embedBuilder = new DiscordEmbedBuilder()
                        .WithAuthor($"{user.Username}#{user.Discriminator}", iconUrl: user.AvatarUrl)
                        .WithTitle($"Message deleted in #{e.Channel.Name}")
                        .WithDescription(e.Message.Content)
                        .WithFooter($"ID: {user.Id}")
                        .WithColor(DiscordColor.Red)
                        .WithTimestamp(DateTime.UtcNow);

                    if (e.Message.Attachments != null && e.Message.Attachments.Count != 0)
                    {
                        var firstAttachment = e.Message.Attachments[0];

                        embedBuilder.WithImageUrl(firstAttachment.ProxyUrl);
                    }

                    if (e.Message.ReferencedMessage != null)
                    {
                        embedBuilder.AddField("Referenced message:", $"{e.Message.ReferencedMessage.Content}");
                    }

                    await channel.SendMessageAsync(embedBuilder);
                }
            }
            catch (Exception ex)
            {
                sender.Logger.LogError(ex, "Error while getting channel.");
            }

        }

        private static async Task HandleRoleSelectionReactionAdded(DiscordClient sender, MessageReactionAddEventArgs e)
        {
            if (e.User.IsBot)
            {
                return;
            }

            if (_roleSelectCache.TryGetValue(e.Guild.Id, out RoleSelect roleSelect))
            {
                if (e.Channel.Id != roleSelect.ChannelId || e.Message.Id != roleSelect.MessageId)
                {
                    return;
                }

                var reaction = roleSelect.RoleSelectReactions.FirstOrDefault(x => x.Emoji.EmojiId == e.Emoji.Id && x.Emoji.Name == e.Emoji.GetDiscordName());

                if (reaction != null)
                {
                    try
                    {
                        var role = e.Guild.GetRole(reaction.Role.DiscordRoleId);
                        var member = await e.Guild.GetMemberAsync(e.User.Id);
                        await member.GrantRoleAsync(role);
                    }
                    catch (Exception ex)
                    {
                        sender.Logger.LogError(ex, "Error while giving role to user!");
                    }
                }
                else
                {
                    await e.Message.DeleteReactionsEmojiAsync(e.Emoji);
                }
            }
        }

        private static async Task HandleRoleSelectionReactionRemoved(DiscordClient sender, MessageReactionRemoveEventArgs e)
        {
            if (e.User.IsBot)
            {
                return;
            }

            if (_roleSelectCache.TryGetValue(e.Guild.Id, out RoleSelect roleSelect))
            {
                if (e.Channel.Id != roleSelect.ChannelId || e.Message.Id != roleSelect.MessageId)
                {
                    return;
                }

                var reaction = roleSelect.RoleSelectReactions.FirstOrDefault(x => x.Emoji.EmojiId == e.Emoji.Id && x.Emoji.Name == e.Emoji.GetDiscordName());

                if (reaction != null)
                {
                    try
                    {
                        var role = e.Guild.GetRole(reaction.Role.DiscordRoleId);
                        var member = await e.Guild.GetMemberAsync(e.User.Id);
                        await member.RevokeRoleAsync(role);
                    }
                    catch (Exception ex)
                    {
                        sender.Logger.LogError(ex, "Error while revoking role at user!");
                    }
                }
            }
        }

        private static async Task LogMessageActivity(DiscordClient sender, MessageCreateEventArgs e)
        {
            try
            {
                if (e.Guild is null)
                {
                    return;
                }
                if (e.Author.IsBot)
                {
                    return;
                }

                using var dbContext = new FreeGuardDbContext();

                var guild = await Guild.GetOrCreateGuildAsync(dbContext, e.Guild.Id, e.Guild.Name);
                var discordUser = await Discorduser.GetOrCreateDiscorduserAsync(dbContext, e.Author.Id);
                await Guilduser.GetOrCreateGuildUserAsync(dbContext, guild.Id, e.Author.Id);

                MessageActivityLogEntry messageActivityLogEntry = new()
                {
                    WriteDate = DateTime.UtcNow,
                    WriterId = discordUser.DiscordUserId,
                    GuildId = guild.Id
                };

                dbContext.MessageActivityLogEntries.Add(messageActivityLogEntry);
                await dbContext.SaveChangesAsync();

                await _leaderboardActionService.GrantPointsAsync(dbContext, e.Guild.Id, discordUser.DiscordUserId, 1, "Discord chat message.", sender.CurrentUser.Id);
            }
            catch (Exception ex)
            {
                sender.Logger.LogError(ex, "Error while writing message log");
            }
        }

        private static void RegisterBotCommands(CommandsNextExtension commands)
        {
            commands.RegisterCommands<BlacklistCommandModule>();
            commands.RegisterCommands<RecruitmentCommandModule>();
            commands.RegisterCommands<ManagementCommandModule>();
            commands.RegisterCommands<SupportCommandModule>();
            commands.RegisterCommands<MiscCommandModule>();
            commands.RegisterCommands<ETACommandModule>();
            commands.RegisterCommands<MusicCommandModule>();
            commands.RegisterCommands<SocialCreditCommandModule>();
            commands.RegisterCommands<TtsCommandModule>();
            commands.RegisterCommands<QuizCommandModule>();
            commands.RegisterCommands<PollCommandModule>();
        }

        private async static Task GuildMemberLeftHandler(DiscordClient sender, GuildMemberRemoveEventArgs e)
        {
            using FreeGuardDbContext dbContext = new();
            var guild = await Guild.GetOrCreateGuildAsync(dbContext, e.Guild.Id, e.Guild.Name);

            var recruit = dbContext.Recruits
                .OrderBy(x => x.CreateDate)
                .Last(x => x.GuildId == guild.Id
                    && x.RecruitId == e.Member.Id
                    && x.Joined == "true"
                );
            recruit.Left = "true";
            recruit.ModifyDate = DateTime.UtcNow;
            recruit.ModifyUserId = sender.CurrentUser.Id;

            dbContext.Recruits.Update(recruit);
            await dbContext.SaveChangesAsync();
        }

        private async static Task PrintPingMessageAsync(ulong messageId, DiscordChannel channel, string commandIdentifier)
        {
            using FileStream fs = File.Open("./images/LLS-dialog.gif", FileMode.Open);

            await new DiscordMessageBuilder()
                .WithFile("lls.gif", fs)
                .WithContent($"Long Live Stalweidism! \nHow may i help you? \nYou can use `{commandIdentifier}help` to get more informations!")
                .WithReply(messageId)
                .SendAsync(channel);
        }

        private async static Task GuildMemberJoinedHandler(DiscordClient client, GuildMemberAddEventArgs e)
        {
            using FreeGuardDbContext dbContext = new();

            var guild = await Guild.GetOrCreateGuildAsync(dbContext, e.Guild.Id, e.Guild.Name);
            var recruit = dbContext.Recruits
                .SingleOrDefault(x =>
                    x.GuildId == guild.Id &&
                    x.DiscordName == $"{e.Member.Username}#{e.Member.Discriminator}" &&
                    x.Joined == "false"
            );

            if (recruit != null)
            {
                try
                {
                    await GrantRecruitRolesAsync(dbContext, e, guild.Id);
                    await CommandHelpers.UpdateRecruitAsync(e.Member, dbContext, guild, recruit);

                    if (guild.RecruitmentChannelId.HasValue)
                    {
                        await CommandHelpers.SendJoinMessage(e.Member, e.Guild, recruit, guild.RecruitmentChannelId.Value, client.CurrentUser.Username);
                    }
                    if (guild.WelcomeMessageChannel.HasValue && !string.IsNullOrEmpty(guild.WelcomeMessage))
                    {
                        await SendGreetingAsync(guild, e, recruit);
                    }


                    await _leaderboardActionService.GrantPointsAsync(dbContext, e.Guild.Id, recruit.RecruiterId.Value, 500, "Recruited someone", client.CurrentUser.Id);

                }
                catch (UnauthorizedAccessException ex)
                {
                    client.Logger.LogError(ex, "The bot either misses 'manage roles' permission or the right is higher in the hierachy.");
                }
            }
        }

        private async static Task SendGreetingAsync(Guild g, GuildMemberAddEventArgs e, Recruit recruit)
        {
            if (e.Guild.Members.TryGetValue(recruit.RecruitId.Value, out DiscordMember dsRecruit) && e.Guild.Members.TryGetValue(recruit.RecruiterId.Value, out DiscordMember dsRecruiter))
            {
                var welcomeMessage = g.WelcomeMessage;
                welcomeMessage = welcomeMessage
                    .Replace("$RECRUIT$", dsRecruit.Mention)
                    .Replace("$RECRUITER$", dsRecruiter.Mention);

                var channel = e.Guild.GetChannel(g.WelcomeMessageChannel.Value);

                var mentions = new List<IMention>
                {
                    new UserMention(dsRecruit.Id),
                    new UserMention(dsRecruiter.Id)
                };

                foreach (Match match in Regex.Matches(welcomeMessage, "#[a-zA-Z0-9_]+"))
                {
                    var mentionedChannel = e.Guild.Channels.Values.FirstOrDefault(x => x.Name == match.Value[1..]);

                    if (mentionedChannel != null)
                    {
                        welcomeMessage = welcomeMessage.Replace(match.Value, mentionedChannel.Mention);
                    }
                }

                await new DiscordMessageBuilder()
                    .WithContent(welcomeMessage)
                    .WithAllowedMentions(mentions)
                    .SendAsync(channel);
            }
        }

        private static async Task GrantRecruitRolesAsync(FreeGuardDbContext dbContext, GuildMemberAddEventArgs e, int guildId)
        {
            var joinRoles = await dbContext.Roles.Where(x => x.GuildId == guildId && x.IsJoinRole == "true").ToListAsync();

            foreach (var role in joinRoles)
            {
                if (e.Guild.Roles.TryGetValue(role.DiscordRoleId, out DiscordRole dsRole))
                {
                    await e.Member.GrantRoleAsync(dsRole, "Joining as new recruit.");
                }
            }
        }

        private static async Task CmdErroredHandler(CommandsNextExtension ctx, CommandErrorEventArgs e)
        {
            using var dbCtx = new FreeGuardDbContext();

            if (e.Context.Guild is null)
            {
                e.Context.Client.Logger.LogError("Command from non command context fired!");
                return;
            }

            var guild = await Guild.GetOrCreateGuildAsync(dbCtx, e.Context.Guild.Id, e.Context.Guild.Name);

            string commandName = "";
            if (e.Context.Command != null)
            {
                commandName = e.Context.Command.QualifiedName;
            }

            await ErrorLog.Create(dbCtx, guild.Id, commandName, e.Exception);

            if (e.Exception is ChecksFailedException failedException)
            {
                var failedChecks = failedException.FailedChecks;

                foreach (var failedCheck in failedChecks)
                {
                    if (failedCheck is CheckUserAccessAttribute)
                    {
                        var errorEmbed = CreateErrorEmbed("Insufficient rights!", "You have insufficient rights for the excution of this command!", $"{e.Context.Member.Username}#{e.Context.Member.Discriminator}", e.Context.Member.DefaultAvatarUrl);
                        await e.Context.RespondAsync(errorEmbed);
                    }
                }

                await e.Context.Message.AddFailureReaction(ctx.Client);
            }
            else if (e.Exception is CommandNotFoundException)
            {
                ctx.Client.Logger.LogError(e.Exception, $"Possible problem with a module!");
            }
            else if (e.Command != null && e.Exception is InvalidOperationException || e.Exception is ArgumentException && e.Exception.Source == "DSharpPlus.CommandsNext")
            {
                var errorEmbed = CreateErrorEmbed("Invalid command or usage!", $"The command does not exist. \nPlease use `{_commandIdentifier}help {e.Command.QualifiedName}` to view the other commands.", $"{e.Context.Member.Username}#{e.Context.Member.Discriminator}", e.Context.Member.AvatarUrl);
                await e.Context.RespondAsync(errorEmbed);
                ctx.Client.Logger.LogError(e.Exception, $"Possible problem with a module!");

                await e.Context.Message.AddFailureReaction(ctx.Client);
            }
            else if (e.Command != null && e.Exception is FormatException)
            {
                var errorEmbed = CreateErrorEmbed("Invalid paramethers!", $"The input parameters were wrong! \nPlease use `{_commandIdentifier}help {e.Command.QualifiedName}` to view the help for this command.", $"{e.Context.Member.Username}#{e.Context.Member.Discriminator}", e.Context.Member.AvatarUrl);
                await e.Context.RespondAsync(errorEmbed);
                ctx.Client.Logger.LogError(e.Exception, $"Possible problem with a module!");

                await e.Context.Message.AddFailureReaction(ctx.Client);
            }
            else
            {
                var errorEmbed = CreateErrorEmbed("Unknown error!", $"An error occured during execution! \nPlease report this with `{_commandIdentifier}support report-bug`", $"{e.Context.Member.Username}#{e.Context.Member.Discriminator}", e.Context.Member.AvatarUrl);
                await e.Context.RespondAsync(errorEmbed);
                ctx.Client.Logger.LogError(e.Exception, $"Possible problem with a module!");

                await e.Context.Message.AddFailureReaction(ctx.Client);
            }

        }

        private static DiscordEmbed CreateErrorEmbed(string errorTitle, string message, string author, string profileUrl)
        {
            return new DiscordEmbedBuilder()
                .WithColor(new DiscordColor(139, 0, 0))
                .WithAuthor(author, profileUrl, profileUrl)
                .WithDescription($":x: {errorTitle}")
                .AddField(DiscordConstants.EmptyField, message)
                .Build();
        }

        private async static Task ReactToMessages(MessageCreateEventArgs e, ILogger<BaseDiscordClient> logger)
        {
            using var dbContext = new FreeGuardDbContext();
            var guild = await Guild.GetOrCreateGuildAsync(dbContext, e.Guild.Id, e.Guild.Name);

            var messageReactions = await dbContext.MessageReactions
                .Where(x => x.GuildId == guild.Id)
                .Include("ReactionEmojis")
                .ToListAsync();

            foreach (var messageReaction in messageReactions.Where(x => e.Message.Content.ToLower().Contains(x.Keyword.ToLower())))
            {
                foreach (var reactionEmoji in messageReaction.ReactionEmojis)
                {
                    var emoji = await dbContext.Emojis
                        .FirstOrDefaultAsync(x => x.Id == reactionEmoji.EmojiId);

                    try
                    {
                        var discordEmoji = await e.Guild.GetEmojiAsync(emoji.EmojiId);

                        await e.Message.CreateReactionAsync(discordEmoji);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "Error while trying to react to message!");
                    }
                }
            }
        }

        public async static Task UpdateCacheAsync()
        {
            using FreeGuardDbContext ctx = new FreeGuardDbContext();

            var roleSelections = await ctx.RoleSelects
                .Include(x => x.RoleSelectReactions)
                .ThenInclude(x => x.Emoji)
                .Include(x => x.RoleSelectReactions)
                .ThenInclude(x => x.Role)
                .ToListAsync();

            _roleSelectCache.Clear();

            foreach (var roleSelect in roleSelections)
            {
                var guild = await ctx.Guilds.FirstOrDefaultAsync(x => x.Id == roleSelect.GuildId);
                _roleSelectCache.TryAdd(guild.GuildId, roleSelect);
            }
        }
    }
}
