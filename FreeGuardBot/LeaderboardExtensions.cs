﻿using FreeGuardBot.Builders;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot
{
    /// <summary>
    /// 
    /// </summary>
    public static class LeaderboardExtensions
    {
        public static GroupLeaderboardResultBuilder CreateBuilder(this Leaderboard leaderboard)
        {
            var builder = new GroupLeaderboardResultBuilder(leaderboard.GuildId);

            if (leaderboard.OrderField.HasValue && leaderboard.OrderDirection.HasValue)
            {
                builder.SetOrderBy(leaderboard.OrderDirection, leaderboard.OrderField);
            }
            else
            {
                builder.SetOrderBy(FreeGuardBotLib.OrderDirection.Descending, FreeGuardBotLib.OrderField.SumOfPoints);
            }

            if (leaderboard.MaxAge.HasValue)
            {
                builder.SetMaxAge(TimeSpan.FromDays(leaderboard.MaxAge.Value));
            }

            if (leaderboard.MaxMonthsInPast.HasValue)
            {
                builder.SetMaxMonthsAgo(leaderboard.MaxMonthsInPast);
            }

            if (leaderboard.Limit.HasValue)
            {
                builder.SetLimitOfMessages(leaderboard.Limit);
            }
            else
            {
                builder.SetLimitOfMessages(20);
            }

            return builder;

        }
    }
}
