﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot
{
    public class PaginatedSelectMenu
    {
        private readonly int _pageSize = 10;
        private readonly List<DiscordSelectComponentOption> _options;
        private readonly DiscordMessageBuilder _messageBuilder;
        private int _currentPage;

        public int PageSize { get { return _pageSize; } }
        public IEnumerable<DiscordSelectComponentOption> Options { get { return _options; } }
        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                if (value <= 0 || value > GetPageCount())
                {
                    _currentPage = 1;
                    return;
                }

                _currentPage = value;
            }
        }

        public PaginatedSelectMenu(int pageSize, IEnumerable<DiscordSelectComponentOption> options)
        {
            _currentPage = 1;
            _pageSize = pageSize;

            _options = new();
            _options.AddRange(options);

            _messageBuilder = new DiscordMessageBuilder();
        }

        public int GetPageCount()
        {
            float division = ((float)_options.Count / (float)PageSize);

            return (int)Math.Ceiling(division);
        }

        public async Task<InteractivityResult<ComponentInteractionCreateEventArgs>> DisplayCurrentPageAsync(CommandContext ctx, string displayMessage, TimeSpan? timeout = null)
        {
            var page = GetOptionsForPage();

            string dropdownName = Guid.NewGuid().ToString();
            var rolesDropdown = new DiscordSelectComponent(dropdownName, null, page, false, 1, page.Count());

            _messageBuilder.ClearComponents();
            _messageBuilder
                .WithContent($"{displayMessage} (Page {CurrentPage} from {GetPageCount()})")
                .AddComponents(rolesDropdown);

            var message = await _messageBuilder.SendAsync(ctx.Channel);

            var roleResult = await message.WaitForSelectAsync(ctx.Member, dropdownName, timeout);
            await roleResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);

            return roleResult;
        }

        public void Next()
        {
            CurrentPage = CurrentPage + 1;
        }

        private IEnumerable<DiscordSelectComponentOption> GetOptionsForPage()
        {
            var page = _options.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();
            page.Add(new DiscordSelectComponentOption("Next page", "page_changer"));

            return page;
        }
    }
}
