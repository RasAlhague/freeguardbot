﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FreeGuardBot
{
    public class DiscordChannelArchiver
    {
        private readonly DiscordChannel _channel;

        public DiscordChannelArchiver(DiscordChannel discordChannel)
        {
            _channel = discordChannel;
        }

        public async Task<XDocument> CreateArchiveXml(int messageDepth)
        {
            var rootElement = new XElement("messages");
            rootElement.Add(new XAttribute("guild", _channel.Guild.Name));
            var messages = await _channel.GetMessagesAsync(messageDepth);

            foreach (var message in messages)
            {
                var messageElement = new XElement("message");
                messageElement.Add(new XAttribute("author", message.Author.Username));
                messageElement.Add(new XAttribute("createDate", message.CreationTimestamp));
                messageElement.Add(new XElement("content", message.Content));

                rootElement.Add(messageElement);
            }

            XDocument xmlDoc = new(rootElement);

            return xmlDoc;
        }
    }
}
