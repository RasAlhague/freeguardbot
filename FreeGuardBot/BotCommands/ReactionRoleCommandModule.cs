﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

[Group("test")]
[Description("Commands for ETA.")]
public class ReactionRoleCommandModule : BaseCommandModule
{
    [Command("test")]
    public async Task SetReaction(CommandContext ctx, ulong messageId)
    {
        var message = await new DiscordMessageBuilder()
            .WithContent("This is a test!")
            .SendAsync(ctx.Channel);

        var pollEmojis = await ctx.Client.GetInteractivity().DoPollAsync(message, new List<DiscordEmoji> { DiscordEmoji.FromName(ctx.Client, ":thinking:"), DiscordEmoji.FromName(ctx.Client, ":grinning:") },DSharpPlus.Interactivity.Enums.PollBehaviour.DeleteEmojis, TimeSpan.FromSeconds(30));

        foreach (var pollEmoji in pollEmojis)
        {
            foreach (var vote in pollEmoji.Voted)
            {
                await ctx.Channel.SendMessageAsync($"User {vote.Username} has voted {pollEmoji.Emoji}");
            }
        }
    }
}
