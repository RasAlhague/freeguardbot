﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBot.Services;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

[Group("poll")]
[Description("Commands for polls.")]
public class PollCommandModule : LogableCommandModule
{
    private readonly IPollService _pollService;

    public PollCommandModule(IPollService pollService, ICommandLogService commandLogService) : base(commandLogService)
    {
        _pollService = pollService;
    }

    [RequireGuild]
    [Command("create")]
    [Description("Creates a new poll. you still have to use the 'poll run' command to run the poll so a poll can be used multiple times.")]
    public async Task CreatePoll(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        DiscordDialogue dialogue = new DiscordDialogue();
        await dialogue.InitializeAsync(ctx);

        await dialogue.DiscordDmChannel.SendMessageAsync("Long live Stalweidism!");

        string title = await dialogue.SendDmDialogueAsync<string>("Please give me the title of the poll.", timeout: 300);

        if (string.IsNullOrEmpty(title))
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("No title was provided so the poll creation has been aborted!");
            return;
        }

        string description = await dialogue.SendDmDialogueAsync<string>("Please provide me a description for the poll.", timeout: 300);

        if (string.IsNullOrEmpty(description))
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("No description was provided so the poll creation has been aborted!");
            return;
        }

        int maxResults = await dialogue.SendDmDialogueAsync<int>("How many possible options do you want to add to the poll? (Min 2, Max 10)", x => x >= 2 && x <= 10, timeout: 300);

        if (maxResults <= 2 && maxResults >= 10)
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("No valid number of options was provided so the poll creation has been aborted!");
            return;
        }

        var options = new List<(string, DiscordEmoji)>();

        for (int i = 0; i < maxResults; i++)
        {
            var answerText = await dialogue.SendDmDialogueAsync<string>($"Please send an a description for option No.{i+1}!");
            var message = await dialogue.DiscordDmChannel.SendMessageAsync("Please react to this message with the emoji you want for the described option.\n\nYou can only use emojis which the bot has access to!");

            var result = await message.WaitForReactionAsync(ctx.Member, TimeSpan.FromMinutes(5));

            if (result.TimedOut)
            {
                await dialogue.DiscordDmChannel.SendMessageAsync("The emoji reaction timed out! Aborting poll creation!");
                return;
            }

            options.Add((answerText, result.Result.Emoji));
        }

        Poll poll = new Poll
        {
            CreateDate = DateTime.UtcNow,
            CreateUserId = ctx.Member.Id,
            Description = description,
            Name = title,
            GuildId = guild.Id
        };

        foreach (var option in options)
        {
            Emoji emoji = await Emoji.GetOrCreate(dbContext, ctx.Member.Id, option.Item2.Id, option.Item2.GetDiscordName());
            PollEmoji pollEmoji = new PollEmoji
            {
                EmojiId = emoji.Id,
                Emoji = emoji,
                Description = option.Item1
            };
            poll.PollEmojis.Add(pollEmoji);
        }

        poll = await _pollService.CreatePollAsync(dbContext, poll);
        DiscordEmbedBuilder embedBuilder = BuildPollEmbed(ctx.Client, poll, ctx.Member);

        await dialogue.DiscordDmChannel.SendMessageAsync(embedBuilder);
        await dialogue.DiscordDmChannel.SendMessageAsync($"You can use this poll via using `{ctx.Prefix}poll run {poll.Id}`.");
    }

    [RequireGuild]
    [Command("run")]
    [Description("Runs a poll so users can react to it.")]
    public async Task ExecutePoll(CommandContext ctx, int pollId, int durationInMinutes)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        var polls = await _pollService.GetPollsAsync(dbContext, guild.Id, ctx.Member.Id);

        var poll = polls.FirstOrDefault(x => x.Id == pollId);

        if (poll == null)
        {
            await ctx.RespondAsync("No poll found with this id!");
        }

        var embed = BuildPollEmbed(ctx.Client, poll, ctx.Member);
        var message = await ctx.Channel.SendMessageAsync(embed);

        var emojis = new List<DiscordEmoji>();

        foreach (var emoji in poll.PollEmojis)
        {
            emojis.Add(GetEmojiFromPollEmoji(ctx.Client, emoji));
        }

        var interactivePollResult = await ctx.Client.GetInteractivity().DoPollAsync(message, emojis, PollBehaviour.KeepEmojis, TimeSpan.FromMinutes(durationInMinutes));

        var votes = new List<Vote>();

        foreach (var pollEmojiResult in interactivePollResult)
        {
            var pollEmoji = poll.PollEmojis.FirstOrDefault(x => x.Emoji.EmojiId == pollEmojiResult.Emoji.Id);

            foreach (var userVote in pollEmojiResult.Voted)
            {
                await Discorduser.GetOrCreateDiscorduserAsync(dbContext, userVote.Id);
                await Guilduser.GetOrCreateGuildUserAsync(dbContext, guild.Id, userVote.Id);

                Vote vote = new Vote();
                vote.PollEmoji = pollEmoji;
                vote.PollEmojiId = pollEmoji.Id;
                vote.VoteeId = userVote.Id;

                votes.Add(vote);
            }
        }

        var pollResult = await _pollService.CreatePollResultAsync(dbContext, poll.Id, ctx.Member.Id, guild.Id, votes);
        await message.RespondAsync($"{ctx.Member.Mention}, the poll has ended, you can show the poll results via the `{ctx.Prefix}poll result {pollResult.Id}` or `{ctx.Prefix}poll result-Details {pollResult.Id}` command.");
        await ctx.Channel.SendMessageAsync($"In total `{votes.Count}` people have participated.");
    }

    [RequireGuild]
    [Command("show")]
    [Description("Shows all the polls a user can run.")]
    public async Task ShowPolls(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        var polls = await _pollService.GetPollsAsync(dbContext, guild.Id, ctx.Member.Id);

        if (!polls.Any())
        {
            await ctx.RespondAsync("You currently have no polls.");
            return;
        }

        var embedBuilder = new DiscordEmbedBuilder()
            .WithTitle($"Polls for {ctx.Member.DisplayName}:");

        foreach (var poll in polls)
        {
            embedBuilder.AddField($"Poll no.{poll.Id}: {poll.Name}", poll.Description);
        }

        await ctx.RespondAsync(embedBuilder);
    }

    [RequireGuild]
    [Command("results")]
    [Description("Shows all the ids from which you can get poll results.")]
    public async Task ShowAllResults(CommandContext ctx, int pollId)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        var polls = await _pollService.GetPollsAsync(dbContext, guild.Id, ctx.Member.Id);
        var poll = polls.FirstOrDefault(x => x.Id == pollId);

        if (poll == null)
        {
            await ctx.RespondAsync("No poll found with this id!");
            return;
        }

        var embed = new DiscordEmbedBuilder()
            .WithTitle($"Pole results for: {poll.Name}")
            .WithDescription(poll.Description)
            .WithAuthor(ctx.Member.DisplayName, iconUrl: ctx.Member.AvatarUrl);

        foreach (var pollResult in poll.PollResults)
        {
            embed.AddField($"Created: {pollResult.CreateDate}", $"`{ctx.Prefix}poll result {pollResult.Id}`");
        }

        await ctx.RespondAsync(embed);
    }

    [RequireGuild]
    [Command("result")]
    [Description("Shows the results of one poll.")]
    public async Task ShowResults(CommandContext ctx, int resultId)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        var pollResult = await _pollService.GetResultAsync(dbContext, guild.Id, resultId, ctx.Member.Id);

        var embed = new DiscordEmbedBuilder()
            .WithAuthor(ctx.Member.DisplayName, iconUrl: ctx.Member.AvatarUrl)
            .WithTitle($"Pollresult for poll Id {pollResult.Poll.Id}: {pollResult.Poll.Name}")
            .WithDescription(pollResult.Poll.Description)
            .AddField("Create Date: ", pollResult.CreateDate.ToString());

        var groupedVotes = pollResult.Votes.GroupBy(x => x.PollEmoji);

        StringBuilder sb = new StringBuilder();

        foreach (var group in groupedVotes)
        {
            DiscordEmoji emoji = GetEmojiFromPollEmoji(ctx.Client, group.Key);
            sb.AppendLine($"{emoji}: {group.Key.Description} `{group.Count()}`");
        }

        embed.AddField("Options results:", sb.ToString());

        await ctx.RespondAsync(embed);
    }

    [RequireGuild]
    [Command("result-details")]
    [Description("Shows the details of the results of one poll.")]
    public async Task ShowResultDetails(CommandContext ctx, int pollId)
    {
        await ctx.RespondAsync("Not implemented yet!");
    }

    private static DiscordEmbedBuilder BuildPollEmbed(DiscordClient client, Poll poll, DiscordMember discordMember)
    {
        var embedBuilder = new DiscordEmbedBuilder()
                        .WithTitle($"Poll Id {poll.Id}: {poll.Name}")
                        .WithDescription(poll.Description)
                        .WithAuthor(discordMember.DisplayName, iconUrl: discordMember.AvatarUrl);

        StringBuilder sb = new StringBuilder();

        foreach (var pollEmoji in poll.PollEmojis)
        {
            DiscordEmoji emoji = GetEmojiFromPollEmoji(client, pollEmoji);
            sb.AppendLine($"{emoji}: {pollEmoji.Description}");
        }

        embedBuilder.AddField("Options:", sb.ToString());
        return embedBuilder;
    }

    private static DiscordEmoji GetEmojiFromPollEmoji(DiscordClient client, PollEmoji pollEmoji)
    {
        DiscordEmoji emoji;

        if (DiscordEmoji.TryFromName(client, pollEmoji.Emoji.Name, out emoji))
        {
            return emoji;
        }
        if (DiscordEmoji.TryFromUnicode(pollEmoji.Emoji.Name, out emoji))
        {
            return emoji;
        }
        if (DiscordEmoji.TryFromGuildEmote(client, pollEmoji.Emoji.EmojiId, out emoji))
        {
            return emoji;
        }

        return emoji;
    }
}
