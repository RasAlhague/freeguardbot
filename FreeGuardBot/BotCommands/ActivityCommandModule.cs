﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

/// <summary>
/// Contains the Commands for the activity of members.
/// </summary>
[Group("activity")]
[Description("Commands for the activity.")]
public class ActivityCommandModule : LogableCommandModule
{
    private readonly IActivityService _activityService;

    public ActivityCommandModule(IActivityService activityService, ICommandLogService commandLogService) : base(commandLogService)
    {
        _activityService = activityService;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ctx"></param>
    /// <returns></returns>
    [RequireGuild]
    [CheckUserAccess(Rights.UseActivity)]
    [Command("ds-list")]
    [Description("Displays a list of most inactive members in discord.")]
    public async Task DiscordActivityCommand(CommandContext ctx)
    {
        Stopwatch sw = new Stopwatch();

        sw.Start();
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var members = await ctx.Guild.GetAllMembersAsync();
        var channels = await ctx.Guild.GetChannelsAsync();

        List<DiscordMessage> totalMessages = new();
        var maxDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(30));

        foreach (var channel in channels.Where(x => !x.IsCategory && x.Type != DSharpPlus.ChannelType.Voice))
        {
            try
            {
                var messages = await channel.GetMessagesAsync(60000);
                totalMessages.AddRange(messages.Where(x => x.CreationTimestamp >= maxDate));
            }
            catch (UnauthorizedException _)
            {
                ctx.Client.Logger.LogWarning($"The access to the channel '{channel.Name}' is forbidden!");
            }

        }
        sw.Stop();

        await ctx.RespondAsync($"Messages: {totalMessages.Count}, time: {sw.ElapsedMilliseconds / 1000}");
    }
}
