﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Services;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

[Group("ETA")]
[Description("Commands for ETA.")]
public class ETACommandModule : LogableCommandModule
{
    public ETACommandModule(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    /// <summary>
    /// Sends a image which contains Long live Stalweidism into the channel it was executed on.
    /// </summary>
    /// <param name="ctx">The command context.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [Command("new")]
    [Description("Creates a new ETA.")]
    public async Task EtaNewCommand(CommandContext ctx, DateTime date, [RemainingText] string titel)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var newETA = new ETA()
        {
            GuildId = guild.Id,
            Titel = titel,
            Description = "None",
            ETADate = date,
            CreateDate = DateTime.UtcNow,
            CreateUserId = ctx.Member.Id
        };

        dbContext.ETAs.Add(newETA);
        await dbContext.SaveChangesAsync();

        var embedBuilder = new DiscordEmbedBuilder()
            .WithColor(new DiscordColor(0, 49, 83))
            .WithAuthor(ctx.Client.CurrentUser.Username)
            .WithTitle($"ETA for: {newETA.Titel}")
            .WithDescription(newETA.Description)
            .AddField("Estimated date of arrival:", newETA.ETADate.ToLongDateString())
            .AddField("Creator", ctx.Member.DisplayName, inline: true)
            .AddField("CreateDate", newETA.CreateDate.ToShortDateString(), inline: true);

        var messageBuilder = new DiscordMessageBuilder()
            .WithReply(ctx.Message.Id)
            .WithEmbed(embedBuilder);

        await messageBuilder.SendAsync(ctx.Channel);
    }

    /// <summary>
    /// Sends a image which contains Long live Stalweidism into the channel it was executed on.
    /// </summary>
    /// <param name="ctx">The command context.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [Command("GIB")]
    [Description("Gives the etas.")]
    public async Task EtaCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var etas = await dbContext.ETAs
            .Where(x => x.GuildId == guild.Id && x.ETADate > DateTime.UtcNow)
            .OrderBy(x => x.ETADate)
            .Include("CreateUser")
            .ToListAsync();

        var pages = new List<Page>();

        foreach (var eta in etas)
        {
            var creator = await ctx.Guild.GetMemberAsync(eta.CreateUser.DiscordUserId);
            string creatorName = eta.CreateUserId.ToString();

            if (creator != null)
            {
                creatorName = creator.DisplayName;
            }

            var embedBuilder = new DiscordEmbedBuilder()
                .WithColor(new DiscordColor(0, 49, 83))
                .WithAuthor(ctx.Client.CurrentUser.Username)
                .WithTitle($"ETA for: {eta.Titel}")
                .WithDescription(eta.Description)
                .AddField("Estimated date of arrival:", eta.ETADate.ToLongDateString())
                .AddField("Creator", creator.DisplayName, inline: true)
                .AddField("CreateDate", eta.CreateDate.ToShortDateString(), inline: true);

            pages.Add(new Page(embed: embedBuilder));
        }

        await ctx.Channel.SendPaginatedMessageAsync(ctx.Member, pages);
    }
}
