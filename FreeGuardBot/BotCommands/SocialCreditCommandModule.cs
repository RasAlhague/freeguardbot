﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

[Group("socialcredit")]
[Description("Commands for social credit memes!")]
public class SocialCreditCommandModule : LogableCommandModule
{
    private readonly ISocialCreditService _socialCreditService;
    
    public SocialCreditCommandModule(ISocialCreditService socialCreditService, ICommandLogService commandLogService) : base(commandLogService)
    {
        _socialCreditService = socialCreditService;
    }

    [RequireGuild]
    [Command("show")]
    [Description("Displays your current social credits.")]
    public async Task ShowCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var socialCreditData = _socialCreditService.GetSocialCreditInfo(ctx.Member.Id);

        var embed = new DiscordEmbedBuilder()
            .WithTitle("Social Credit Board:")
            .WithAuthor(ctx.Member.DisplayName, iconUrl: ctx.Member.AvatarUrl)
            .WithColor(DiscordColor.Blue)
            .AddField("Name:", ctx.Member.Mention)
            .AddField("Points:", socialCreditData.ToString());

        await ctx.RespondAsync(embed);
    }

    [RequireGuild]
    [Command("show")]
    [Description("Displays your current social credits.")]
    public async Task ShowCommand(CommandContext ctx, DiscordMember target)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var socialCreditData = _socialCreditService.GetSocialCreditInfo(target.Id);

        var embed = new DiscordEmbedBuilder()
            .WithTitle("Social Credit Board:")
            .WithAuthor(target.DisplayName, iconUrl: target.AvatarUrl)
            .WithColor(DiscordColor.Blue)
            .AddField("Name:", target.Mention)
            .AddField("Points:", socialCreditData.ToString());

        await ctx.RespondAsync(embed);
    }

    [RequireGuild]
    [Command("bet")]
    [Description("Reevalutes your social credits.")]
    public async Task BetCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var socialCreditData = _socialCreditService.BetSocialCredits(ctx.Member.Id);

        var embed = new DiscordEmbedBuilder()
            .WithTitle("Social Credit notice:")
            .WithAuthor(ctx.Member.DisplayName, iconUrl: ctx.Member.AvatarUrl)
            .WithDescription($"You have get {socialCreditData.Item2} social credits!")
            .AddField("Name:", ctx.Member.Mention)
            .AddField("Changed amount:", socialCreditData.Item2.ToString())
            .AddField("Total points:", socialCreditData.Item1.ToString())
            .AddField("Reason:", socialCreditData.Item3);

        if(socialCreditData.Item2 >= 0)
        {
            embed.WithColor(DiscordColor.Green);
        }
        else
        {
            embed.WithColor(DiscordColor.Red);
        }

        await ctx.RespondAsync(embed);
    }

    [RequireGuild]
    [Command("deduct")]
    [Description("Deducts social credits.")]
    public async Task DeductCommand(CommandContext ctx, DiscordUser target, int value, [RemainingText] string reason)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var socialCreditData = _socialCreditService.Deduct(target.Id, value);

        var embed = new DiscordEmbedBuilder()
            .WithTitle("Social Credit notice:")
            .WithAuthor(target.Username, iconUrl: target.AvatarUrl)
            .WithDescription($"You have lost {value} social credits!")
            .WithColor(DiscordColor.Red)
            .AddField("Name:", target.Mention)
            .AddField("Changed amount:", value.ToString())
            .AddField("Total points:", socialCreditData.ToString())
            .AddField("Reason:", reason);

        await ctx.RespondAsync(embed);
    }
}
