using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBot.Services;
using Microsoft.Extensions.Logging;

namespace FreeGuardBot.BotCommands; 

public class TtsCommandModule : LogableCommandModule
{
    private readonly ITtsService _ttsService;

    public TtsCommandModule(ITtsService ttsService, ICommandLogService commandLogService) : base(commandLogService)
    {
        _ttsService = ttsService;
    }

    [RequireGuild]
    [Command("tts")]
    [Description("Converts a message into speech saved as wav file.")]   
    public async Task TtsCommand(CommandContext ctx, [RemainingText] string text)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);
        await _ttsService.SendTtsFileAsync(ctx, text, _ttsService.DefaultLanguage);
    }

    [RequireGuild]
    [Command("tts")]
    [Description("Converts a message into speech saved as wav file.")]   
    public async Task TtsCommand(CommandContext ctx, Language language, [RemainingText] string text)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);
        await _ttsService.SendTtsFileAsync(ctx, text, language);
    }

    [RequireGuild]
    [Command("tts-l")]
    [Description("Sets the default language for the tts feature.")]   
    public async Task TtsLanguageSelectionCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);
        var language = await GetTtsLanguage(ctx);

        if (string.IsNullOrEmpty(language)) 
        {
            await ctx.RespondAsync("No language selected!");
            return;
        }

        _ttsService.ChangeLanguage(language);
        await ctx.Channel.SendMessageAsync($"Language is now set to `{language}`!");
    }

    private static async Task<string> GetTtsLanguage(CommandContext ctx)
    {
        var options = new List<DiscordSelectComponentOption>()
        {
            new DiscordSelectComponentOption("German", "de-DE"),
            new DiscordSelectComponentOption("Briish", "en-GB"),
            new DiscordSelectComponentOption("English", "en-US"),
            new DiscordSelectComponentOption("Spanish", "es-ES"),
            new DiscordSelectComponentOption("French", "fr-FR"),
            new DiscordSelectComponentOption("Italian", "it-IT"),
            new DiscordSelectComponentOption("Kebab", "tr-TR"),
            new DiscordSelectComponentOption("Polish", "pl-PL"),
            new DiscordSelectComponentOption("Japanese", "ja-JP"),
        };

        string dropdownId = "languageDropdown";
        var dropdown = new DiscordSelectComponent(dropdownId, null, options, false);

        var builder = new DiscordMessageBuilder()
            .WithContent("Please choose the language you want to use as default!")
            .AddComponents(dropdown);
        var message = await builder.SendAsync(ctx.Channel);

        var changeFieldsResult = await message.WaitForSelectAsync(ctx.Member, dropdownId, TimeSpan.FromMinutes(5));
        if (changeFieldsResult.TimedOut)
        {
            await message.RespondAsync("Language selection timed out!");
            return null;
        }

        await changeFieldsResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
        return changeFieldsResult.Result.Values[0].Replace('_', '-');
    }
}
