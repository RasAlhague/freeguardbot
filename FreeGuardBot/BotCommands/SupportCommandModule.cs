﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBot.Services;
using FreeGuardBotLib;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using GitLabApiClient;
using GitLabApiClient.Models.Issues.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

/// <summary>
/// Contains the Commands for the blacklist.
/// </summary>
[Group("support")]
[Description("Commands for stuff like reporting bugs or making suggestions.")]
public class SupportCommandModule : LogableCommandModule
{
    public SupportCommandModule(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    /// <summary>
    /// Opens a dm channel in which you can report a bug, which will create an issue on gitlab.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [Command(SupportCommandNameConstants.ReportBugCommandName)]
    [Description("Opens a dm channel in which you can report a bug, which will create an issue on gitlab.")]
    public async Task ReportBugCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue discordDialogue = new();
        await discordDialogue.InitializeAsync(ctx);

        var consent = await discordDialogue.SendDmDialogueAsync<string>(
            "If you use this, your discord name will be added in the issue. Do you aggree? (yes/no)",
            x => x.ToLower() == "yes" || x.ToLower() == "no");

        if (string.IsNullOrEmpty(consent))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }
        if (consent == "no")
        {
            return;
        }

        var titel = await discordDialogue.SendDmDialogueAsync<string>("Please send a titel for your problem.");

        if (string.IsNullOrEmpty(titel))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        var description = await discordDialogue.SendDmDialogueAsync<string>("Please send the problem you had as detailed as possible.");

        if (string.IsNullOrEmpty(description))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        await SendGitlabIssue(ctx, titel, description, "bug");

        await ctx.RespondAsync("Successfully send bug report. Thank you for helping!");
    }

    /// <summary>
    /// Opens a dm channel in which you can give a suggestion which will create an issue on gitlab.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [Command(SupportCommandNameConstants.SuggestCommandName)]
    [Description("Opens a dm channel in which you can give a suggestion which will create an issue on gitlab.")]
    public async Task SuggestCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue discordDialogue = new();
        await discordDialogue.InitializeAsync(ctx);

        var consent = await discordDialogue.SendDmDialogueAsync<string>(
            "If you use this, your discord name will be added in the issue. Do you aggree? (yes/no)",
            x => x.ToLower() == "yes" || x.ToLower() == "no");

        if (string.IsNullOrEmpty(consent))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }
        if (consent == "no")
        {
            return;
        }

        var titel = await discordDialogue.SendDmDialogueAsync<string>("Please send a titel for your problem.");

        if (string.IsNullOrEmpty(titel))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        var description = await discordDialogue.SendDmDialogueAsync<string>("Please send the suggestions you have as detailed as possible.");

        if (string.IsNullOrEmpty(description))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        var issueUrl = await SendGitlabIssue(ctx, titel, description, "suggestion");

        await ctx.RespondAsync($"Successfully send suggestion. Thank you for helping!\n\nYou can look at progress here: {issueUrl}");
    }

    private static async Task<string> SendGitlabIssue(CommandContext ctx, string titel, string description, string label)
    {
        var config = BotConfig.GetInstance();

        var client = new GitLabClient("https://gitlab.com/", config.GitlabToken);

        var issue = new CreateIssueRequest($"{label}:{titel}");
        issue.Labels.Add(label);
        issue.Description = $"{description}{Environment.NewLine}{Environment.NewLine}Send by: '{ctx.Member.Username}#{ctx.Member.Discriminator}'.";
        issue.Confidential = false;

        var createdIssue = await client.Issues.CreateAsync(28080014, issue);

        return createdIssue.WebUrl;
    }
}
