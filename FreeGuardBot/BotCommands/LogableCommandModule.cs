﻿using DSharpPlus.CommandsNext;
using FreeGuardBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

public abstract class LogableCommandModule : BaseCommandModule
{
    protected readonly ICommandLogService _commandLogService;

    public LogableCommandModule(ICommandLogService commandLogService)
    {
        _commandLogService = commandLogService;
    }
}
