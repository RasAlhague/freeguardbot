﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.BotCommands.Services;
using FreeGuardBot.Builders.Quiz;
using FreeGuardBot.Services;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

[Group("quiz")]
[Description("Commands for discord quizzes.")]
public class QuizCommandModule : LogableCommandModule
{
    private readonly IQuizService _quizService;
    private readonly IParticipationService _participationService;
    private readonly IQuizResultService _quizResultService;

    public QuizCommandModule(IQuizService quizService, IParticipationService participationService, IQuizResultService quizResultService, ICommandLogService commandLogService) : base(commandLogService)
    {
        _quizService = quizService;
        _participationService = participationService;
        _quizResultService = quizResultService;
    }

    [Command("new")]
    [Description("Creates a new quiz.")]
    public async Task CreateQuizCommand(CommandContext ctx, [RemainingText] string quizTitle)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);

        var description = await dialogue.SendDmDialogueAsync<string>("Please write a description for your quiz, you have 10 min.", 600);

        if (string.IsNullOrEmpty(description))
        {
            await ctx.RespondAsync("Quiz creation aborted, description cannot be empty.");
            return;
        }

        var retries = await dialogue.SendDmDialogueAsync<int>("Please choose the number of retries, from `0` to `10`.", x => x >= 0 && x <= 10);
        var duration = await dialogue.SendDmDialogueAsync<int>("Please choose the selection of the test in minutes.\n`0` means you dont have a time limit.", x => x >= 0);
        var startDate = await dialogue.SendDmDialogueAsync<DateTime>($"Please send the start date. The format is `YYYY-MM-DD`.\nIf you dont want any date use {DateTime.MinValue.ToString("yyyy-MM-dd")}.");
        var endDate = await dialogue.SendDmDialogueAsync<DateTime>($"Please select a end date.\nMake sure end date is __bigger__ then start date!\nThe format is `YYYY-MM-DD`.\nIf you dont want any date use {DateTime.MaxValue.ToString("yyyy-MM-dd")}.", x => x > startDate);

        var quizBuilder = new QuizBuilder(ctx.Member.Id, quizTitle)
            .WithDescription(description)
            .WithRetries(retries)
            .WithDuration(duration);
        if (startDate != DateTime.MinValue)
        {
            quizBuilder.WithStartdate(startDate);
        }
        if (endDate != DateTime.MaxValue)
        {
            quizBuilder.WithEnddate(endDate);
        }

        await SetQuestions(ctx, dialogue, quizBuilder);

        await dialogue.DiscordDmChannel.SendMessageAsync(quizBuilder.BuildEmbed(ctx.Member));

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        await quizBuilder.Create(dbContext, guild.Id);
        await dialogue.DiscordDmChannel.SendMessageAsync("The quiz has been created!");
    }

    [Command("show")]
    [Description("Shows the information about a single quiz. Can only show quizzes you own or participate in. If you are only participant it restricts the visible information.")]
    public async Task ShowQuiz(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var quizzes = await dbContext.Quizzes
            .Where(x => x.GuildId == guild.Id)
            .Include(x => x.QuizParticipations)
            .Where(x => x.CreateUserId == ctx.Member.Id || x.QuizParticipations.Any(x => x.ParticipantId == ctx.Member.Id))
            .Include(x => x.QuizQuestions)
            .ThenInclude(y => y.Question)
            .ThenInclude(q => q.AvailableAnswers)
            .ToListAsync();

        

        await ctx.RespondAsync(quizzes.Count().ToString());
    }

    [Command("show")]
    [Description("Shows the information about a single quiz. Can only show quizzes you own or participate in. If you are only participant it restricts the visible information.")]
    public async Task ShowQuiz(CommandContext ctx, [Description("The id of the quiz which should be displayed.")] int quizId)
    {
    }

    [Command("list")]
    [Description("Shows an overview of all quizzes you either own or participate in.")]
    public async Task ShowQuizList(CommandContext ctx)
    {
    }

    [Command("from")]
    [Description("Creates a new quiz based on another one.")]
    public async Task CreateFromQuiz(CommandContext ctx)
    {
    }

    [Command("from")]
    [Description("Creates a new quiz based on another one.")]
    public async Task CreateFromQuiz(CommandContext ctx, int quizId)
    {
    }

    [Command("edit")]
    [Description("Edits a existing quiz.")]
    public async Task EditQuiz(CommandContext ctx)
    {
    }

    [Command("edit")]
    [Description("Edits a existing quiz.")]
    public async Task EditQuiz(CommandContext ctx, int quizId)
    {
    }

    [Command("enroll")]
    [Description("Enrolls specified users to a quiz. This action can be refused by the users.")]
    public async Task EnrollToQuiz(CommandContext ctx, params DiscordMember[] members)
    {
    }

    [Command("kick")]
    [Description("Kicks a user from partaking in a quiz.")]
    public async Task KickFromQuiz(CommandContext ctx, params DiscordMember[] members)
    {
    }

    [Command("force-enroll")]
    [Description("Forces users to partake in a quiz.")]
    public async Task ForceEnrollToQuiz(CommandContext ctx, int quizId, params DiscordMember[] members)
    {
    }

    [Command("result")]
    [Description("Gets the result of a quiz. Can be a unfinished result.")]
    public async Task GetQuizResult(CommandContext ctx)
    {
    }

    [Command("result")]
    [Description("Gets the result of a quiz. Can be a unfinished result.")]
    public async Task GetQuizResult(CommandContext ctx, int quizId)
    {
    }

    [Command("final")]
    [Description("Gets the result of a quiz. This will set the quiz in a finished state.")]
    public async Task GetFinalQuizResult(CommandContext ctx)
    {
    }

    [Command("participate")]
    [Description("Starts the quiz for the participating user.")]
    public async Task ParticipateInQuiz(CommandContext ctx)
    {
    }

    [Command("participate")]
    [Description("Starts the quiz for the participating user.")]
    public async Task ParticipateInQuiz(CommandContext ctx, int quizId)
    {
    }

    [Command("delete")]
    [Description("Deletes a quiz.")]
    public async Task DeleteQuiz(CommandContext ctx)
    {
    }

    private async Task SetQuestions(CommandContext ctx, DiscordDialogue dialogue, IQuizBuilder<FreeGuardDbContext> quizBuilder)
    {
        bool waitResult = true;

        var questionCreationEmbed = new DiscordEmbedBuilder()
            .WithTitle("Question creation:")
            .WithColor(DiscordColor.Aquamarine);

        do
        {
            await dialogue.DiscordDmChannel.SendMessageAsync(questionCreationEmbed);
            var question = await GetQuestionBuilderAsync(ctx, dialogue, ctx.Member.Id);

            quizBuilder.AddQuestion(questionBuilder =>
            {
                questionBuilder
                    .WithText(question.Text)
                    .WithQuestionType(question.QuestionTypeId);

                foreach (var answer in question.AnswerBuilders)
                {
                    questionBuilder.AddAnswer(answerBuilder =>
                    {
                        answerBuilder
                            .WithName(answer.Name)
                            .WithDescription(answer.Description)
                            .WithPoints(answer.Points);

                        if (answer.Correct)
                        {
                            answerBuilder.IsCorrect();
                        }
                        else
                        {
                            answerBuilder.IsIncorrect();
                        }
                    });
                }

            });

            await dialogue.DiscordDmChannel.SendMessageAsync(question.BuildEmbed(ctx.Member));

            waitResult = await AskForMoreDialogueAsync(ctx, dialogue, "Do you want to create more questions?");
        } while (waitResult);
    }

    private async Task<bool> AskForMoreDialogueAsync(CommandContext ctx, DiscordDialogue dialogue, string text)
    {
        var messageBuilder = new DiscordMessageBuilder()
            .WithContent(text)
            .AddComponents(new DiscordComponent[]
            {
                new DiscordButtonComponent(DSharpPlus.ButtonStyle.Success,  "true", "yes"),
                new DiscordButtonComponent(DSharpPlus.ButtonStyle.Danger, "false", "no")
            });

        var message = await messageBuilder.SendAsync(dialogue.DiscordDmChannel);
        var response = await message.WaitForButtonAsync(ctx.Member, TimeSpan.FromMinutes(1));

        if (response.TimedOut)
        {
            await message.RespondAsync("Creation ended because of a timeout.");
        }

        await response.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);

        return bool.Parse(response.Result.Id);
    }

    private static async Task<string> GetCorrectnessOfAnswer(CommandContext ctx, DiscordDialogue dialogue)
    {
        var messageBuilder = new DiscordMessageBuilder()
            .WithContent("Is this a correct answer?")
            .AddComponents(new DiscordComponent[]
            {
                new DiscordButtonComponent(DSharpPlus.ButtonStyle.Success, "correct", "Correct"),
                new DiscordButtonComponent(DSharpPlus.ButtonStyle.Danger, "incorrect", "Incorrect")
            });

        var message = await messageBuilder.SendAsync(dialogue.DiscordDmChannel);
        var response = await message.WaitForButtonAsync(ctx.Member, TimeSpan.FromMinutes(1));

        if (response.TimedOut)
        {
            await message.RespondAsync("Answer creation ended because of a timeout.");
        }

        await response.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);

        return response.Result.Id;
    }

    private async Task<QuestionBuilder> GetQuestionBuilderAsync(CommandContext ctx, DiscordDialogue dialogue, ulong createUserId)
    {
        var questionText = await dialogue.SendDmDialogueAsync<string>("Please send the question text.");
        var questionBuilder = new QuestionBuilder(createUserId, questionText);

        var waitResult = true;
        var answerCreationEmbed = new DiscordEmbedBuilder()
            .WithTitle("Answer Creation creation:")
            .WithColor(DiscordColor.Aquamarine);

        do
        {
            await dialogue.DiscordDmChannel.SendMessageAsync(answerCreationEmbed);
            var answer = await GetAnswerBuilderAsync(ctx, dialogue);

            questionBuilder.AddAnswer(answerBuilder =>
            {
                answerBuilder.WithName(answer.Name)
                    .WithDescription(answer.Description)
                    .WithPoints(answer.Points);

                if (answer.Correct)
                {
                    answerBuilder.IsCorrect();
                }
            });

            waitResult = await AskForMoreDialogueAsync(ctx, dialogue, "Do you want to create more answers?");
        } while (waitResult);

        return questionBuilder;
    }

    private async Task<AnswerBuilder> GetAnswerBuilderAsync(CommandContext ctx, DiscordDialogue dialogue)
    {
        var answerName = await dialogue.SendDmDialogueAsync<string>("Please send the answer name, something like `a), 1) ...`!");
        var answerText = await dialogue.SendDmDialogueAsync<string>("Please send the answer text.");
        var points = await dialogue.SendDmDialogueAsync<int>("Pls send the points you get for choosing this answer!");

        AnswerBuilder answerBuilder = new();
        answerBuilder.WithName(answerName)
            .WithDescription(answerText)
            .WithPoints(points);

        var correctnessState = await GetCorrectnessOfAnswer(ctx, dialogue);

        if (correctnessState == "correct")
        {
            answerBuilder.IsCorrect();
        }
        else
        {
            answerBuilder.IsIncorrect();
        }

        await dialogue.DiscordDmChannel.SendMessageAsync(answerBuilder.BuildEmbed(ctx.Member));

        return answerBuilder;
    }
}
