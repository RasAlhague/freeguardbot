﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBot.BotCommands.Enums;
using FreeGuardBot.Converters;
using FreeGuardBot.Services;
using FreeGuardBotLib.AccessManagement;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

/// <summary>
/// Contains the Commands for the blacklist.
/// </summary>
[Group("blacklist")]
[Description("Commands for the blacklist.")]
public class BlacklistCommandModule : LogableCommandModule
{
    private const string _createEntryDescription = "Command to create blacklist entries. Will open a dm channel. Please have a lookup player string ready.";
    private const string _deleteEntryDescription = "Command for deleting blacklist entries. This is not recommended.";
    private const string _deleteEntryIdDescription = "The id of th blacklist entry you want to delete.";
    private const string _updateEntryDescription = "Command for updating blacklist entries.";
    private const string _exportListDescription = "Command for exporting the blacklist entries into a csv file.";
    private const string _searchEntryDescription = "Command for searching blacklist entries.";
    private const string _searchEntrySearchTypeDescription = "The type of search you want to execute. Possible are: `steamid`, `playfabid`, `name` and `id`.";
    private const string _searchEntrySearchValueDescription = "The concrete search value.";

    public BlacklistCommandModule(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    /// <summary>
    /// Command for creating new blacklist entries. Requests a lookup player string
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.CreateEntry, Rights.ManageEntries, Rights.UseList, Rights.ManageBot)]
    [Command(BlacklistCommandNameConstants.CreateEntryCommand)]
    [Description(_createEntryDescription)]
    public async Task CreateEntryCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);
        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);

        var lookUpPlayer = await dialogue.SendDmDialogueAsync<string>($"Pls send the lookup player string! If you dont know what is meant please use `{ctx.Prefix}misc {MiscCommandNameConstants.LookUpPlayerHelpName}`", 
            x => Regex.IsMatch(x, LookUpPlayerData.LookupPlayerRegex));

        if (string.IsNullOrEmpty(lookUpPlayer))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        if (string.IsNullOrEmpty(lookUpPlayer))
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("Stopped execution of command!");
            await ctx.RespondAsync("Failed to execute command. Provided wrong input multiple times.");
            await ctx.Message.AddFailureReaction(ctx.Client);

            return;
        }

        var reason = await dialogue.SendDmDialogueAsync<string>("Pls send the reason!");

        if (string.IsNullOrEmpty(reason))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        ctx.Client.Logger.LogDebug($"LookupPlayer:{lookUpPlayer};Reason:{reason}");

        LookUpPlayerData lookUpPlayerData = new(lookUpPlayer);

        using FreeGuardDbContext dbContext = new();

        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);
        var entry = await dbContext.Blacklistentries
            .SingleOrDefaultAsync(x => x.SteamId == lookUpPlayerData.SteamId && x.GuildId == guild.Id);

        if (entry is null)
        {
            entry = new Blacklistentry()
            {
                Username = lookUpPlayerData.Name,
                SteamId = lookUpPlayerData.SteamId,
                PlayfabId = lookUpPlayerData.PlayFabId,
                Reason = reason,
                CreateDate = DateTime.UtcNow,
                CreateUserId = ctx.Member.Id,
                GuildId = guild.Id
            };

            await dbContext.AddAsync(entry);
            await dbContext.SaveChangesAsync();

            await dialogue.DiscordDmChannel.SendMessageAsync("New blacklist entry created!");

            await new DiscordMessageBuilder()
                .WithEmbed(CommandHelpers.GetBlacklistEntryEmbed(ctx, entry))
                .SendAsync(ctx.Channel);
        }
        else
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("The entry already exists!");
        }
    }

    /// <summary>
    /// Command for deleting entries on the blacklist. Does request a lookup player string.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.DeleteEntry, Rights.ManageBot, Rights.ManageEntries)]
    [Command(BlacklistCommandNameConstants.DeleteEntryCommand)]
    [Description(_deleteEntryDescription)]
    public async Task DeleteEntryCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var directChannel = await ctx.Member.CreateDmChannelAsync();

        await directChannel.SendMessageAsync("Pls send the lookup player string.");
        var lookUpPlayerString = await dialogue.SendDmDialogueAsync<string>(
            "Pls send the lookup player string.",
            x => Regex.IsMatch(x, LookUpPlayerData.LookupPlayerRegex)
        );

        if (string.IsNullOrEmpty(lookUpPlayerString))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        var lookUpPlayerData = new LookUpPlayerData(lookUpPlayerString);
        var blEntry = await Blacklistentry.GetAsync(dbContext, lookUpPlayerData.SteamId, guild.Id);

        await DeleteEntryAsync(blEntry, dbContext, ctx, dialogue);
    }

    /// <summary>
    /// Command for deleting blacklist entries. Does not request a lookup player string.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="blacklistEntryId">The id of the blacklist entry which should be deleted.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.DeleteEntry, Rights.ManageBot, Rights.ManageEntries)]
    [Command(BlacklistCommandNameConstants.DeleteEntryCommand)]
    [Description(_deleteEntryDescription)]
    public async Task DeleteEntryCommand(CommandContext ctx, [Description(_deleteEntryIdDescription)] int blacklistEntryId)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var blEntry = await dbContext.Blacklistentries.FindAsync(blacklistEntryId);

        await DeleteEntryAsync(blEntry, dbContext, ctx, dialogue);
    }

    /// <summary>
    /// Command for updating blacklist entries.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.UpdateEntry, Rights.ManageBot, Rights.ManageEntries)]
    [Command(BlacklistCommandNameConstants.UpdateEntryCommand)]
    [Description(_updateEntryDescription)]
    public async Task UpdateEntryCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        await ctx.RespondAsync("Not implemented yet!");
    }

    /// <summary>
    /// Command for exporting the blacklist entries into a csv file.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ExportList, Rights.ManageBot)]
    [Command(BlacklistCommandNameConstants.ExportListCommand)]
    [Description(_exportListDescription)]
    public async Task ExportListCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        StringBuilder csvBuilder = new StringBuilder();
        csvBuilder.AppendLine("id;username;steamid;playfabid;reason;createdate;createuserid;modifiydate;modifyuserid");

        foreach (var entry in await dbContext.Blacklistentries.Where(x => x.GuildId == guild.Id).ToListAsync())
        {
            csvBuilder.AppendLine($"{entry.Id};{entry.Username};{entry.SteamId};{entry.PlayfabId};{entry.Reason.Replace(';', ',')};{entry.CreateDate};{entry.CreateUserId};{entry.ModifyDate};{entry.ModifyUserId}");
        }

        await File.WriteAllTextAsync("./data.csv", csvBuilder.ToString());

        using var fs = new FileStream("./data.csv", FileMode.Open);

        var response = new DiscordMessageBuilder()
            .WithContent("Current blacklist entries:")
            .WithFile($"blacklist_{DateTime.Now.ToString("yyyy_MM_dd")}.csv", fs)
            .WithReply(ctx.Message.Id);

        await response.SendAsync(ctx.Channel);
    }

    /// <summary>
    /// Command for searching entries in the blacklist. Does request a lookup player string.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.SearchEntries, Rights.UseList, Rights.ManageBot, Rights.ManageEntries)]
    [Command(BlacklistCommandNameConstants.SearchEntryCommand)]
    [Description(_searchEntryDescription)]
    public async Task SearchEntryCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);
        InteractivityResult<ComponentInteractionCreateEventArgs> searchTypeResult = await GetSearchTypes(ctx, dialogue);
        var searchTypes = searchTypeResult.Result.Values;

        using var dbContext = new FreeGuardDbContext();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var query = dbContext.Blacklistentries.Where(x => x.GuildId == guild.Id);

        foreach (var searchType in searchTypes)
        {
            query = searchType switch
            {
                "id" => await GetIdQueryAsync(query, dialogue),
                "name" => await GetNameQueryAsync(query, dialogue),
                "steam_id" => await GetSteamIdQueryAsync(query, dialogue),
                "playfab_id" => await GetPlayfabIdQueryAsync(query, dialogue),
                _ => throw new ArgumentException("Invalid dropdown value!"),
            };
        }

        var queryResults = await query.ToListAsync();

        if (queryResults.Any())
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("Found results: ");

            var pages = new List<Page>();

            foreach (var searchResult in queryResults)
            {
                var page = new Page
                {
                    Embed = CommandHelpers.GetBlacklistEntryEmbed(ctx, searchResult)
                };

                pages.Add(page);
            }

            await dialogue.DiscordDmChannel.SendPaginatedMessageAsync(ctx.Member, pages, behaviour: PaginationBehaviour.WrapAround);
            await ctx.Message.DeleteAsync();
        }
        else
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("No entry found!");
            await ctx.Message.DeleteAsync();
        }
    }    

    /// <summary>
    /// Command for searching blacklist entries. 
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="searchType">The search type.</param>
    /// <param name="searchValue">The search value.</param>
    /// <returns></returns>
    [RequireGuild]
    [CheckUserAccess(Rights.SearchEntries, Rights.UseList, Rights.ManageBot, Rights.ManageEntries)]
    [Command(BlacklistCommandNameConstants.SearchEntryCommand)]
    [Description(_searchEntryDescription)]
    public async Task SearchEntryCommand(CommandContext ctx, [Description(_searchEntrySearchTypeDescription)] SearchType searchType, [Description(_searchEntrySearchValueDescription)] string searchValue)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using var dbContext = new FreeGuardDbContext();

        int id = searchType switch
        {
            SearchType.EntryId => int.Parse(searchValue),
            _ => 0,
        };

        IEnumerable<Blacklistentry> searchResults = searchType switch
        {
            SearchType.SteamId => await dbContext.Blacklistentries
                .Where(x => x.SteamId == searchValue)
                .ToListAsync(),
            SearchType.PlayfabId => await dbContext.Blacklistentries
                .Where(x => x.PlayfabId == searchValue)
                .ToListAsync(),
            SearchType.IngameName => await dbContext.Blacklistentries
                .Where(x => x.Username.Contains(searchValue))
                .ToListAsync(),
            SearchType.EntryId => await dbContext.Blacklistentries
                .Where(x => x.Id == id)
                .ToListAsync(),
            _ => Enumerable.Empty<Blacklistentry>(),
        };

        if (searchResults.Any())
        {
            await ctx.RespondAsync("Found results: ");


            var pages = new List<Page>();

            foreach (var searchResult in searchResults)
            {
                var page = new Page
                {
                    Embed = CommandHelpers.GetBlacklistEntryEmbed(ctx, searchResult)
                };

                pages.Add(page);
            }

            await ctx.Channel.SendPaginatedMessageAsync(ctx.Member, pages, behaviour: PaginationBehaviour.WrapAround);
        }
        else
        {
            await ctx.RespondAsync("No entry found!");
        }

    }

    private async Task DeleteEntryAsync(Blacklistentry blEntry, DbContext dbContext, CommandContext ctx, DiscordDialogue dialogue)
    {
        var reason = await dialogue.SendDmDialogueAsync<string>("Pls send a reason for the deletion.");

        if (string.IsNullOrEmpty(reason))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        if (blEntry is null)
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("Cant delete a non existing entry!");
        }
        else
        {
            var embed = new DiscordEmbedBuilder()
                .WithColor(new DiscordColor(0, 49, 83))
                .WithTitle($"*{ctx.Member.DisplayName}* deleted the entry *Nr.{blEntry.Id}*! Reason: *{reason}*")
                .Build();

            dbContext.Remove(blEntry);
            await dbContext.SaveChangesAsync();

            await dialogue.DiscordDmChannel.SendMessageAsync("Deleted entry from blacklist!");

            await ctx.RespondAsync(embed);
        }
    }

    private async Task<IQueryable<Blacklistentry>> GetPlayfabIdQueryAsync(IQueryable<Blacklistentry> query, DiscordDialogue dialogue)
    {
        var result = await dialogue.SendDmDialogueAsync<string>("Please send the playfabid of the entry you search!");

        return query.Where(x => x.PlayfabId == result);
    }

    private async Task<IQueryable<Blacklistentry>> GetSteamIdQueryAsync(IQueryable<Blacklistentry> query, DiscordDialogue dialogue)
    {
        var result = await dialogue.SendDmDialogueAsync<string>("Please send the steamid of the entry you search!");

        return query.Where(x => x.SteamId == result);
    }

    private async Task<IQueryable<Blacklistentry>> GetNameQueryAsync(IQueryable<Blacklistentry> query, DiscordDialogue dialogue)
    {
        var result = await dialogue.SendDmDialogueAsync<string>("Please send the name of the entry you search!");

        return query.Where(x => x.Username.Contains(result));
    }

    private async Task<IQueryable<Blacklistentry>> GetIdQueryAsync(IQueryable<Blacklistentry> query, DiscordDialogue dialogue)
    {
        var result = await dialogue.SendDmDialogueAsync<int>("Please send the id of the entry you search!");

        return query.Where(x => x.Id == result);
    }

    private static async Task<InteractivityResult<ComponentInteractionCreateEventArgs>> GetSearchTypes(CommandContext ctx, DiscordDialogue dialogue)
    {
        var options = new List<DiscordSelectComponentOption>()
        {
            new DiscordSelectComponentOption("Id", "id", "Searches by the internal id of the entries."),
            new DiscordSelectComponentOption("Name", "name", "Searches by the name or parts of a name of the player."),
            new DiscordSelectComponentOption("SteamId", "steam_id", "Searches by the steam id of players."),
            new DiscordSelectComponentOption("PlayfabId", "playfab_id", "Searches by the playfab id of players."),
        };

        string dropdownId = "search_type";
        var dropdown = new DiscordSelectComponent(dropdownId, null, options, false, 1, 4);

        var builder = new DiscordMessageBuilder()
            .WithContent("Please choose the type of search you want to do!")
            .AddComponents(dropdown);
        var message = await builder.SendAsync(dialogue.DiscordDmChannel);

        var searchTypeResult = await message.WaitForSelectAsync(ctx.Member, dropdownId, TimeSpan.FromMinutes(5));
        if (searchTypeResult.TimedOut)
        {
            await message.RespondAsync("Searchtype selection timed out!");
        }

        await searchTypeResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
        return searchTypeResult;
    }
}
