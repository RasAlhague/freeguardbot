﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBot.Services;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

/// <summary>
/// Contains commands for recruitment.
/// </summary>
[Group("recruit")]
[Description("Commands for the recruitment.")]
public class RecruitmentCommandModule : LogableCommandModule
{
    private const string _createRecruitDescription = "Creates a new recruit. Will open a dm channel with the user if he has the permission to use it.";
    private const string _fixRecruitsDescription = "Fixes the recruitment messages. Use this when a recruit is not created before he joined the server.";
    private const string _updateRecruitsDescription = "Updates the recruit.";

    public RecruitmentCommandModule(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    /// <summary>
    /// Creates a new recruit. Will open a dm channel with the user if he has the permission to use it.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.CreateRecruit, Rights.ManageBot, Rights.ManageRecruits, Rights.UseRecruits)]
    [Command(RecruitmentCommandNameConstants.CreateRecruitName)]
    [Description(_createRecruitDescription)]
    public async Task CreateRecruitCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);

        ulong recruiterId = ctx.Member.Id;

        var steamId = await dialogue.SendDmDialogueAsync<string>(
            "Pls send the steam id of the person to recruit. Steam links are fine as long as they dont contain a custom steam id. In case of a custom steam id please use https://steamid.io/lookup!",
            x => Regex.IsMatch(x, RegexConstants.Steam64Regex)
        );

        if (string.IsNullOrEmpty(steamId))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        if (Regex.IsMatch(steamId, RegexConstants.SteamLinkRegex))
        {
            Regex regex = new(RegexConstants.Steam64Regex);

            var match = regex.Match(steamId);

            steamId = match.Captures[0].Value;
        }

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        if (CheckBlacklist(dbContext, guild.Id, steamId))
        {
            await dialogue.DiscordDmChannel.SendMessageAsync("This person is on the blacklist.");
            await dialogue.DiscordDmChannel.SendMessageAsync("Found entry:");

            var entry = await dbContext.Blacklistentries
                .SingleOrDefaultAsync(x => x.GuildId == guild.Id && x.SteamId == steamId);
            var embed = CommandHelpers.GetBlacklistEntryEmbed(ctx, entry);

            await dialogue.DiscordDmChannel.SendMessageAsync(embed);

            string confirmation = await dialogue.SendDmDialogueAsync<string>(
                "Are you sure you want to recruit this person? (yes/no)",
                inputValue => inputValue == "yes" || inputValue == "no"
            );

            if (confirmation == "no")
            {
                await dialogue.DiscordDmChannel.SendMessageAsync("Recruitment has been aborted!");

                return;
            }
        }

        string discordName = await dialogue.SendDmDialogueAsync<string>(
            $"Please give me the discord name. It should similar to this: {ctx.Client.CurrentUser.Username}#{ctx.Client.CurrentUser.Discriminator}",
            x => Regex.IsMatch(x, RegexConstants.DiscordTag)
        );

        if (string.IsNullOrEmpty(discordName))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        string ingameName = await dialogue.SendDmDialogueAsync<string>("Please give me the ingame name.");

        if (string.IsNullOrEmpty(ingameName))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        string notice = await dialogue.SendDmDialogueAsync<string>("If you have any notes for the recruit pls write them, or write 'none'.");

        if (string.IsNullOrEmpty(notice))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        if (notice.ToLower() == "none")
        {
            notice = null;
        }

        Recruit recruit = new()
        {
            Name = ingameName,
            DiscordName = discordName,
            SteamId = steamId,
            Joined = "false",
            Left = "false",
            RecruiterId = recruiterId,
            GuildId = guild.Id,
            CreateUserId = recruiterId,
            CreateDate = DateTime.UtcNow,
            Notice = notice,
        };

        await dbContext.Recruits
            .AddAsync(recruit);
        await dbContext.SaveChangesAsync();

        await dialogue.DiscordDmChannel
            .SendMessageAsync("The recruit has been created, pls send him the invite following invite link:");
        var inviteLink = await ctx.Guild
            .GetDefaultChannel()
            .CreateInviteAsync(max_age: 0, max_uses: 1, reason: "Invite of new recruit.");

        await dialogue.DiscordDmChannel
            .SendMessageAsync(inviteLink.ToString());

        await dialogue.DiscordDmChannel
            .SendMessageAsync("As soon as the recruit joins he will get the appropariate rolls and will be displayed in recruiting.");
    }



    /// <summary>
    /// Fixes the recruitment messages. Use this when a recruit is not created before he joined the server.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot, Rights.ManageRecruits)]
    [Command(RecruitmentCommandNameConstants.FixRecruitsName)]
    [Description(_fixRecruitsDescription)]
    public async Task FixRecruitsCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();

        var guild = await Guild.GetOrCreateGuildAsync(dbContext, ctx.Guild.Id, ctx.Guild.Name);
        var recruits = await dbContext.Recruits
            .Where(x =>
                x.GuildId == guild.Id &&
                x.Joined == "false"
            ).ToListAsync();

        foreach (var recruit in recruits)
        {
            var member = ctx.Guild.Members.Values
                .FirstOrDefault(x => $"{x.Username}#{x.Discriminator}" == recruit.DiscordName);

            if (member == null)
            {
                continue;
            }
            await CommandHelpers.UpdateRecruitAsync(member, dbContext, guild, recruit);

            if (guild.RecruitmentChannelId.HasValue)
            {
                await CommandHelpers.SendJoinMessage(member, ctx.Guild, recruit, guild.RecruitmentChannelId.Value, ctx.Client.CurrentUser.Username);
            }

        }
    }

    /// <summary>
    /// Command for exporting the blacklist entries into a csv file.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ExportList, Rights.ManageBot)]
    [Command(RecruitmentCommandNameConstants.ExportListName)]
    [Description("Exports the blacklist to a csv file.")]
    public async Task ExportListCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        StringBuilder csvBuilder = new StringBuilder();
        csvBuilder.AppendLine("id;recruitid;recruiterid;name;discordname;steamid;joined;left;notice;createdate;createuserid;modifydate;modifyuserid");

        foreach (var entry in await dbContext.Recruits.Where(x => x.GuildId == guild.Id).ToListAsync())
        {
            csvBuilder.AppendLine($"{entry.Id};{entry.RecruitId};{entry.RecruiterId};{entry.Name};{entry.DiscordName};{entry.SteamId};{entry.Joined};{entry.Left};{entry.Notice?.Replace(';', ',')};{entry.CreateDate};{entry.CreateUserId};{entry.ModifyDate};{entry.ModifyUserId}");
        }

        await File.WriteAllTextAsync("./data.csv", csvBuilder.ToString());

        using var fs = new FileStream("./data.csv", FileMode.Open);

        var response = new DiscordMessageBuilder()
            .WithContent("Current recruit entries:")
            .WithFile($"recruit_{DateTime.Now.ToString("yyyy_MM_dd")}.csv", fs)
            .WithReply(ctx.Message.Id);

        await response.SendAsync(ctx.Channel);
    }

    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot, Rights.ManageRecruits, Rights.UpdateRecruit)]
    [Command(RecruitmentCommandNameConstants.UpdateRecruitName)]
    [Description(_updateRecruitsDescription)]
    public async Task UpdateRecruitsCommand(CommandContext ctx, int id)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetOrCreateGuildAsync(dbContext, ctx.Guild.Id, ctx.Guild.Name);

        if (!dbContext.Recruits.Any(x => x.Id == id))
        {
            await ctx.RespondAsync($"No recruit entry with id '{id}' could be found!");
            return;
        }

        DiscordDialogue dialogue = new();
        await dialogue.InitializeAsync(ctx);

        InteractivityResult<ComponentInteractionCreateEventArgs> changeFieldsResult = await GetChangeFields(ctx, dialogue);
        var changeFields = changeFieldsResult.Result.Values;

        var recruit = await dbContext.Recruits.FindAsync(id);

        foreach (var field in changeFields)
        {
            switch (field)
            {
                case "recruit_id":
                    await UpdateRecruitIdAsync(recruit, dialogue);
                    break;
                case "recruiter_id":
                    await UpdateRecruiterIdAsync(recruit, dialogue);
                    break;
                case "name":
                    await UpdateNameAsync(recruit, dialogue);
                    break;
                case "discordname":
                    await UpdateDiscordNameAsync(recruit, dialogue);
                    break;
                case "steam_id":
                    await UpdateSteamIdAsync(recruit, dialogue);
                    break;
                case "joined":
                    await UpdateJoinedAsync(recruit, dialogue);
                    break;
                case "left":
                    await UpdateLeftAsync(recruit, dialogue);
                    break;
                case "notice":
                    await UpdateNoticeAsync(recruit, dialogue);
                    break;
                default:
                    throw new ArgumentException("This is not possible!");
            }
        }

        if(changeFields.Any())
        {
            recruit.ModifyDate = DateTime.UtcNow;
            recruit.ModifyUserId = ctx.Member.Id;

            dbContext.Recruits.Update(recruit);
            await dbContext.SaveChangesAsync();

            await dialogue.DiscordDmChannel.SendMessageAsync("Updated entry!");

            if (guild.RecruitmentChannelId.HasValue && recruit.Joined.ToLower() == true.ToString().ToLower())
            {
                var member = await ctx.Guild.GetMemberAsync(recruit.RecruitId.Value);

                await CommandHelpers.SendJoinMessage(member, ctx.Guild, recruit, guild.RecruitmentChannelId.Value, ctx.Client.CurrentUser.Username);
            }
        }
    }

    private async Task UpdateDiscordNameAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var name = await dialogue.SendDmDialogueAsync<string>($"Please send the new discord name!\nOld: `{recruit.DiscordName}`", x => Regex.IsMatch(x, RegexConstants.DiscordTag));

        if (name != null)
        {
            recruit.DiscordName = name;
        }
    }

    private async Task UpdateNoticeAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var notice = await dialogue.SendDmDialogueAsync<string>($"Please send the new notice information!\nOld: `{recruit.Notice}`");

        if (notice != null)
        {
            recruit.Notice = notice;
        }
    }

    private async Task UpdateLeftAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var left = await dialogue.SendDmDialogueAsync<string>($"Please send 'true' if he left or 'false' if he did not leave!\nOld: `{recruit.Left}`");

        if (left != null)
        {
            recruit.Left = left;
        }
    }

    private async Task UpdateJoinedAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var joined = await dialogue.SendDmDialogueAsync<string>($"Please send 'true' if he joined or 'false' if he did not join yet!\nOld: `{recruit.Joined}`");

        if (joined != null)
        {
            recruit.Joined = joined;
        }
    }

    private async Task UpdateSteamIdAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var steamId = await dialogue.SendDmDialogueAsync<string>($"Please send the new steam64 Id!\nOld: `{recruit.SteamId}`");

        if (steamId != null)
        {
            recruit.SteamId = steamId;
        }
    }

    private async Task UpdateNameAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var name = await dialogue.SendDmDialogueAsync<string>($"Please send the new name!\nOld: `{recruit.Name}`");

        if (name != null)
        {
            recruit.Name = name;
        }
    }

    private async Task UpdateRecruiterIdAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var recruiterId = await dialogue.SendDmDialogueAsync<string>($"Please send the new recruiter discord id!\nOld: `{recruit.RecruiterId}`", x => ulong.TryParse(x, out _));

        if (recruiterId != null)
        {
            recruit.RecruiterId = ulong.Parse(recruiterId);
        }
    }

    private async Task UpdateRecruitIdAsync(Recruit recruit, DiscordDialogue dialogue)
    {
        var recruitId = await dialogue.SendDmDialogueAsync<string>($"Please send the new recruit discord id!\nOld: `{recruit.RecruitId}`", x => ulong.TryParse(x, out _));

        if (recruitId != null)
        {
            recruit.RecruitId = ulong.Parse(recruitId);
        }
    }

    private bool CheckBlacklist(FreeGuardDbContext context, int guildId, string steamId)
    {
        return context.Blacklistentries.Any(x => x.GuildId == guildId && x.SteamId == steamId);
    }

    private static async Task<InteractivityResult<ComponentInteractionCreateEventArgs>> GetChangeFields(CommandContext ctx, DiscordDialogue dialogue)
    {
        var options = new List<DiscordSelectComponentOption>()
        {
            new DiscordSelectComponentOption("RecruitId", "recruit_id", "Changes the discord id of the recruit. Only use it if you experienced!"),
            new DiscordSelectComponentOption("RecruiterId", "recruiter_id", "Changes the discord id of the recruiter. Only use it if you experienced!"),
            new DiscordSelectComponentOption("Name", "name", "Changes the name of a recruit."),
            new DiscordSelectComponentOption("Discord name", "discordname", "Changes the discord name of a recruit."),
            new DiscordSelectComponentOption("Steam64Id", "steam_id", "Changes the steam64id."),
            new DiscordSelectComponentOption("Joined", "joined", "Changes the joined state of a recruit."),
            new DiscordSelectComponentOption("Left", "left", "Changes the left state of a recruit."),
            new DiscordSelectComponentOption("Notice", "notice", "Changes the notice field of a recruit.")
        };

        string dropdownId = "changefields";
        var dropdown = new DiscordSelectComponent(dropdownId, null, options, false, 1, 7);

        var builder = new DiscordMessageBuilder()
            .WithContent("Please choose the fields you want to change!")
            .AddComponents(dropdown);
        var message = await builder.SendAsync(dialogue.DiscordDmChannel);

        var changeFieldsResult = await message.WaitForSelectAsync(ctx.Member, dropdownId, TimeSpan.FromMinutes(5));
        if (changeFieldsResult.TimedOut)
        {
            await message.RespondAsync("Changefield selection timed out!");
        }

        await changeFieldsResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);
        return changeFieldsResult;
    }
}
