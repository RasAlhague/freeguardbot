﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{ 
    class VacationCommandNameConstants
    {
        public const string VacationNewCommandName = "start";
        public const string VacationListCommandName = "list";
        public const string VacationShowCommandName = "show";
        public const string VacationEndCommandName = "end";
        public const string ExportListName = "export";
    }
}
