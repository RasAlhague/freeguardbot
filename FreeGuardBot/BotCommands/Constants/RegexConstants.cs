﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    public static class RegexConstants
    {
        public const string PlayfabRegex = "[A-Z0-9]{16,18}";
        public const string Steam64Regex = "[0-9]{16,18}";
        public const string SteamLinkRegex = "https?://steamcommunity.com/profiles/[0-9]{16,18}";
        public const string DiscordTag = ".+#[0-9]{4}";
    }
}
