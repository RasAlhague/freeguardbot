﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    public static class ManagementCommandNameConstants
    {
        public const string SetRecruitmentChannelName = "set-recruit-channel";
        public const string DeleteRecruitmentChannelName = "del-recruit-channel";
        /// <summary>
        /// Constant for the ViewRightsCommand.
        /// </summary>
        public const string ViewRightsCommand = "view-rights";
        /// <summary>
        /// Constant for the SetRightCommand.
        /// </summary>
        public const string SetRightCommand = "set-right";
        /// <summary>
        /// Constant for the DeleteRightCommand.
        /// </summary>
        public const string DeleteRightCommand = "del-right";
        public const string SetGreetingChannelName = "set-greeting-channel";
        public const string DeleteGreetingChannelName = "del-greeting-channel";
        public const string SetGreetingMessageName = "set-greeting";
        public const string DeleteGreetingMessageName = "del-greeting";
        public const string AddReactionName = "add-reaction";
        public const string RemoveReactionName = "remove-reaction";
        public const string ViewReactionsName = "view-reaction";
        public const string SetRolesName = "set-roles";
        public const string ResetRolesName = "reset-roles";
    }
}
