﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    public static class RecruitmentCommandNameConstants
    {
        public const string CreateRecruitName = "new";
        public const string UpdateRecruitName = "update";
        public const string FixRecruitsName = "fix";
        public const string ExportListName = "export";
    }
}
