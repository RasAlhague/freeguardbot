﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    public static class SupportCommandNameConstants
    {
        public const string SuggestCommandName = "suggest";
        public const string ReportBugCommandName = "report-bug";
    }
}
