﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands.Constants
{
    public static class MiscCommandNameConstants
    {
        public const string LookUpPlayerHelpName = "lookupplayer-help";
        public const string VersionInfoName = "version";
        public const string CodeTestName = "codetest";
    }
}
