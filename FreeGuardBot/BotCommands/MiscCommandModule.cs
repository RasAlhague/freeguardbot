﻿using Dapper;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBot.Services;
using FreeGuardBotLib;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

/// <summary>
/// Contains multiple unorganized commands.
/// </summary>
[Group("misc")]
[Description("Alot of different commands.")]
public class MiscCommandModule : LogableCommandModule
{
    private const string _videoTutorialLink = "https://youtu.be/dJgYXu_IAYM";
    private const string _lookupPlayerDescription = "Sends a help for getting the lookupplayer string.";
    private const string _versionInfoDescription = "Command for displaying the version info. Not needed for anyone besides me the dev.";

    public MiscCommandModule(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    /// <summary>
    /// Sends a help for getting the lookupplayer string.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [Command(MiscCommandNameConstants.LookUpPlayerHelpName)]
    [Description(_lookupPlayerDescription)]
    public async Task LookUpPlayerHelpCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var embed = new DiscordEmbedBuilder()
           .WithColor(new DiscordColor(0, 49, 83))
           .WithTitle("LookUpPlayer help")
           .WithAuthor(ctx.Client.CurrentUser.Username)
           .AddField("Step 1.", "Make sure your console key is bound in settings.")
           .AddField("Step 2.", "Open console by pressing your console key twice.")
           .AddField("Step 3.", "Write `PlayerList` into the console. Now it shows you a list of players with numbers infront of names.")
           .AddField("Step 4.", "Search for the player which you want to get the string of and remember the number.")
           .AddField("Step 5.", "Type `LookUpPlayer <number>` into the console. For the `<number>` insert the number of the person.")
           .AddField("Finish", "Now it has copied the whole lookup player string into the console.")
           .AddField("Video tutorial", _videoTutorialLink)
           .Build();

        await ctx.RespondAsync(embed);
    }

    /// <summary>
    /// Command for displaying the version info. Not needed for anyone besides me the dev.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(MiscCommandNameConstants.VersionInfoName)]
    [Description(_versionInfoDescription)]
    public async Task VersionInfoCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);


        var repoInfo = RepositoryInformation.GetRepositoryInformationForPath("../../../../.git");

        string trackedBranch = "Repo info not available!";
        string commitHash = "Repo info not available!";
        string message = "Repo info not available!";
        string author = "Repo info not available!";

        if (repoInfo != null)
        {
            var first = repoInfo.Log.First();

            trackedBranch = repoInfo.TrackedBranchName;
            commitHash = repoInfo.CommitHash;
            author = first.Author.Name;
            message = first.Message;
        }


        var embed = new DiscordEmbedBuilder()
            .WithColor(new DiscordColor(0, 49, 83))
            .WithTitle("Version Info")
            .WithAuthor(ctx.Client.CurrentUser.Username)
            .AddField("Branch name", trackedBranch)
            .AddField("Commit hash", commitHash)
            .AddField("Commit message", message)
            .AddField("Commit author", author)
            .AddField("Bot assembly version", typeof(MiscCommandModule).Assembly.GetName().Version.ToString(), inline: true)
            .AddField("Lib assembly version", typeof(RepositoryInformation).Assembly.GetName().Version.ToString(), inline: true);

        await ctx.RespondAsync(embed);
    }

    [RequireOwner]
    [Command("sql")]
    public async Task ExecuteSql(CommandContext ctx)
    {
        var dm = new DiscordDialogue();
        await dm.InitializeAsync(ctx);

        string sql = await dm.SendDmDialogueAsync<string>("Please send the sql to execute: ");

        using var conn = new MySqlConnector.MySqlConnection(BotConfig.GetInstance().DbConnString);

        try
        {
            var result = await conn.QueryAsync(sql, commandTimeout: 300);
            var json = JsonConvert.SerializeObject(result);

            var fileName = $"{Guid.NewGuid()}.json";
            File.WriteAllText(fileName, json);
            var stream = File.OpenRead(fileName);

            var messageBuilder = new DiscordMessageBuilder()
                .WithFile(fileName, stream)
                .WithContent("Here are the results of your query: ");

            await ctx.RespondAsync(messageBuilder);
        }
        catch (Exception ex)
        {
            await ctx.RespondAsync("Something went wrong during execution.");

            ctx.Client.Logger.LogError("Something went wrong during execution of sql.", ex);
        }
    }
}
