﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using FreeGuardBot.Attributes;
using FreeGuardBot.BotCommands.Constants;
using FreeGuardBot.Services;
using FreeGuardBotLib.Controls;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBot.BotCommands;

/// <summary>
/// Contains the Commands for the blacklist.
/// </summary>
[Group("management")]
[Description("Contains commands for managing the bot.")]
public class ManagementCommandModule : LogableCommandModule
{
    #region Description-Constants
    private const string _viewRightDescription = "Command for viewing the rights of commands.";
    private const string _setRightDescription = "Command for setting rigths to a specific role.";
    private const string _setRightBotRightDescription = "The right which should be given to a role.";
    private const string _setRightDiscordRoleDescription = "The role which should get the right.";
    private const string _deleteRightDescription = "Command for deleting rights for bot commands.";
    private const string _deleteRightBotRightDescription = "The right which should be removed from a role.";
    private const string _deleteRightRoleDescription = "The role which should lose the right.";
    private const string _setRecruitmentDescription = "Sets the channel into which recruiment messages will be sent.";
    private const string _setRecruitmentChannelDescription = "The channel into which recruitment messages should be sent.";
    private const string _deleteRecruitmentDescription = "Unsets the binding of recruitment messages to a certain channel.";
    private const string _setGreetingChannelDescription = "Sets a channel as greeting channel so new recruits will be greeted by the bot there.";
    private const string _setGreetingChannelChannelDescription = "The channel where the greeting of new recruits should be send into.";
    private const string _deleteChannelGreetingDescription = "Unsets the current greeting channel if one exists.";
    private const string _setGreetingMessageDescription = "Sets the greeting message. Will open a dm channel with you.";
    private const string _deleteGreetingMessageDescription = "Removes the current greeting message if one exists.";
    private const string _addReactionDescription = "Adds new reactions to keywords.";
    private const string _addReactionKeywordDescription = "The keyword which triggers the reaction. If it consists of multiple words it must be in \"\".";
    private const string _removeReactionDescription = "Removes the reactions based on a certain keyword.";
    private const string _removeReactionKeywordDescription = "The keyword which should not get reactions anymore. If it consists of multiple words it must be in \"\".";
    private const string _viewReactionDescription = "Displays all keywords with the emojis which will be send.";
    #endregion

    public ManagementCommandModule(ICommandLogService commandLogService) : base(commandLogService)
    {
    }

    /// <summary>
    /// Command for viewing the rights of commands.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ViewRights, Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.ViewRightsCommand)]
    [Description(_viewRightDescription)]
    public async Task ViewRightsCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var rights = await dbContext.Botrights.ToListAsync();
        var guild = await dbContext.Guilds.SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);

        var pages = new List<Page>();

        foreach (var right in rights)
        {
            DiscordEmbedBuilder embedBuilder = await CreateRightEntryEmbedAsync(ctx, dbContext, guild, right);

            pages.Add(new Page(embed: embedBuilder));
        }

        await ctx.Channel.SendPaginatedMessageAsync(ctx.Member, pages);
    }


    /// <summary>
    /// Command for setting rigths to a specific role.
    /// </summary>   
    /// <param name="ctx">The context of the command.</param>
    /// <param name="botright">The rigths for the bot commands.</param>
    /// <param name="discordRole">The role which should get the right.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.SetRights, Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.SetRightCommand)]
    [Description(_setRightDescription)]
    public async Task SetRightCommand(CommandContext ctx, [Description(_setRightBotRightDescription)] Rights botright, [Description(_setRightDiscordRoleDescription)] DiscordRole discordRole)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx, $"right:{botright};role:{discordRole.Id}");

        using FreeGuardDbContext dbContext = new();

        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);
        var role = await Role
            .GetOrCreateRoleAsync(dbContext, discordRole.Id, ctx.Member.Id, guild.Id);
        var right = await dbContext.Botrights
            .FindAsync((int)botright);
        var roleRight = await dbContext.Rolerights
            .SingleOrDefaultAsync(x => x.RoleId == role.Id && x.BotRightId == right.Id);

        if (roleRight is null)
        {
            roleRight = new Roleright()
            {
                RoleId = role.Id,
                BotRightId = right.Id,
                CreateUserId = ctx.Member.Id,
                CreateDate = DateTime.UtcNow
            };

            await dbContext.AddAsync(roleRight);
            await dbContext.SaveChangesAsync();

            await ctx.RespondAsync($"The role '{discordRole.Name}' has now the '{right.Name}' rights.");
        }
        else
        {
            await ctx.RespondAsync("This role already posses this right!");
        }
    }

    /// <summary>
    /// Command for deleting rights for bot commands.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="botright">The right which should be deleted.</param>
    /// <param name="discordRole">The role which should lose this right.</param>
    /// <returns>Returns just a task.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.DeleteRights, Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.DeleteRightCommand)]
    [Description(_deleteRightDescription)]
    public async Task DeleteRightCommand(CommandContext ctx, [Description(_deleteRightBotRightDescription)] Rights botright, [Description(_deleteRightRoleDescription)] DiscordRole discordRole)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx, $"right:{botright};role:{discordRole.Id}");

        using FreeGuardDbContext dbContext = new();

        var guild = await dbContext.Guilds
            .SingleOrDefaultAsync(x => x.GuildId == ctx.Guild.Id);
        var role = await Role
            .GetOrCreateRoleAsync(dbContext, discordRole.Id, ctx.Member.Id, guild.Id);
        var right = await dbContext.Botrights
            .FindAsync((int)botright);
        var roleRight = await dbContext.Rolerights
            .SingleOrDefaultAsync(x => x.RoleId == role.Id && x.BotRightId == right.Id);

        if (roleRight != null)
        {
            dbContext.Rolerights.Remove(roleRight);
            await dbContext.SaveChangesAsync();

            await ctx.RespondAsync($"The right '{right.Name}' has been removed from the role '{discordRole.Name}'.");
        }
        else
        {
            await ctx.RespondAsync("This role does not have the searched right!");
        }
    }

    /// <summary>
    /// Sets the channel into which recruiment messages will be sent.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="channel">The channel into which recruitment messages should be sent.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot, Rights.SetRecruitChannel)]
    [Command(ManagementCommandNameConstants.SetRecruitmentChannelName)]
    [Description(_setRecruitmentDescription)]
    public async Task SetRecruitmentChannelCommand(CommandContext ctx, [Description(_setRecruitmentChannelDescription)] DiscordChannel channel)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx, channel.Id.ToString());

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        if (guild.RecruitmentChannelId == channel.Id)
        {
            await ctx.RespondAsync("Channel is already set as recruitment channel!");

            return;
        }

        await UpdateRecruitmentChannelAsync(dbContext, guild, channel.Id);

        await ctx.RespondAsync("Channel has been set as recruitment channel!");
    }

    /// <summary>
    /// Unsets the binding of recruitment messages to a certain channel.
    /// </summary>
    /// <param name="ctx"></param>
    /// <returns></returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot, Rights.DeleteRecruitChannel)]
    [Command(ManagementCommandNameConstants.DeleteRecruitmentChannelName)]
    [Description(_deleteRecruitmentDescription)]
    public async Task DeleteRecruitmentChannelCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        if (!guild.RecruitmentChannelId.HasValue)
        {
            await ctx.RespondAsync("Channel is not set as recruitment channel!");

            return;
        }

        await UpdateRecruitmentChannelAsync(dbContext, guild, null);

        await ctx.RespondAsync("Channel has been removed as recruitment channel! **Please dont forget to set a new one!**");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="channel">The channel where the greeting of new recruits should be send into.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.SetGreetingChannelName)]
    [Description(_setGreetingChannelDescription)]
    public async Task SetGreetingChannelCommand(CommandContext ctx, [Description(_setGreetingChannelChannelDescription)] DiscordChannel channel)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx, channel.Id.ToString());

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        if (guild.WelcomeMessageChannel == channel.Id)
        {
            await ctx.RespondAsync("Channel is already set as greeting channel!");

            return;
        }

        await UpdateGreetingChannelAsync(dbContext, guild, channel.Id);

        await ctx.RespondAsync("Channel has been set as greeting channel!");
    }

    /// <summary>
    /// Unsets the current greeting channel if one exists.
    /// </summary>
    /// <param name="ctx">The command context.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.DeleteGreetingChannelName)]
    [Description(_deleteChannelGreetingDescription)]
    public async Task DeleteGreetingChannelCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        if (!guild.RecruitmentChannelId.HasValue)
        {
            await ctx.RespondAsync("Channel is not set as greeting channel!");

            return;
        }

        await UpdateGreetingChannelAsync(dbContext, guild, null);

        await ctx.RespondAsync("Channel has been removed as greeting channel! **Please dont forget to set a new one!**");
    }

    /// <summary>
    /// Sets the greeting message. Will open a dm channel with you.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.SetGreetingMessageName)]
    [Description(_setGreetingMessageDescription)]
    public async Task SetGreetingMessageCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        var dmDialogue = new DiscordDialogue();
        await dmDialogue.InitializeAsync(ctx);

        var message = await dmDialogue.SendDmDialogueAsync<string>("Pls send me the new greeting message! `$RECRUIT$` and `$RECRUITER$` can be used as placeholders.");

        if (string.IsNullOrEmpty(message))
        {
            await ctx.Message.AddFailureReaction(ctx.Client);
            return;
        }

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        await UpdateGreetingMessageAsync(dbContext, guild, message);

        await ctx.RespondAsync("New greeting message has been set");
    }

    /// <summary>
    /// Removes the current greeting message if one exists.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.DeleteGreetingMessageName)]
    [Description(_deleteGreetingMessageDescription)]
    public async Task DeleteGreetingMessageCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        await UpdateGreetingMessageAsync(dbContext, guild, null);

        await ctx.RespondAsync("Message has been removed as greeting message! **Please dont forget to set a new one!**");
    }

    /// <summary>
    /// Adds new reactions to keywords.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="keyword">The keyword which triggers the reaction. If it consists of multiple words it must be in \"\".</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.AddReactionName)]
    [Description(_addReactionDescription)]
    public async Task AddReactionCommand(CommandContext ctx, [Description(_addReactionKeywordDescription)] string keyword)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        Guild guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var message = await ctx.RespondAsync("Please react with the emotes for a keyword here!");
        var reactions = await message.CollectReactionsAsync(TimeSpan.FromSeconds(30));

        var messageReaction = new MessageReaction()
        {
            Keyword = keyword,
            GuildId = guild.Id,
            CreateDate = DateTime.UtcNow,
            CreateUserId = ctx.Member.Id
        };

        dbContext.MessageReactions.Add(messageReaction);
        await dbContext.SaveChangesAsync();

        foreach (var reaction in reactions.Where(x => x.Users.Any(x => x.Id == ctx.Member.Id)))
        {
            var emoji = await Emoji.GetOrCreate(dbContext, ctx.Member.Id, reaction.Emoji.Id, reaction.Emoji.GetDiscordName());

            var reactionEmoji = new ReactionEmoji()
            {
                MessageReactionId = messageReaction.Id,
                EmojiId = emoji.Id,
            };

            dbContext.ReactionEmojis.Add(reactionEmoji);
            await dbContext.SaveChangesAsync();
        }

        var emojisString = string.Join(' ', reactions.Select(x => x.Emoji.GetDiscordName()));

        await ctx.RespondAsync($"Reacting to keyword `{keyword}` with {emojisString}!");
    }

    /// <summary>
    /// Removes the reactions based on a certain keyword.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <param name="keyword">The keyword which should not get reactions anymore. If it consists of multiple words it must be in \"\".</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.RemoveReactionName)]
    [Description(_removeReactionDescription)]
    public async Task RemoveReactionCommand(CommandContext ctx, [Description(_removeReactionKeywordDescription)] string keyword)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        Guild guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var messageReaction = dbContext.MessageReactions.Include("ReactionEmojis").FirstOrDefault(x => x.Keyword == keyword && x.GuildId == guild.Id);
        foreach (var reactionEmoji in messageReaction.ReactionEmojis)
        {
            dbContext.ReactionEmojis.Remove(reactionEmoji);
        }
        dbContext.MessageReactions.Remove(messageReaction);
        await dbContext.SaveChangesAsync();

        await ctx.RespondAsync($"Removed Message reaction for `{keyword}`!");
    }

    /// <summary>
    /// Displays all keywords with the emojis which will be send.
    /// </summary>
    /// <param name="ctx">The context of the command.</param>
    /// <returns>No value.</returns>
    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.ViewReactionsName)]
    [Description(_viewReactionDescription)]
    public async Task ViewReactionsCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        Guild guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var reactionMessages = await dbContext.MessageReactions
            .Where(x => x.GuildId == guild.Id).ToListAsync();

        var pages = new List<Page>();

        foreach (var reactionMessage in reactionMessages)
        {
            var reactionEmojis = await dbContext.ReactionEmojis
                .Where(x => x.MessageReactionId == reactionMessage.Id)
                .Include("Emoji").ToListAsync();

            var embedBuilder = new DiscordEmbedBuilder()
                .WithColor(new DiscordColor(0, 0, 0))
                .WithAuthor(ctx.Client.CurrentUser.Username)
                .WithTitle("Reactions:")
                .WithDescription("Displays all reactions to certain keywords.")
                .AddField("Keyword:", reactionMessage.Keyword);

            foreach (var reactionEmoji in reactionEmojis)
            {
                var emoji = await ctx.Guild.GetEmojiAsync(reactionEmoji.Emoji.EmojiId);

                embedBuilder.AddField("Emotes", emoji, inline: true);
            }

            pages.Add(new Page(embed: embedBuilder));
        }

        if (!pages.Any())
        {
            await ctx.RespondAsync("No reactions found yet! Maybe you have to set them up first?!");
            return;
        }

        await ctx.Channel.SendPaginatedMessageAsync(ctx.Member, pages);
    }

    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.SetRolesName)]
    [Description("Sets roles for the bot.")]
    public async Task SetRolesCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        var roles = ctx.Guild.Roles;

        List<DiscordSelectComponentOption> roleTypeOptions = new()
        {
            new DiscordSelectComponentOption("Warning Roles", "warning_roles", "The roles for warnings."),
            new DiscordSelectComponentOption("Join Roles", "join_roles", "The roles for new members."),
            new DiscordSelectComponentOption("Vacation Roles", "vacation_roles", "The roles for vacations."),
            new DiscordSelectComponentOption("Ticket Manager Roles", "ticket_manager_roles", "The roles for ticket managers."),
        };

        string roleTypeDropdownName = "role_types_dropdown";
        var roleTypeDropdown = new DiscordSelectComponent(roleTypeDropdownName, null, roleTypeOptions, false);

        var builder = new DiscordMessageBuilder()
            .WithContent("Please choose the type of role you want to set!")
            .AddComponents(roleTypeDropdown);

        var message = await builder.SendAsync(ctx.Channel);

        var roleTypeResult = await message.WaitForSelectAsync(ctx.Member, roleTypeDropdownName, TimeSpan.FromMinutes(1));

        if (roleTypeResult.TimedOut)
        {
            await message.RespondAsync("Roles not changed, dropdown timed out!");
            return;
        }

        await roleTypeResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);


        List<DiscordSelectComponentOption> roleOptions = new();

        foreach (var role in roles)
        {
            roleOptions.Add(new DiscordSelectComponentOption(role.Value.Name, role.Key.ToString()));
        }

        PaginatedSelectMenu paginatedSelectMenu = new(10, roleOptions);
        int pageNumber = 1;
        paginatedSelectMenu.CurrentPage = pageNumber;

        InteractivityResult<ComponentInteractionCreateEventArgs> roleResult;

        List<string> paginatedResults = new List<string>();

        do
        {
            roleResult = await paginatedSelectMenu.DisplayCurrentPageAsync(ctx, "Please choose the roles to set!", TimeSpan.FromMinutes(5));

            if (roleResult.TimedOut)
            {
                await message.RespondAsync("Roles not changed, dropdown timed out!");
                return;
            }

            paginatedResults.AddRange(roleResult.Result.Values);
            paginatedResults.RemoveAll(x => x == "page_changer");

            paginatedSelectMenu.Next();

        } while (roleResult.Result.Values.Any(x => x == "page_changer"));

        string roleType = roleTypeResult.Result.Values[0];

        switch (roleType)
        {
            case "vacation_roles":
                await SetVacationRoles(paginatedResults, dbContext, ctx, guild);
                break;
            case "join_roles":
                await SetJoinRoles(paginatedResults, dbContext, ctx, guild);
                break;
            case "warning_roles":
                await SetWarningRoles(paginatedResults, dbContext, ctx, guild);
                break;
            case "ticket_manager_roles":
                await SetTicketManagerRoles(paginatedResults, dbContext, ctx, guild);
                break;
            default:
                throw new ArgumentException("Invalid dropdown value!");
        }
    }

    [RequireGuild]
    [CheckUserAccess(Rights.ManageBot)]
    [Command(ManagementCommandNameConstants.ResetRolesName)]
    [Description("Resets the vacation role.")]
    public async Task ResetRolesCommand(CommandContext ctx)
    {
        await _commandLogService.LogCommandExecutionAsync(ctx);

        using FreeGuardDbContext dbContext = new();
        var guild = await Guild.GetAsync(dbContext, ctx.Guild.Id);

        List<DiscordSelectComponentOption> roleTypeOptions = new()
        {
            new DiscordSelectComponentOption("Warning Roles", "warning_roles", "The roles for warnings."),
            new DiscordSelectComponentOption("Join Roles", "join_roles", "The roles for new members."),
            new DiscordSelectComponentOption("Vacation Roles", "vacation_roles", "The roles for vacations."),
            new DiscordSelectComponentOption("Ticket manager Roles", "ticket_manager_roles", "The roles for ticket managers."),
        };

        string roleTypeDropdownName = "role_types_dropdown";
        var roleTypeDropdown = new DiscordSelectComponent(roleTypeDropdownName, null, roleTypeOptions, false);

        var builder = new DiscordMessageBuilder()
            .WithContent("Please choose the type of role you want to reset!")
            .AddComponents(roleTypeDropdown);

        var message = await builder.SendAsync(ctx.Channel);

        var roleTypeResult = await message.WaitForSelectAsync(ctx.Member, roleTypeDropdownName, TimeSpan.FromMinutes(1));

        if (roleTypeResult.TimedOut)
        {
            await message.RespondAsync("Roles not changed, dropdown timed out!");
        }

        await roleTypeResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);

        List<DiscordSelectComponentOption> roleOptions = new();

        string roleType = roleTypeResult.Result.Values[0];
        var roles = roleType switch
        {
            "join_roles" => await dbContext.Roles.Where(x => x.IsJoinRole == true.ToString()).ToListAsync(),
            "vacation_roles" => await dbContext.Roles.Where(x => x.IsVacationRole == true.ToString()).ToListAsync(),
            "warning_roles" => await dbContext.Roles.Where(x => x.IsWarningRole == true.ToString()).ToListAsync(),
            "ticket_manager_roles" => await dbContext.Roles.Where(x => x.IsTicketManager).ToListAsync(),
            _ => throw new ArgumentException("Invalid dropdown value!"),
        };

        if (!roles.Any())
        {
            await ctx.RespondAsync("This role type has no roles associated!");

            return;
        }

        foreach (var role in roles)
        {
            if (ctx.Guild.Roles.TryGetValue(role.DiscordRoleId, out DiscordRole dsRole))
            {
                roleOptions.Add(new DiscordSelectComponentOption(dsRole.Name, dsRole.Id.ToString()));
            }
            else
            {
                var brokenRoles = await dbContext.Rolerights.Where(x => x.RoleId == role.Id).ToListAsync();
                role.IsJoinRole = "False";
                role.IsVacationRole = "False";
                role.IsWarningRole = "False";
                role.IsTicketManager = false;
                dbContext.Rolerights.RemoveRange(brokenRoles);
                dbContext.Roles.Update(role);
                await dbContext.SaveChangesAsync();
            }
        }

        string rolesDropdownName = "roles_dropdown";
        var rolesDropdown = new DiscordSelectComponent(rolesDropdownName, null, roleOptions, false, 1, roleOptions.Count);

        builder.ClearComponents();
        builder
            .WithContent("Please choose the roles to reset!")
            .AddComponents(rolesDropdown);

        message = await builder.SendAsync(ctx.Channel);

        var roleResult = await message.WaitForSelectAsync(ctx.Member, rolesDropdownName, TimeSpan.FromMinutes(1));

        if (roleResult.TimedOut)
        {
            await message.RespondAsync("Roles not changed, dropdown timed out!");
        }

        await roleResult.Result.Interaction.CreateResponseAsync(DSharpPlus.InteractionResponseType.DeferredMessageUpdate);

        switch (roleType)
        {
            case "vacation_roles":
                await ResetVacationRoles(roleResult.Result.Values, dbContext, ctx, guild);
                break;
            case "join_roles":
                await ResetJoinRoles(roleResult.Result.Values, dbContext, ctx, guild);
                break;
            case "warning_roles":
                await ResetWarningRoles(roleResult.Result.Values, dbContext, ctx, guild);
                break;
            case "ticket_manager_roles":
                await ResetTicketManagerRoles(roleResult.Result.Values, dbContext, ctx, guild);
                break;
            default:
                throw new ArgumentException("Invalid dropdown value!");
        }
    }

    private async Task SetWarningRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var warningRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (warningRole.HasWarning)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** already is set up as warning role!");
            }
            else
            {
                await SetWarningRoleAsync(dbContext, warningRole, ctx.Member.Id);
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** is now set as a warning role!");
            }
        }
    }

    private async Task SetTicketManagerRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var ticketManagerRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (ticketManagerRole.IsTicketManager)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** already is set up as ticket manager role!");
            }
            else
            {
                await SetTicketManagerRoleAsync(dbContext, ticketManagerRole, ctx.Member.Id);
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** is now set as a ticket manager role!");
            }
        }
    }

    private async Task SetJoinRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var joinRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (joinRole.HasJoin)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** already is set up as join role!");
            }
            else
            {
                await SetJoinRoleAsync(dbContext, joinRole, ctx.Member.Id);
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** is now set as a join role!");
            }
        }
    }

    private async Task SetVacationRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var vacationRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (vacationRole.HasVacation)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** already is set up as vacation role!");
            }
            else
            {
                await SetVacationRoleAsync(dbContext, vacationRole, ctx.Member.Id);
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** is now set as a vacation role!");
            }
        }
    }

    private async Task ResetWarningRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var warningRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (!warningRole.HasWarning)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** has not the warning role!");
            }
            else
            {
                await DeleteWarningRoleAsync(dbContext, warningRole, ctx.Member.Id);
                await ctx.RespondAsync($"The warning role has been removed from the **_'{dsRole.Name}'_** role!");
            }
        }
    }

    private async Task ResetTicketManagerRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var ticketManagerRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (!ticketManagerRole.IsTicketManager)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** has not the ticket manager role!");
            }
            else
            {
                await DeleteTicketManagerRoleAsync(dbContext, ticketManagerRole, ctx.Member.Id);
                await ctx.RespondAsync($"The ticket manager role has been removed from the **_'{dsRole.Name}'_** role!");
            }
        }
    }

    private async Task ResetJoinRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var joinRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (!joinRole.HasJoin)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** has not the join role!");
            }
            else
            {
                await DeleteJoinRolesAsync(dbContext, joinRole, ctx.Member.Id);
                await ctx.RespondAsync($"The join role has been removed from the **_'{dsRole.Name}'_** role!");
            }
        }
    }

    private async Task ResetVacationRoles(IEnumerable<string> roles, FreeGuardDbContext dbContext, CommandContext ctx, Guild guild)
    {
        foreach (var role in roles)
        {
            var roleId = ulong.Parse(role);
            var vacationRole = await Role.GetOrCreateRoleAsync(dbContext, roleId, ctx.Member.Id, guild.Id);
            var dsRole = ctx.Guild.GetRole(roleId);

            if (!vacationRole.HasVacation)
            {
                await ctx.RespondAsync($"The role **_'{dsRole.Name}'_** has not the vacation role!");
            }
            else
            {
                await DeleteVacationRoleAsync(dbContext, vacationRole, ctx.Member.Id);
                await ctx.RespondAsync($"The vacation role has been removed from the **_'{dsRole.Name}'_** role!");
            }
        }
    }

    private static async Task<DiscordEmbedBuilder> CreateRightEntryEmbedAsync(CommandContext ctx, FreeGuardDbContext dbContext, Guild guild, Botright right)
    {
        var embedBuilder = new DiscordEmbedBuilder()
                            .WithColor(new DiscordColor(255, 131, 0))
                            .WithAuthor(ctx.Client.CurrentUser.Username)
                            .WithTitle("Bot rights")
                            .WithDescription("Displays the bots current right system. Admins and guild owners always have access to all commands.")
                            .AddField($"{right.Id}. Right: {right.Name}", right.Description);

        var roleRights = await dbContext.Rolerights
            .Include("Role")
            .Where(x => x.BotRightId == right.Id)
            .Where(x => x.Role.GuildId == guild.Id)
            .ToListAsync();

        foreach (var roleRight in roleRights)
        {
            if (ctx.Guild.Roles.TryGetValue(roleRight.Role.DiscordRoleId, out DiscordRole discordRole))
            {
                embedBuilder.AddField("Role name", discordRole.Name);
            }
            else
            {
                embedBuilder.AddField("Role Id:", roleRight.Role.DiscordRoleId.ToString());
            }

            embedBuilder.AddField("Create date: ", roleRight.CreateDate.ToString(), true);

            if (ctx.Guild.Members.TryGetValue(roleRight.CreateUserId, out DiscordMember member))
            {
                embedBuilder.AddField("Create user: ", member.DisplayName, true);
            }
            else
            {
                embedBuilder.AddField("Create user: ", roleRight.CreateUserId.ToString(), true);
            }

            ctx.Client.Logger.LogDebug($"Role: {roleRight.Role.Id}");
        }

        return embedBuilder;
    }

    private static async Task UpdateWarningRoleAsync(FreeGuardDbContext dbContext, Role warningRole, ulong userId, bool isWarning)
    {
        warningRole.IsWarningRole = isWarning.ToString();
        warningRole.ModifyUserId = userId;
        warningRole.ModifyDate = DateTime.UtcNow;

        dbContext.Roles.Update(warningRole);
        await dbContext.SaveChangesAsync();
    }

    private static async Task UpdateTicketManagerRoleAsync(FreeGuardDbContext dbContext, Role ticketManagerRole, ulong userId, bool isTicketManager)
    {
        ticketManagerRole.IsTicketManager = isTicketManager;
        ticketManagerRole.ModifyUserId = userId;
        ticketManagerRole.ModifyDate = DateTime.UtcNow;

        dbContext.Roles.Update(ticketManagerRole);
        await dbContext.SaveChangesAsync();
    }

    private static async Task UpdateVacationRoleAsync(FreeGuardDbContext dbContext, Role vacationRole, ulong userId, bool isVacation)
    {
        vacationRole.IsVacationRole = isVacation.ToString();
        vacationRole.ModifyUserId = userId;
        vacationRole.ModifyDate = DateTime.UtcNow;

        dbContext.Roles.Update(vacationRole);
        await dbContext.SaveChangesAsync();
    }

    private static async Task UpdateJoinRoleAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId, bool isJoin)
    {
        joinRole.IsJoinRole = isJoin.ToString();
        joinRole.ModifyUserId = userId;
        joinRole.ModifyDate = DateTime.UtcNow;

        dbContext.Roles.Update(joinRole);
        await dbContext.SaveChangesAsync();
    }

    private static async Task SetJoinRoleAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId)
    {
        await UpdateJoinRoleAsync(dbContext, joinRole, userId, true);
    }

    private static async Task DeleteJoinRolesAsync(FreeGuardDbContext dbContext, Role joinRole, ulong userId)
    {
        await UpdateJoinRoleAsync(dbContext, joinRole, userId, false);
    }

    private static async Task SetWarningRoleAsync(FreeGuardDbContext dbContext, Role warningRole, ulong userId)
    {
        await UpdateWarningRoleAsync(dbContext, warningRole, userId, true);
    }

    private static async Task DeleteWarningRoleAsync(FreeGuardDbContext dbContext, Role warningRole, ulong userId)
    {
        await UpdateWarningRoleAsync(dbContext, warningRole, userId, false);
    }

    private static async Task SetTicketManagerRoleAsync(FreeGuardDbContext dbContext, Role warningRole, ulong userId)
    {
        await UpdateTicketManagerRoleAsync(dbContext, warningRole, userId, true);
    }

    private static async Task DeleteTicketManagerRoleAsync(FreeGuardDbContext dbContext, Role ticketManagerRole, ulong userId)
    {
        await UpdateTicketManagerRoleAsync(dbContext, ticketManagerRole, userId, false);
    }

    private static async Task SetVacationRoleAsync(FreeGuardDbContext dbContext, Role vacationRole, ulong userId)
    {
        await UpdateVacationRoleAsync(dbContext, vacationRole, userId, true);
    }

    private static async Task DeleteVacationRoleAsync(FreeGuardDbContext dbContext, Role vacationRole, ulong userId)
    {
        await UpdateVacationRoleAsync(dbContext, vacationRole, userId, false);
    }

    private static async Task UpdateRecruitmentChannelAsync(FreeGuardDbContext dbContext, Guild guild, ulong? channelId)
    {
        guild.RecruitmentChannelId = channelId;
        dbContext.Guilds.Update(guild);
        await dbContext.SaveChangesAsync();
    }

    private static async Task UpdateGreetingChannelAsync(FreeGuardDbContext dbContext, Guild guild, ulong? channelId)
    {
        guild.WelcomeMessageChannel = channelId;
        dbContext.Guilds.Update(guild);
        await dbContext.SaveChangesAsync();
    }

    private static async Task UpdateGreetingMessageAsync(FreeGuardDbContext dbContext, Guild guild, string message)
    {
        guild.WelcomeMessage = message;
        dbContext.Guilds.Update(guild);
        await dbContext.SaveChangesAsync();
    }
}
