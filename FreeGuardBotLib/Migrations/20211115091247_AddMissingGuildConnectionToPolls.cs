﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class AddMissingGuildConnectionToPolls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GuildId",
                table: "Polls",
                type: "int(11)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GuildId",
                table: "PollResults",
                type: "int(11)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Polls_GuildId",
                table: "Polls",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_PollResults_GuildId",
                table: "PollResults",
                column: "GuildId");

            migrationBuilder.AddForeignKey(
                name: "FK_PollResults_Guild_GuildId",
                table: "PollResults",
                column: "GuildId",
                principalTable: "Guild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Polls_Guild_GuildId",
                table: "Polls",
                column: "GuildId",
                principalTable: "Guild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PollResults_Guild_GuildId",
                table: "PollResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Polls_Guild_GuildId",
                table: "Polls");

            migrationBuilder.DropIndex(
                name: "IX_Polls_GuildId",
                table: "Polls");

            migrationBuilder.DropIndex(
                name: "IX_PollResults_GuildId",
                table: "PollResults");

            migrationBuilder.DropColumn(
                name: "GuildId",
                table: "Polls");

            migrationBuilder.DropColumn(
                name: "GuildId",
                table: "PollResults");
        }
    }
}
