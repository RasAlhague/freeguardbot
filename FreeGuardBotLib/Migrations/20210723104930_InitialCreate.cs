﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "BotRight",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Description = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotRight", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                  {1, "create_entry", "Right for creating new entries in the blacklist." },
                  {2, "update_entry", "Right for updating entries in the blacklist."},
                  {3, "delete_entry", "Right for deleting entries in the blacklist."},
                  {4, "export_list", "Right for exporting all entries into a csv file."},
                  {5, "search_entries", "Right for searching the blacklist for entries."},
                  {6, "set_rights", "Right for setting bot rights."},
                  {7, "del_rights", "Right for deleting bot rights."},
                  {8, "manage_entries", "Groupright for right 1,2,3 and 5. Meant for simple setup of a entry manager role."},
                  {9, "manage_bot", "Groupright for right 1,2,3,4,5,6,7,11. Meant for a simple setup of a bot admin role."},
                  {10, "use_list", "Groupright for right 1, 5. Meant for a simple setup of a simple user role."},
                  {11 ,"view_rights", "Right for viewing the existing rights."},
                  {12, "create_recruit", "Right for creating new recruits."}
                });


            migrationBuilder.CreateTable(
                name: "DiscordUser",
                columns: table => new
                {
                    DiscordUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordUser", x => x.DiscordUserId);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "Guild",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GuildId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    InitialName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    RecruitmentChannelId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guild", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "BlacklistEntry",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SteamId = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PlayfabId = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Reason = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlacklistEntry", x => x.Id);
                    table.ForeignKey(
                        name: "blacklistentry_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "blacklistentry_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "blacklistentry_ibfk_3",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "CommandInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    CommandName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Paramether = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommandInfo", x => x.Id);
                    table.ForeignKey(
                        name: "commandinfo_ibfk_1",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "GuildUser",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    DiscordUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuildUser", x => x.Id);
                    table.ForeignKey(
                        name: "guilduser_ibfk_1",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "guilduser_ibfk_2",
                        column: x => x.DiscordUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "Recruit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RecruitId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true),
                    RecruiterId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DiscordName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SteamId = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Joined = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recruit", x => x.Id);
                    table.ForeignKey(
                        name: "recruit_ibfk_1",
                        column: x => x.RecruitId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "recruit_ibfk_2",
                        column: x => x.RecruiterId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "recruit_ibfk_3",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "recruit_ibfk_4",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "recruit_ibfk_5",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DiscordRoleId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    IsJoinRole = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: false, defaultValueSql: "'false'", collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsWarningRole = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: false, defaultValueSql: "'false'", collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                    table.ForeignKey(
                        name: "role_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "role_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "role_ibfk_3",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "Alias",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    BlacklistEntryId = table.Column<int>(type: "int(11)", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alias", x => x.Id);
                    table.ForeignKey(
                        name: "alias_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "alias_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "alias_ibfk_3",
                        column: x => x.BlacklistEntryId,
                        principalTable: "BlacklistEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "Confirmation",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ConfirmUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    BlacklistEntryId = table.Column<int>(type: "int(11)", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Confirmation", x => x.Id);
                    table.ForeignKey(
                        name: "confirmation_ibfk_1",
                        column: x => x.ConfirmUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "confirmation_ibfk_2",
                        column: x => x.BlacklistEntryId,
                        principalTable: "BlacklistEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "confirmation_ibfk_3",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "confirmation_ibfk_4",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "RoleRight",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(type: "int(11)", nullable: false),
                    BotRightId = table.Column<int>(type: "int(11)", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleRight", x => x.Id);
                    table.ForeignKey(
                        name: "roleright_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "roleright_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "roleright_ibfk_3",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "roleright_ibfk_4",
                        column: x => x.BotRightId,
                        principalTable: "BotRight",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "Vacation",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AffectedId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    FreezeTitleId = table.Column<int>(type: "int(11)", nullable: false),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    EstimatedDuration = table.Column<int>(type: "int(11)", nullable: true),
                    Reason = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Finished = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacation", x => x.Id);
                    table.ForeignKey(
                        name: "vacation_ibfk_1",
                        column: x => x.AffectedId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vacation_ibfk_2",
                        column: x => x.FreezeTitleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vacation_ibfk_3",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vacation_ibfk_4",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vacation_ibfk_5",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateIndex(
                name: "BlacklistEntryId",
                table: "Alias",
                column: "BlacklistEntryId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId",
                table: "Alias",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId",
                table: "Alias",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId1",
                table: "BlacklistEntry",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "GuildId",
                table: "BlacklistEntry",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId1",
                table: "BlacklistEntry",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "GuildId1",
                table: "CommandInfo",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "BlacklistEntryId1",
                table: "Confirmation",
                column: "BlacklistEntryId");

            migrationBuilder.CreateIndex(
                name: "ConfirmUserId",
                table: "Confirmation",
                column: "ConfirmUserId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId2",
                table: "Confirmation",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId2",
                table: "Confirmation",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "DiscordUserId",
                table: "GuildUser",
                column: "DiscordUserId");

            migrationBuilder.CreateIndex(
                name: "GuildId2",
                table: "GuildUser",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId3",
                table: "Recruit",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "GuildId3",
                table: "Recruit",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId3",
                table: "Recruit",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "RecruiterId",
                table: "Recruit",
                column: "RecruiterId");

            migrationBuilder.CreateIndex(
                name: "RecruitId",
                table: "Recruit",
                column: "RecruitId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId4",
                table: "Role",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "GuildId4",
                table: "Role",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId4",
                table: "Role",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "BotRightId",
                table: "RoleRight",
                column: "BotRightId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId5",
                table: "RoleRight",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId5",
                table: "RoleRight",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "RoleId",
                table: "RoleRight",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "AffectedId",
                table: "Vacation",
                column: "AffectedId");

            migrationBuilder.CreateIndex(
                name: "CreateUserId6",
                table: "Vacation",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "FreezeTitleId",
                table: "Vacation",
                column: "FreezeTitleId");

            migrationBuilder.CreateIndex(
                name: "GuildId5",
                table: "Vacation",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "ModifyUserId6",
                table: "Vacation",
                column: "ModifyUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alias");

            migrationBuilder.DropTable(
                name: "CommandInfo");

            migrationBuilder.DropTable(
                name: "Confirmation");

            migrationBuilder.DropTable(
                name: "GuildUser");

            migrationBuilder.DropTable(
                name: "Recruit");

            migrationBuilder.DropTable(
                name: "RoleRight");

            migrationBuilder.DropTable(
                name: "Vacation");

            migrationBuilder.DropTable(
                name: "BlacklistEntry");

            migrationBuilder.DropTable(
                name: "BotRight");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "DiscordUser");

            migrationBuilder.DropTable(
                name: "Guild");
        }
    }
}
