﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class FixReactionEmoji : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Emojis_Emojis_EmojiId1",
                table: "Emojis");

            migrationBuilder.DropIndex(
                name: "IX_Emojis_EmojiId1",
                table: "Emojis");

            migrationBuilder.DropColumn(
                name: "EmojiId1",
                table: "Emojis");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EmojiId1",
                table: "Emojis",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Emojis_EmojiId1",
                table: "Emojis",
                column: "EmojiId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Emojis_Emojis_EmojiId1",
                table: "Emojis",
                column: "EmojiId1",
                principalTable: "Emojis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
