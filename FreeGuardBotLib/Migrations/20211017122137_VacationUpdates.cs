﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class VacationUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SteamId",
                table: "DiscordUser",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_unicode_ci")
                .Annotation("MySql:CharSet", "utf8mb4");
            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                  {25, "use_vacation", "Right for normal vacation commands."}
                });
            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                  {26, "manage_vacation", "Right to use all vacation commands."}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SteamId",
                table: "DiscordUser");
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 25
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 26
            );
        }
    }
}
