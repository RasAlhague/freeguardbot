﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class ChangeLeaderboardFieldName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leaderboard_Guild_GuildId",
                table: "Leaderboard");

            migrationBuilder.DropForeignKey(
                name: "FK_LeaderboardAction_Guild_GuildId",
                table: "LeaderboardAction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LeaderboardAction",
                table: "LeaderboardAction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Leaderboard",
                table: "Leaderboard");

            migrationBuilder.RenameTable(
                name: "LeaderboardAction",
                newName: "LeaderboardActions");

            migrationBuilder.RenameTable(
                name: "Leaderboard",
                newName: "Leaderboards");

            migrationBuilder.RenameColumn(
                name: "CreateTime",
                table: "LeaderboardActions",
                newName: "CreateDate");

            migrationBuilder.RenameIndex(
                name: "IX_LeaderboardAction_GuildId",
                table: "LeaderboardActions",
                newName: "IX_LeaderboardActions_GuildId");

            migrationBuilder.RenameIndex(
                name: "IX_LeaderboardAction_CreateUserId",
                table: "LeaderboardActions",
                newName: "IX_LeaderboardActions_CreateUserId");

            migrationBuilder.RenameIndex(
                name: "IX_LeaderboardAction_AffectedUserId",
                table: "LeaderboardActions",
                newName: "IX_LeaderboardActions_AffectedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Leaderboard_ModifyUserId",
                table: "Leaderboards",
                newName: "IX_Leaderboards_ModifyUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Leaderboard_GuildId",
                table: "Leaderboards",
                newName: "IX_Leaderboards_GuildId");

            migrationBuilder.RenameIndex(
                name: "IX_Leaderboard_CreateUserId",
                table: "Leaderboards",
                newName: "IX_Leaderboards_CreateUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LeaderboardActions",
                table: "LeaderboardActions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Leaderboards",
                table: "Leaderboards",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaderboardActions_Guild_GuildId",
                table: "LeaderboardActions",
                column: "GuildId",
                principalTable: "Guild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Leaderboards_Guild_GuildId",
                table: "Leaderboards",
                column: "GuildId",
                principalTable: "Guild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaderboardActions_Guild_GuildId",
                table: "LeaderboardActions");

            migrationBuilder.DropForeignKey(
                name: "FK_Leaderboards_Guild_GuildId",
                table: "Leaderboards");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Leaderboards",
                table: "Leaderboards");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LeaderboardActions",
                table: "LeaderboardActions");

            migrationBuilder.RenameTable(
                name: "Leaderboards",
                newName: "Leaderboard");

            migrationBuilder.RenameTable(
                name: "LeaderboardActions",
                newName: "LeaderboardAction");

            migrationBuilder.RenameIndex(
                name: "IX_Leaderboards_ModifyUserId",
                table: "Leaderboard",
                newName: "IX_Leaderboard_ModifyUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Leaderboards_GuildId",
                table: "Leaderboard",
                newName: "IX_Leaderboard_GuildId");

            migrationBuilder.RenameIndex(
                name: "IX_Leaderboards_CreateUserId",
                table: "Leaderboard",
                newName: "IX_Leaderboard_CreateUserId");

            migrationBuilder.RenameColumn(
                name: "CreateDate",
                table: "LeaderboardAction",
                newName: "CreateTime");

            migrationBuilder.RenameIndex(
                name: "IX_LeaderboardActions_GuildId",
                table: "LeaderboardAction",
                newName: "IX_LeaderboardAction_GuildId");

            migrationBuilder.RenameIndex(
                name: "IX_LeaderboardActions_CreateUserId",
                table: "LeaderboardAction",
                newName: "IX_LeaderboardAction_CreateUserId");

            migrationBuilder.RenameIndex(
                name: "IX_LeaderboardActions_AffectedUserId",
                table: "LeaderboardAction",
                newName: "IX_LeaderboardAction_AffectedUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Leaderboard",
                table: "Leaderboard",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LeaderboardAction",
                table: "LeaderboardAction",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Leaderboard_Guild_GuildId",
                table: "Leaderboard",
                column: "GuildId",
                principalTable: "Guild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LeaderboardAction_Guild_GuildId",
                table: "LeaderboardAction",
                column: "GuildId",
                principalTable: "Guild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
