﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class FixMissingNullableModifiy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "poll_ibfk_2",
                table: "Polls");

            migrationBuilder.AlterColumn<ulong>(
                name: "ModifyUserId",
                table: "Polls",
                type: "bigint(20) unsigned",
                nullable: true,
                oldClrType: typeof(ulong),
                oldType: "bigint(20) unsigned");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifyDate",
                table: "Polls",
                type: "datetime(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AddForeignKey(
                name: "poll_ibfk_2",
                table: "Polls",
                column: "ModifyUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "poll_ibfk_2",
                table: "Polls");

            migrationBuilder.AlterColumn<ulong>(
                name: "ModifyUserId",
                table: "Polls",
                type: "bigint(20) unsigned",
                nullable: false,
                defaultValue: 0ul,
                oldClrType: typeof(ulong),
                oldType: "bigint(20) unsigned",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifyDate",
                table: "Polls",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "poll_ibfk_2",
                table: "Polls",
                column: "ModifyUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
