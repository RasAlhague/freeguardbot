﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class NullableModifyDates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "roleselect_ibfk_2",
                table: "RoleSelect");

            migrationBuilder.DropForeignKey(
                name: "roleselectreaction_ibfk_2",
                table: "RoleSelectReaction");

            migrationBuilder.AlterColumn<ulong>(
                name: "ModifyUserId",
                table: "RoleSelectReaction",
                type: "bigint(20) unsigned",
                nullable: true,
                oldClrType: typeof(ulong),
                oldType: "bigint(20) unsigned");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifyDate",
                table: "RoleSelectReaction",
                type: "datetime(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AlterColumn<ulong>(
                name: "ModifyUserId",
                table: "RoleSelect",
                type: "bigint(20) unsigned",
                nullable: true,
                oldClrType: typeof(ulong),
                oldType: "bigint(20) unsigned");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifyDate",
                table: "RoleSelect",
                type: "datetime(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AddForeignKey(
                name: "roleselect_ibfk_2",
                table: "RoleSelect",
                column: "ModifyUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "roleselectreaction_ibfk_2",
                table: "RoleSelectReaction",
                column: "ModifyUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "roleselect_ibfk_2",
                table: "RoleSelect");

            migrationBuilder.DropForeignKey(
                name: "roleselectreaction_ibfk_2",
                table: "RoleSelectReaction");

            migrationBuilder.AlterColumn<ulong>(
                name: "ModifyUserId",
                table: "RoleSelectReaction",
                type: "bigint(20) unsigned",
                nullable: false,
                defaultValue: 0ul,
                oldClrType: typeof(ulong),
                oldType: "bigint(20) unsigned",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifyDate",
                table: "RoleSelectReaction",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldNullable: true);

            migrationBuilder.AlterColumn<ulong>(
                name: "ModifyUserId",
                table: "RoleSelect",
                type: "bigint(20) unsigned",
                nullable: false,
                defaultValue: 0ul,
                oldClrType: typeof(ulong),
                oldType: "bigint(20) unsigned",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifyDate",
                table: "RoleSelect",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "roleselect_ibfk_2",
                table: "RoleSelect",
                column: "ModifyUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "roleselectreaction_ibfk_2",
                table: "RoleSelectReaction",
                column: "ModifyUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
