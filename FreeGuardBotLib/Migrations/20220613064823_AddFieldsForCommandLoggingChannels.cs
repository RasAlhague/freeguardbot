﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class AddFieldsForCommandLoggingChannels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "GeneralCommandLoggingChannel",
                table: "Guild",
                type: "bigint unsigned",
                nullable: true);

            migrationBuilder.AddColumn<ulong>(
                name: "LeaderboardLoggingChannel",
                table: "Guild",
                type: "bigint unsigned",
                nullable: true);

            migrationBuilder.AddColumn<ulong>(
                name: "TimeoutLoggingChannel",
                table: "Guild",
                type: "bigint unsigned",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeneralCommandLoggingChannel",
                table: "Guild");

            migrationBuilder.DropColumn(
                name: "LeaderboardLoggingChannel",
                table: "Guild");

            migrationBuilder.DropColumn(
                name: "TimeoutLoggingChannel",
                table: "Guild");
        }
    }
}
