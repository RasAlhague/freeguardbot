﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class AddLeaderboard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Leaderboard",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    DiscordChannelId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    FilterSql = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: true),
                    ModifyDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leaderboard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leaderboard_Guild_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "leaderboard_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "leaderboard_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId");
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "LeaderboardAction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AffectedUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    Reason = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Points = table.Column<long>(type: "bigint", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaderboardAction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeaderboardAction_Guild_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "leaderboardaction_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "leaderboardaction_ibfk_2",
                        column: x => x.AffectedUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateIndex(
                name: "IX_Leaderboard_CreateUserId",
                table: "Leaderboard",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Leaderboard_GuildId",
                table: "Leaderboard",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_Leaderboard_ModifyUserId",
                table: "Leaderboard",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaderboardAction_AffectedUserId",
                table: "LeaderboardAction",
                column: "AffectedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaderboardAction_CreateUserId",
                table: "LeaderboardAction",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaderboardAction_GuildId",
                table: "LeaderboardAction",
                column: "GuildId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Leaderboard");

            migrationBuilder.DropTable(
                name: "LeaderboardAction");
        }
    }
}
