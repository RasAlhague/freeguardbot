﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class AddRightForTimeout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                    {29, "use_timeout", "Rights that allow a role to use timeouts."}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 29
            );
        }
    }
}
