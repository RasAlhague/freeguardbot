﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class ResizeLogSize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Paramether",
                table: "CommandInfo",
                type: "varchar(2048)",
                maxLength: 2048,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.RenameColumn(
                name: "Paramether",
                table: "CommandInfo",
                newName: "Parameter");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Parameter",
                table: "CommandInfo",
                newName: "Paramether");

            migrationBuilder.Sql(@"UPDATE CommandInfo SET Paramether = LEFT(Paramether, 255) WHERE LENGTH(Paramether) > 255");

            migrationBuilder.AlterColumn<string>(
                name: "Paramether",
                table: "CommandInfo",
                type: "varchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(2048)",
                oldMaxLength: 2048);
        }
    }
}
