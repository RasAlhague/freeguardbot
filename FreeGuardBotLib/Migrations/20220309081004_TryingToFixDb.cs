﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class TryingToFixDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoleSelectReaction_Emojis_EmojiId",
                table: "RoleSelectReaction");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleSelectReaction_Role_RoleId",
                table: "RoleSelectReaction");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleSelectReaction_RoleSelect_RoleSelectId",
                table: "RoleSelectReaction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleSelectReaction",
                table: "RoleSelectReaction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleSelect",
                table: "RoleSelect");

            migrationBuilder.RenameTable(
                name: "RoleSelectReaction",
                newName: "RoleSelectReactions");

            migrationBuilder.RenameTable(
                name: "RoleSelect",
                newName: "RoleSelects");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReaction_RoleSelectId",
                table: "RoleSelectReactions",
                newName: "IX_RoleSelectReactions_RoleSelectId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReaction_RoleId",
                table: "RoleSelectReactions",
                newName: "IX_RoleSelectReactions_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReaction_ModifyUserId",
                table: "RoleSelectReactions",
                newName: "IX_RoleSelectReactions_ModifyUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReaction_EmojiId",
                table: "RoleSelectReactions",
                newName: "IX_RoleSelectReactions_EmojiId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReaction_CreateUserId",
                table: "RoleSelectReactions",
                newName: "IX_RoleSelectReactions_CreateUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelect_ModifyUserId",
                table: "RoleSelects",
                newName: "IX_RoleSelects_ModifyUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelect_CreateUserId",
                table: "RoleSelects",
                newName: "IX_RoleSelects_CreateUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleSelectReactions",
                table: "RoleSelectReactions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleSelects",
                table: "RoleSelects",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RoleSelectReactions_Emojis_EmojiId",
                table: "RoleSelectReactions",
                column: "EmojiId",
                principalTable: "Emojis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleSelectReactions_Role_RoleId",
                table: "RoleSelectReactions",
                column: "RoleId",
                principalTable: "Role",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleSelectReactions_RoleSelects_RoleSelectId",
                table: "RoleSelectReactions",
                column: "RoleSelectId",
                principalTable: "RoleSelects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoleSelectReactions_Emojis_EmojiId",
                table: "RoleSelectReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleSelectReactions_Role_RoleId",
                table: "RoleSelectReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleSelectReactions_RoleSelects_RoleSelectId",
                table: "RoleSelectReactions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleSelects",
                table: "RoleSelects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleSelectReactions",
                table: "RoleSelectReactions");

            migrationBuilder.RenameTable(
                name: "RoleSelects",
                newName: "RoleSelect");

            migrationBuilder.RenameTable(
                name: "RoleSelectReactions",
                newName: "RoleSelectReaction");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelects_ModifyUserId",
                table: "RoleSelect",
                newName: "IX_RoleSelect_ModifyUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelects_CreateUserId",
                table: "RoleSelect",
                newName: "IX_RoleSelect_CreateUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReactions_RoleSelectId",
                table: "RoleSelectReaction",
                newName: "IX_RoleSelectReaction_RoleSelectId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReactions_RoleId",
                table: "RoleSelectReaction",
                newName: "IX_RoleSelectReaction_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReactions_ModifyUserId",
                table: "RoleSelectReaction",
                newName: "IX_RoleSelectReaction_ModifyUserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReactions_EmojiId",
                table: "RoleSelectReaction",
                newName: "IX_RoleSelectReaction_EmojiId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleSelectReactions_CreateUserId",
                table: "RoleSelectReaction",
                newName: "IX_RoleSelectReaction_CreateUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleSelect",
                table: "RoleSelect",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleSelectReaction",
                table: "RoleSelectReaction",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RoleSelectReaction_Emojis_EmojiId",
                table: "RoleSelectReaction",
                column: "EmojiId",
                principalTable: "Emojis",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleSelectReaction_Role_RoleId",
                table: "RoleSelectReaction",
                column: "RoleId",
                principalTable: "Role",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleSelectReaction_RoleSelect_RoleSelectId",
                table: "RoleSelectReaction",
                column: "RoleSelectId",
                principalTable: "RoleSelect",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
