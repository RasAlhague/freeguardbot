﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class AddMaxMonthsInPast : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxMonthsInPast",
                table: "Leaderboards",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxMonthsInPast",
                table: "Leaderboards");
        }
    }
}
