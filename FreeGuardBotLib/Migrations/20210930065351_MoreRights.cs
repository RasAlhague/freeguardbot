﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class MoreRights : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                  {24, "use_activity", "Right for all activity related commands."}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 24
            );
        }
    }
}
