﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class ChangeLeaderboard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilterSql",
                table: "Leaderboards");

            migrationBuilder.AddColumn<int>(
                name: "Limit",
                table: "Leaderboards",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "MaxAge",
                table: "Leaderboards",
                type: "time(6)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrderDirection",
                table: "Leaderboards",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrderField",
                table: "Leaderboards",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Limit",
                table: "Leaderboards");

            migrationBuilder.DropColumn(
                name: "MaxAge",
                table: "Leaderboards");

            migrationBuilder.DropColumn(
                name: "OrderDirection",
                table: "Leaderboards");

            migrationBuilder.DropColumn(
                name: "OrderField",
                table: "Leaderboards");

            migrationBuilder.AddColumn<string>(
                name: "FilterSql",
                table: "Leaderboards",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_unicode_ci")
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
