﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class TriviaFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TriviaAnswer_TriviaQuestion_QuestionId",
                table: "TriviaAnswer");

            migrationBuilder.DropForeignKey(
                name: "FK_TriviaQuestion_DifficultyLevel_DifficultyLevelId",
                table: "TriviaQuestion");

            migrationBuilder.DropForeignKey(
                name: "FK_TriviaQuestion_DiscordUser_CreateUserId",
                table: "TriviaQuestion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TriviaQuestion",
                table: "TriviaQuestion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TriviaAnswer",
                table: "TriviaAnswer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DifficultyLevel",
                table: "DifficultyLevel");

            migrationBuilder.RenameTable(
                name: "TriviaQuestion",
                newName: "TriviaQuestions");

            migrationBuilder.RenameTable(
                name: "TriviaAnswer",
                newName: "TriviaAnswers");

            migrationBuilder.RenameTable(
                name: "DifficultyLevel",
                newName: "DifficultyLevels");

            migrationBuilder.RenameIndex(
                name: "IX_TriviaQuestion_DifficultyLevelId",
                table: "TriviaQuestions",
                newName: "IX_TriviaQuestions_DifficultyLevelId");

            migrationBuilder.RenameIndex(
                name: "IX_TriviaQuestion_CreateUserId",
                table: "TriviaQuestions",
                newName: "IX_TriviaQuestions_CreateUserId");

            migrationBuilder.RenameIndex(
                name: "IX_TriviaAnswer_QuestionId",
                table: "TriviaAnswers",
                newName: "IX_TriviaAnswers_QuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TriviaQuestions",
                table: "TriviaQuestions",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TriviaAnswers",
                table: "TriviaAnswers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DifficultyLevels",
                table: "DifficultyLevels",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaAnswers_TriviaQuestions_QuestionId",
                table: "TriviaAnswers",
                column: "QuestionId",
                principalTable: "TriviaQuestions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaQuestions_DifficultyLevels_DifficultyLevelId",
                table: "TriviaQuestions",
                column: "DifficultyLevelId",
                principalTable: "DifficultyLevels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaQuestions_DiscordUser_CreateUserId",
                table: "TriviaQuestions",
                column: "CreateUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TriviaAnswers_TriviaQuestions_QuestionId",
                table: "TriviaAnswers");

            migrationBuilder.DropForeignKey(
                name: "FK_TriviaQuestions_DifficultyLevels_DifficultyLevelId",
                table: "TriviaQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_TriviaQuestions_DiscordUser_CreateUserId",
                table: "TriviaQuestions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TriviaQuestions",
                table: "TriviaQuestions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TriviaAnswers",
                table: "TriviaAnswers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DifficultyLevels",
                table: "DifficultyLevels");

            migrationBuilder.RenameTable(
                name: "TriviaQuestions",
                newName: "TriviaQuestion");

            migrationBuilder.RenameTable(
                name: "TriviaAnswers",
                newName: "TriviaAnswer");

            migrationBuilder.RenameTable(
                name: "DifficultyLevels",
                newName: "DifficultyLevel");

            migrationBuilder.RenameIndex(
                name: "IX_TriviaQuestions_DifficultyLevelId",
                table: "TriviaQuestion",
                newName: "IX_TriviaQuestion_DifficultyLevelId");

            migrationBuilder.RenameIndex(
                name: "IX_TriviaQuestions_CreateUserId",
                table: "TriviaQuestion",
                newName: "IX_TriviaQuestion_CreateUserId");

            migrationBuilder.RenameIndex(
                name: "IX_TriviaAnswers_QuestionId",
                table: "TriviaAnswer",
                newName: "IX_TriviaAnswer_QuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TriviaQuestion",
                table: "TriviaQuestion",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TriviaAnswer",
                table: "TriviaAnswer",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DifficultyLevel",
                table: "DifficultyLevel",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaAnswer_TriviaQuestion_QuestionId",
                table: "TriviaAnswer",
                column: "QuestionId",
                principalTable: "TriviaQuestion",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaQuestion_DifficultyLevel_DifficultyLevelId",
                table: "TriviaQuestion",
                column: "DifficultyLevelId",
                principalTable: "DifficultyLevel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TriviaQuestion_DiscordUser_CreateUserId",
                table: "TriviaQuestion",
                column: "CreateUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
