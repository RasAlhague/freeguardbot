﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations;

public partial class AddRightForManageLeaderboard : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.InsertData(
            table: "BotRight",
            columns: new[] { "Id", "Name", "Description" },
            values: new object[,] {
              {28, "manage_leaderboard", "Right for commands related to granting and revoking points."}
            });
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DeleteData(
            table: "BotRight",
            keyColumn: "Id",
            keyValue: 28
        );
    }
}
