﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class AddMissingRights : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                  {13, "update_recruit", "Right for updating recruits."},
                  {14, "delete_recruit", "Right for deleting recruits."},
                  {15, "search_recruit", "Right for searching for recruits."},
                  {16, "set_join_roles", "Right for setting the join roles of new recruits."},
                  {17, "del_join_roles", "Right for deleting the join roles of new recruits."},
                  {18, "set_warn_role", "Right for setting the role which is used as a warn role."},
                  {19, "del_warn_role", "Right for deleting the role which is used as warn role."},
                  {20, "set_recruit_channel", "Right for setting the recruitment message channel."},
                  {21, "del_recruit_channel", "Right for deleting the set recruitment message channel."},
                  {22, "manage_recruits", "Group right for managing recruits. Groups the rights 12, 13, 14 and 15."},
                  {23, "use_recruits", "Group right for using recruitment. Groups the rights 12 and 15."}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 13
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 14
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 15
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 16
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 17
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 18
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 19
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 20
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 21
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 22
            );
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 23
            );
        }
    }
}
