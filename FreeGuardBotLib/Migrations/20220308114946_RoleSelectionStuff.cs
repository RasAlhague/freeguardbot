﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class RoleSelectionStuff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RoleSelect",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    MessageId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    ChannelId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    GuildId = table.Column<int>(type: "int", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleSelect", x => x.Id);
                    table.ForeignKey(
                        name: "roleselect_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "roleselect_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateTable(
                name: "RoleSelectReaction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Message = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_unicode_ci")
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EmojiId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int(11)", nullable: false),
                    RoleSelectId = table.Column<int>(type: "int", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreateUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    ModifyDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    ModifyUserId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleSelectReaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleSelectReaction_Emojis_EmojiId",
                        column: x => x.EmojiId,
                        principalTable: "Emojis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleSelectReaction_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleSelectReaction_RoleSelect_RoleSelectId",
                        column: x => x.RoleSelectId,
                        principalTable: "RoleSelect",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "roleselectreaction_ibfk_1",
                        column: x => x.CreateUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "roleselectreaction_ibfk_2",
                        column: x => x.ModifyUserId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelect_CreateUserId",
                table: "RoleSelect",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelect_ModifyUserId",
                table: "RoleSelect",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelectReaction_CreateUserId",
                table: "RoleSelectReaction",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelectReaction_EmojiId",
                table: "RoleSelectReaction",
                column: "EmojiId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelectReaction_ModifyUserId",
                table: "RoleSelectReaction",
                column: "ModifyUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelectReaction_RoleId",
                table: "RoleSelectReaction",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleSelectReaction_RoleSelectId",
                table: "RoleSelectReaction",
                column: "RoleSelectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RoleSelectReaction");

            migrationBuilder.DropTable(
                name: "RoleSelect");
        }
    }
}
