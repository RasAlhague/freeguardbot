﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class AddMessageActivityLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MessageActivityLogEntries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    WriterId = table.Column<ulong>(type: "bigint(20) unsigned", nullable: false),
                    GuildId = table.Column<int>(type: "int(11)", nullable: false),
                    WriteDate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageActivityLogEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MessageActivityLogEntries_DiscordUser_WriterId",
                        column: x => x.WriterId,
                        principalTable: "DiscordUser",
                        principalColumn: "DiscordUserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MessageActivityLogEntries_Guild_GuildId",
                        column: x => x.GuildId,
                        principalTable: "Guild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4")
                .Annotation("Relational:Collation", "utf8mb4_unicode_ci");

            migrationBuilder.CreateIndex(
                name: "IX_MessageActivityLogEntries_GuildId",
                table: "MessageActivityLogEntries",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_MessageActivityLogEntries_WriterId",
                table: "MessageActivityLogEntries",
                column: "WriterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessageActivityLogEntries");
        }
    }
}
