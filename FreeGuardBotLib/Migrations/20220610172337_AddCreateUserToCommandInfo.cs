﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FreeGuardBotLib.Migrations
{
    public partial class AddCreateUserToCommandInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "CreateUserId",
                table: "CommandInfo",
                type: "bigint(20) unsigned",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CommandInfo_CreateUserId",
                table: "CommandInfo",
                column: "CreateUserId");

            migrationBuilder.AddForeignKey(
                name: "commandinfo_ibfk_2",
                table: "CommandInfo",
                column: "CreateUserId",
                principalTable: "DiscordUser",
                principalColumn: "DiscordUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "commandinfo_ibfk_2",
                table: "CommandInfo");

            migrationBuilder.DropIndex(
                name: "IX_CommandInfo_CreateUserId",
                table: "CommandInfo");

            migrationBuilder.DropColumn(
                name: "CreateUserId",
                table: "CommandInfo");
        }
    }
}
