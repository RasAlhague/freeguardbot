﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreeGuardBotLib.Migrations
{
    public partial class NewRightForUpload : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BotRight",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,] {
                  {27, "upload_image", "Right for uploading lls images to the bot."}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BotRight",
                keyColumn: "Id",
                keyValue: 27
            );
        }
    }
}
