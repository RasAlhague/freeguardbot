﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib
{
    public class BotConfig
    {
        public string BotToken { get; set; }
        public string DbConnString { get; set; }
        public string GitlabToken { get; set; }
        public string GitlabUsername { get; set; }
        public string GitlabUserPassword { get; set; }
        public short LavalinkPort { get; set; }
        public string LavalinkAddress { get; set; }
        public string LavalinkPasswort { get; set; }
        public string TextGenApiPath { get; set; }
        public LogLevel LogLevel { get; set; }

        private static BotConfig _instance;

        protected BotConfig()
        {

        }

        public static BotConfig GetInstance()
        {
            if (_instance is null)
            {
                string botToken = Environment.GetEnvironmentVariable("fg_bot_token");
                string dbConnString = Environment.GetEnvironmentVariable("fg_conn_string");
                string gitlabToken = Environment.GetEnvironmentVariable("GITLAB_TOKEN");
                string lavalinkAddress = Environment.GetEnvironmentVariable("LAVALINK_ADDRESS");
                short lavalinkPort = short.Parse(Environment.GetEnvironmentVariable("LAVALINK_PORT"));
                string lavalinkPasswort = Environment.GetEnvironmentVariable("LAVALINK_PASSWORT");
                string textGenApiPath = Environment.GetEnvironmentVariable("TEXT_GEN_API_PATH");
                LogLevel logLevel = Enum.Parse<LogLevel>(Environment.GetEnvironmentVariable("LOG_LEVEL"));

                _instance = new BotConfig();

                if (!string.IsNullOrEmpty(botToken) && !string.IsNullOrEmpty(dbConnString))
                {
                    _instance.BotToken = botToken;
                    _instance.DbConnString = dbConnString;
                    _instance.GitlabToken = gitlabToken;
                    _instance.LogLevel = logLevel;
                    _instance.LavalinkAddress = lavalinkAddress;
                    _instance.LavalinkPasswort = lavalinkPasswort;
                    _instance.LavalinkPort = lavalinkPort;
                    _instance.TextGenApiPath = textGenApiPath;
                }
                else
                {
                    throw new ArgumentException("Coult not find environment var for bot token and/or conn string!");
                }
            }

            return _instance;
        }
    }
}
