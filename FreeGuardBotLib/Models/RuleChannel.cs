﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models;

public class RuleChannel
{
    public RuleChannel()
    {
        Categories = new HashSet<RuleCategory>();
    }

    [Key]
    [Required]
    public int Id { get; set; }
    [Required]
    public ulong ChannelId { get; set; }
    [Required, MaxLength(100)]
    public string Name { get; set; }
    public int GuildId { get; set; }
    [Required]
    public ulong CreateUserId { get; set; }
    [Required]
    public DateTime CreateDate { get; set; }
    public ulong? ModifyUserId { get; set; }
    public DateTime? ModifyDate { get; set; }

    public virtual Guild Guild { get; set; }
    public virtual Discorduser CreateUser { get; set; }
    public virtual Discorduser ModifyUser { get; set; }

    public virtual ICollection<RuleCategory> Categories { get; set; }
}