﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class RoleSelect
    {
        public RoleSelect()
        {
            RoleSelectReactions = new HashSet<RoleSelectReaction>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public ulong MessageId { get; set; }
        public ulong ChannelId { get; set; }
        public int GuildId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual ICollection<RoleSelectReaction> RoleSelectReactions { get; set; }
    }
}
