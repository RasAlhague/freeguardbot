﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Ticket
    {
        public static async Task<Ticket> CreateTicket(FreeGuardDbContext dbcontext, DiscordGuild guild, DiscordUser user)
        {
            var dbGuild = await Guild.GetAsync(dbcontext, guild.Id);
            var dbUser = await Discorduser.GetOrCreateDiscorduserAsync(dbcontext, user.Id);
            await Guilduser.GetOrCreateGuildUserAsync(dbcontext, dbGuild.Id, dbUser.DiscordUserId);

            Ticket ticket = new Ticket()
            {
                CreateDate = DateTime.UtcNow,
                CreateUserId = user.Id,
                GuildId = dbGuild.Id,
                Messages = 0,
                IsArchived = false,
            };
            await dbcontext.Tickets.AddAsync(ticket);
            await dbcontext.SaveChangesAsync();

            return ticket;
        }
    }
}
