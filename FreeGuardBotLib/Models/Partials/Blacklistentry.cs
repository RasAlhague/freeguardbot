﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Blacklistentry
    {
        public async static Task<Blacklistentry> GetAsync(FreeGuardDbContext dbContext, string steamId, int guildId)
        {
            return await dbContext.Blacklistentries
                .SingleOrDefaultAsync(x => x.SteamId == steamId && x.GuildId == guildId);
        }
    }
}
