﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class ErrorLog
    {
        public static async Task Create(FreeGuardDbContext db, int guildId, string originCommand, Exception ex)
        {
            var log = new ErrorLog()
            {
                CreateDate = DateTime.UtcNow,
                Message = ex.Message,
                GuildId = guildId,
                OriginCommand =  originCommand,
                Source = ex.Source,
                StackTrace = ex.StackTrace
            };

            await db.ErrorLogs.AddAsync(log);
            await db.SaveChangesAsync();
        }
    }
}
