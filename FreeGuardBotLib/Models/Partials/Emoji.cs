﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Emoji
    {
        public static async Task<Emoji> GetOrCreate(FreeGuardDbContext dbContext, ulong userId, ulong emojiId, string emojiName)
        {
            Emoji emoji = dbContext.Emojis.FirstOrDefault(x => x.EmojiId == emojiId && x.Name == emojiName);

            if (emoji is null)
            {
                emoji = new Emoji()
                {
                    EmojiId = emojiId,
                    Name = emojiName,
                    CreateDate = DateTime.UtcNow,
                    CreateUserId = userId
                };

                dbContext.Emojis.Add(emoji);
                await dbContext.SaveChangesAsync();
            }

            return emoji;
        }
    }
}
