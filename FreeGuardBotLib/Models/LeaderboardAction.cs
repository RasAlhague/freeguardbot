﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models;

public class LeaderboardAction
{
    public int Id { get; set; }
    public ulong AffectedUserId { get; set; }
    public int GuildId { get; set; }
    public string Reason { get; set; }
    public long Points { get; set; }
    public ulong CreateUserId { get; set; }
    public DateTime CreateDate { get; set; }

    public Guild Guild { get; set; }
    public Discorduser AffectedUser { get; set; }
    public Discorduser CreateUser { get; set; }
}
