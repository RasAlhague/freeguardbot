﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models;

public class Rule
{
    [Key]
    [Required]
    public int Id { get; set; }
    [Required]
    public int RuleNumber { get; set; }
    [Required]
    [MaxLength(255)]
    public string Description { get; set; }
    [Required]
    public int GuildId { get; set; }
    [Required]
    public int CategoryId { get; set; }
    [Required]
    public ulong CreateUserId { get; set; }
    [Required]
    public DateTime CreateDate { get; set; }
    public ulong? ModifyUserId { get; set; }
    public DateTime? ModifyDate { get; set; }

    public virtual RuleCategory Category { get; set; }
    public virtual Guild Guild { get; set; }
    public virtual Discorduser CreateUser { get; set; }
    public virtual Discorduser ModifyUser { get; set; }
}
