﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class Vote
    {
        public int Id { get; set; }
        public ulong VoteeId { get; set; }
        public int PollResultId { get; set; }
        public int PollEmojiId { get; set; }

        public virtual Discorduser Votee { get; set; }
        public virtual PollResult PollResult { get; set; }
        public virtual PollEmoji PollEmoji { get; set; }
    }
}
