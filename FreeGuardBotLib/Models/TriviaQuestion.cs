﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class TriviaQuestion
    {
        public TriviaQuestion()
        {
            TriviaAnswers = new HashSet<TriviaAnswer>();
        }

        public int Id { get; set; }
        public string Question { get; set; }
        public int DifficultyLevelId { get; set; }
        public int AnswerTime { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual DifficultyLevel DifficultyLevel { get; set; }
        public virtual ICollection<TriviaAnswer> TriviaAnswers { get; set; }
    }
}
