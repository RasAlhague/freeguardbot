﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class AvailableAnswer
    {
        public AvailableAnswer()
        {
            QuizParticipationAnswers = new HashSet<QuizParticipationAnswer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int QuestionId { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong? ModifyUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int GuildId { get; set; }
        public bool Correct { get; set; }
        public int Points { get; set; }

        public virtual Question Question { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual ICollection<QuizParticipationAnswer> QuizParticipationAnswers { get; set; }
    }
}
