﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreeGuardBotLib.Models
{
    public partial class Commandinfo
    {
        public int Id { get; set; }
        public int GuildId { get; set; }
        public string CommandName { get; set; }
        [MaxLength(2048)]
        public string Parameter { get; set; }
        public ulong? CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual Discorduser CreateUser { get; set; }
    }
}
