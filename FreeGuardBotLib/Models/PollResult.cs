﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class PollResult
    {
        public PollResult()
        {
            Votes = new HashSet<Vote>();
        }

        public int Id { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int GuildId { get; set; }
        public int PollId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Poll Poll { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}
