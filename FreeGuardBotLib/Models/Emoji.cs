﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Emoji
    {
        public Emoji()
        {
            ReactionEmojis = new HashSet<ReactionEmoji>();
            PollEmojis = new HashSet<PollEmoji>();
            RoleSelectReactions = new HashSet<RoleSelectReaction>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ulong EmojiId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong? CreateUserId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual ICollection<ReactionEmoji> ReactionEmojis { get; set; }
        public virtual ICollection<PollEmoji> PollEmojis { get; set; }
        public virtual ICollection<RoleSelectReaction> RoleSelectReactions { get; set; }
    }
}
