﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models;

public partial class Guild
{
    public Guild()
    {
        Blacklistentries = new HashSet<Blacklistentry>();
        Commandinfos = new HashSet<Commandinfo>();
        ErrorLogs = new HashSet<ErrorLog>();
        Guildusers = new HashSet<Guilduser>();
        Recruits = new HashSet<Recruit>();
        Roles = new HashSet<Role>();
        Vacations = new HashSet<Vacation>();
        MessageReactions = new HashSet<MessageReaction>();
        ETAs = new HashSet<ETA>();
        Quizzes = new HashSet<Quiz>();
        Questions = new HashSet<Question>();
        AvailableAnswers = new HashSet<AvailableAnswer>();
        Polls = new HashSet<Poll>();
        PollResults = new HashSet<PollResult>();
        MessageActivityLogEntries = new HashSet<MessageActivityLogEntry>();
        Tickets = new HashSet<Ticket>();
        Leaderboards = new HashSet<Leaderboard>();
        LeaderboardActions = new HashSet<LeaderboardAction>();
        Rules = new HashSet<Rule>();
        RuleCategories = new HashSet<RuleCategory>();
        RuleChannels = new HashSet<RuleChannel>();
        Infractions = new HashSet<Infraction>();
    }

    public int Id { get; set; }
    public ulong GuildId { get; set; }
    public DateTime CreateDate { get; set; }
    public string InitialName { get; set; }
    public ulong? RecruitmentChannelId { get; set; }
    public ulong? WelcomeMessageChannel { get; set; }
    public ulong? TimeoutLoggingChannel { get; set; }
    public ulong? LeaderboardLoggingChannel { get; set; }
    public ulong? GeneralCommandLoggingChannel { get; set; }
    public ulong? InfractionLoggingChannel { get; set; }
    public string WelcomeMessage { get; set; }

    public virtual ICollection<Blacklistentry> Blacklistentries { get; set; }
    public virtual ICollection<Commandinfo> Commandinfos { get; set; }
    public virtual ICollection<ErrorLog> ErrorLogs { get; set; }
    public virtual ICollection<Guilduser> Guildusers { get; set; }
    public virtual ICollection<Recruit> Recruits { get; set; }
    public virtual ICollection<Role> Roles { get; set; }
    public virtual ICollection<Vacation> Vacations { get; set; }
    public virtual ICollection<MessageReaction> MessageReactions { get; set; }
    public virtual ICollection<ETA> ETAs { get; set; }
    public virtual ICollection<Quiz> Quizzes { get; set; }
    public virtual ICollection<Question> Questions { get; set; }
    public virtual ICollection<AvailableAnswer> AvailableAnswers { get; set; }
    public virtual ICollection<Poll> Polls { get; set; }
    public virtual ICollection<PollResult> PollResults { get; set; }
    public virtual ICollection<MessageActivityLogEntry> MessageActivityLogEntries { get; set; }
    public virtual ICollection<Ticket> Tickets { get; set; }
    public virtual ICollection<Leaderboard> Leaderboards { get; set; }
    public virtual ICollection<LeaderboardAction> LeaderboardActions { get; set; }
    public virtual ICollection<Rule> Rules { get; set; }
    public virtual ICollection<RuleCategory> RuleCategories { get; set; }
    public virtual ICollection<RuleChannel> RuleChannels { get; set; }
    public virtual ICollection<Infraction> Infractions { get; set; }
}
