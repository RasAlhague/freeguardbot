﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Vacation
    {
        public int Id { get; set; }
        public ulong AffectedId { get; set; }
        public int FreezeTitleId { get; set; }
        public int GuildId { get; set; }
        public DateTime? StartDate { get; set; }
        public int? EstimatedDuration { get; set; }
        public string Reason { get; set; }
        public string Finished { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Discorduser Affected { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Role FreezeTitle { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
    }
}
