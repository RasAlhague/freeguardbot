﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class RoleSelectReaction
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public int EmojiId { get; set; }
        public int RoleId { get; set; }
        public int RoleSelectId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual Emoji Emoji { get; set; }
        public virtual Role Role { get; set; }
        public virtual RoleSelect RoleSelect { get; set; }

    }
}
