﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class MessageReaction
    {
        public MessageReaction()
        {
            ReactionEmojis = new HashSet<ReactionEmoji>();
        }

        public int Id { get; set; }
        public string Keyword { get; set; }
        public int GuildId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual ICollection<ReactionEmoji> ReactionEmojis { get; set; }
    }
}
