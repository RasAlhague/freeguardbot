﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class QuizQuestion
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public int QuestionId { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Quiz Quiz { get; set; }
        public virtual Question Question { get; set; }
        public virtual Discorduser CreateUser { get; set; }
    }
}
