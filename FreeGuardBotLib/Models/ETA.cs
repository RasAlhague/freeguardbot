﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class ETA
    {
        public int Id { get; set; }
        public int GuildId { get; set; }
        public string Titel { get; set; }
        public string Description { get; set; }
        public DateTime ETADate { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual Discorduser CreateUser { get; set; }
    }
}
