﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class MessageActivityLogEntry
    {
        public int Id { get; set; }
        public ulong WriterId { get; set; }
        public int GuildId { get; set; }
        public DateTime WriteDate { get; set; }

        public virtual Discorduser Writer { get; set; }
        public virtual Guild Guild { get; set; }
    }
}
