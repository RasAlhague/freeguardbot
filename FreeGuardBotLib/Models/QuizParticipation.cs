﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class QuizParticipation
    {
        public QuizParticipation()
        {
            QuizParticipationAnswers = new HashSet<QuizParticipationAnswer>(); 
        }

        public int Id { get; set; }
        public int QuizId { get; set; }
        public ulong ParticipantId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Discorduser Participant { get; set; }
        public virtual ICollection<QuizParticipationAnswer> QuizParticipationAnswers { get; set; }
    }
}
