﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public int GuildId { get; set; }
        public int Messages { get; set; }
        public string ArchivedMessages { get; set; }
        public ulong TicketChannelId { get; set; }
        public bool IsArchived { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual Guild Guild { get; set; }
    }
}
