﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models;

public class RuleCategory
{
    public RuleCategory()
    {
        Rules = new HashSet<Rule>();
    }
    
    [Key]
    [Required]
    public int Id { get; set; }
    [Required, MaxLength(100)]
    public string Name { get; set; }
    [Required, MaxLength(100)]
    public string Description { get; set; }
    [Required]
    public int GuildId { get; set; }
    [Required]
    public int RuleChannelId { get; set; }
    [Required]
    public ulong CreateUserId { get; set; }
    [Required]
    public DateTime CreateDate { get; set; }
    public ulong? ModifyUserId { get; set; }
    public DateTime? ModifyDate { get; set; }

    public virtual ICollection<Rule> Rules { get; set; }
    public virtual Discorduser CreateUser { get; set; }
    public virtual Discorduser ModifyUser { get; set; }
    public virtual Guild Guild { get; set; }
    public virtual RuleChannel RuleChannel { get; set; }
}
