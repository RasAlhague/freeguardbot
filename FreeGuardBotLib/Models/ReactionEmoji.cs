﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class ReactionEmoji
    {
        public int Id { get; set; }
        public int MessageReactionId { get; set; }
        public int EmojiId { get; set; }

        public virtual MessageReaction MessageReaction { get; set; }
        public virtual Emoji Emoji { get; set; }
    }
}
