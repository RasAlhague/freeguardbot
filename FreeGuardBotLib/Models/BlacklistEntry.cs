﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models
{
    public partial class Blacklistentry
    {
        public Blacklistentry()
        {
            Aliases = new HashSet<Alias>();
            Confirmations = new HashSet<Confirmation>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string SteamId { get; set; }
        public string PlayfabId { get; set; }
        public string Reason { get; set; }
        public int GuildId { get; set; }
        public DateTime CreateDate { get; set; }
        public ulong CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public ulong? ModifyUserId { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual ICollection<Alias> Aliases { get; set; }
        public virtual ICollection<Confirmation> Confirmations { get; set; }
    }
}
