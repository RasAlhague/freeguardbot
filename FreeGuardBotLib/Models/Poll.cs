﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class Poll
    {
        public Poll()
        {
            PollEmojis = new HashSet<PollEmoji>();
            PollResults = new HashSet<PollResult>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ulong CreateUserId { get; set; }
        public ulong? ModifyUserId { get; set; }
        public int GuildId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }

        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual Guild Guild { get; set; }
        public virtual ICollection<PollEmoji> PollEmojis { get; set; }
        public virtual ICollection<PollResult> PollResults { get; set; }

    }
}
