﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class DifficultyLevel
    {
        public DifficultyLevel()
        {
            TriviaQuestions = new HashSet<TriviaQuestion>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TriviaQuestion> TriviaQuestions { get; set; }
    }
}
