﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models;

public partial class Leaderboard
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int GuildId { get; set; }
    public ulong DiscordChannelId { get; set; }
    public ulong MessageId { get; set; }
    public OrderDirection? OrderDirection { get; set; }
    public OrderField? OrderField { get; set; }
    public int? Limit { get; set; }
    public long? MaxAge { get; set; }
    public int? MaxMonthsInPast { get; set; }
    public ulong CreateUserId { get; set; }
    public DateTime CreateDate { get; set; }
    public ulong? ModifyUserId { get; set; }
    public DateTime? ModifyDate { get; set; }

    public virtual Discorduser CreateUser { get; set; }
    public virtual Discorduser ModifyUser { get; set; }
    public virtual Guild Guild { get; set; }
}
