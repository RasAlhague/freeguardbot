﻿using System;
using System.Collections.Generic;

namespace FreeGuardBotLib.Models;

public partial class Discorduser
{
    public Discorduser()
    {
        AliasCreateUsers = new HashSet<Alias>();
        AliasModifyUsers = new HashSet<Alias>();
        BlacklistentryCreateUsers = new HashSet<Blacklistentry>();
        BlacklistentryModifyUsers = new HashSet<Blacklistentry>();
        ConfirmationConfirmUsers = new HashSet<Confirmation>();
        ConfirmationCreateUsers = new HashSet<Confirmation>();
        ConfirmationModifyUsers = new HashSet<Confirmation>();
        Guildusers = new HashSet<Guilduser>();
        RecruitCreateUsers = new HashSet<Recruit>();
        RecruitModifyUsers = new HashSet<Recruit>();
        RecruitRecruitNavigations = new HashSet<Recruit>();
        RecruitRecruiters = new HashSet<Recruit>();
        RoleCreateUsers = new HashSet<Role>();
        RoleModifyUsers = new HashSet<Role>();
        RolerightCreateUsers = new HashSet<Roleright>();
        RolerightModifyUsers = new HashSet<Roleright>();
        VacationAffecteds = new HashSet<Vacation>();
        VacationCreateUsers = new HashSet<Vacation>();
        VacationModifyUsers = new HashSet<Vacation>();
        AvailableAnswerCreateUsers = new HashSet<AvailableAnswer>();
        AvailableAnswerModifyUsers = new HashSet<AvailableAnswer>();
        QuestionCreateUsers = new HashSet<Question>();
        QuestionModifyUsers = new HashSet<Question>();
        QuizCreateUsers = new HashSet<Quiz>();
        QuizModifyUsers = new HashSet<Quiz>();
        QuizParticipations = new HashSet<QuizParticipation>();
        QuizQuestions = new HashSet<QuizQuestion>();
        PollCreateUsers = new HashSet<Poll>();
        PollModifyUsers = new HashSet<Poll>();
        Votes = new HashSet<Vote>();
        PollResults = new HashSet<PollResult>();
        MessageActivityLogEntries = new HashSet<MessageActivityLogEntry>();
        TicketCreateUsers = new HashSet<Ticket>();
        TicketModifyUsers = new HashSet<Ticket>();
        RoleSelectCreateUsers = new HashSet<RoleSelect>();
        RoleSelectModifyUsers = new HashSet<RoleSelect>();
        RoleSelectReactionCreateUsers = new HashSet<RoleSelectReaction>();
        RoleSelectReactionModifyUsers = new HashSet<RoleSelectReaction>();
        TriviaQuestionCreateUsers = new HashSet<TriviaQuestion>();
        LeaderboardCreateUsers = new HashSet<Leaderboard>();
        LeaderboardModifyUsers = new HashSet<Leaderboard>();
        LeaderboardActionCreateUsers = new HashSet<LeaderboardAction>();
        LeaderboardActionAffectedUsers = new HashSet<LeaderboardAction>();
        CommandInfoCreateUsers = new HashSet<Commandinfo>();
        RuleCreateUsers = new HashSet<Rule>();
        RuleModifyUsers = new HashSet<Rule>();
        RuleCategoryCreateUsers = new HashSet<RuleCategory>();
        RuleCategoryModifyUsers = new HashSet<RuleCategory>();
        RuleChannelCreateUsers = new HashSet<RuleChannel>();
        RuleChannelModifyUsers = new HashSet<RuleChannel>();
        InfractionPunished = new HashSet<Infraction>();
        InfractionPunisher = new HashSet<Infraction>();
    }

    public ulong DiscordUserId { get; set; }
    public DateTime CreateDate { get; set; }
    public string SteamId { get; set; }

    public virtual ICollection<Alias> AliasCreateUsers { get; set; }
    public virtual ICollection<Alias> AliasModifyUsers { get; set; }
    public virtual ICollection<Blacklistentry> BlacklistentryCreateUsers { get; set; }
    public virtual ICollection<Blacklistentry> BlacklistentryModifyUsers { get; set; }
    public virtual ICollection<Confirmation> ConfirmationConfirmUsers { get; set; }
    public virtual ICollection<Confirmation> ConfirmationCreateUsers { get; set; }
    public virtual ICollection<Confirmation> ConfirmationModifyUsers { get; set; }
    public virtual ICollection<Guilduser> Guildusers { get; set; }
    public virtual ICollection<Recruit> RecruitCreateUsers { get; set; }
    public virtual ICollection<Recruit> RecruitModifyUsers { get; set; }
    public virtual ICollection<Recruit> RecruitRecruitNavigations { get; set; }
    public virtual ICollection<Recruit> RecruitRecruiters { get; set; }
    public virtual ICollection<Role> RoleCreateUsers { get; set; }
    public virtual ICollection<Role> RoleModifyUsers { get; set; }
    public virtual ICollection<Roleright> RolerightCreateUsers { get; set; }
    public virtual ICollection<Roleright> RolerightModifyUsers { get; set; }
    public virtual ICollection<Vacation> VacationAffecteds { get; set; }
    public virtual ICollection<Vacation> VacationCreateUsers { get; set; }
    public virtual ICollection<Vacation> VacationModifyUsers { get; set; }
    public virtual ICollection<AvailableAnswer> AvailableAnswerCreateUsers { get; set; }
    public virtual ICollection<AvailableAnswer> AvailableAnswerModifyUsers { get; set; }
    public virtual ICollection<Question> QuestionCreateUsers { get; set; }
    public virtual ICollection<Question> QuestionModifyUsers { get; set; }
    public virtual ICollection<Quiz> QuizCreateUsers { get; set; }
    public virtual ICollection<Quiz> QuizModifyUsers { get; set; }
    public virtual ICollection<QuizParticipation> QuizParticipations { get; set; }
    public virtual ICollection<QuizQuestion> QuizQuestions { get; set; }
    public virtual ICollection<Poll> PollCreateUsers { get; set; }
    public virtual ICollection<Poll> PollModifyUsers { get; set; }
    public virtual ICollection<Vote> Votes { get; set; }
    public virtual ICollection<PollResult> PollResults { get; set; }
    public virtual ICollection<MessageActivityLogEntry> MessageActivityLogEntries { get; set; }
    public virtual ICollection<Ticket> TicketCreateUsers { get; set; }
    public virtual ICollection<Ticket> TicketModifyUsers { get; set; }
    public virtual ICollection<RoleSelect> RoleSelectCreateUsers { get; set; }
    public virtual ICollection<RoleSelect> RoleSelectModifyUsers { get; set; }
    public virtual ICollection<RoleSelectReaction> RoleSelectReactionCreateUsers { get; set; }
    public virtual ICollection<RoleSelectReaction> RoleSelectReactionModifyUsers { get; set; }
    public virtual ICollection<TriviaQuestion> TriviaQuestionCreateUsers { get; set; }
    public virtual ICollection<Leaderboard> LeaderboardCreateUsers { get; set; }
    public virtual ICollection<Leaderboard> LeaderboardModifyUsers { get; set; }
    public virtual ICollection<LeaderboardAction> LeaderboardActionCreateUsers { get; set; }
    public virtual ICollection<LeaderboardAction> LeaderboardActionAffectedUsers { get; set; }
    public virtual ICollection<Commandinfo> CommandInfoCreateUsers { get; set; }
    public virtual ICollection<Rule> RuleCreateUsers { get; set; }
    public virtual ICollection<Rule> RuleModifyUsers { get; set; }
    public virtual ICollection<RuleChannel> RuleChannelCreateUsers { get; set; }
    public virtual ICollection<RuleChannel> RuleChannelModifyUsers { get; set; }
    public virtual ICollection<RuleCategory> RuleCategoryCreateUsers { get; set; }
    public virtual ICollection<RuleCategory> RuleCategoryModifyUsers { get; set; }
    public virtual ICollection<Infraction> InfractionPunished { get; set; }
    public virtual ICollection<Infraction> InfractionPunisher { get; set; }
}
