﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class QuizParticipationAnswer
    {
        public int Id { get; set; }
        public int QuizParticipationId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public DateTime CreateDate { get; set; }
        
        public virtual QuizParticipation QuizParticipation { get; set; }
        public virtual Question Question { get; set; }
        public virtual AvailableAnswer Answer { get; set; }
    }
}
