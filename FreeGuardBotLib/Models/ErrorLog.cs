﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public partial class ErrorLog
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string OriginCommand { get; set; }
        public string Source { get; set; }
        public int GuildId { get; set; }

        public virtual Guild Guild { get; set; }
    }
}
