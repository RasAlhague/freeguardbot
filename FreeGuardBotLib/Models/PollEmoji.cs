﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class PollEmoji
    {
        public PollEmoji()
        {
            Votes = new HashSet<Vote>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public int PollId { get; set; }
        public int EmojiId { get; set; }

        public virtual Poll Poll { get; set; }
        public virtual Emoji Emoji { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}
