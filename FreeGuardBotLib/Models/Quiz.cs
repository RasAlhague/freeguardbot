﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class Quiz
    {
        public Quiz()
        {
            QuizQuestions = new HashSet<QuizQuestion>();
            QuizParticipations = new HashSet<QuizParticipation>();
        }

        public int Id { get; set; }
        public int GuildId { get; set; }
        public ulong CreateUserId { get; set; }
        public ulong? ModifyUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? ActiveUntil { get; set; }
        public DateTime? ActivFrom { get; set; }
        public int? Retries { get; set; }
        public int? Duration { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual ICollection<QuizQuestion> QuizQuestions { get; set; }
        public virtual ICollection<QuizParticipation> QuizParticipations { get; set; }
    }
}
