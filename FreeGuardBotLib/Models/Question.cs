﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models
{
    public class Question
    {
        public Question()
        {
            AvailableAnswers = new HashSet<AvailableAnswer>();
        }

        public int Id { get; set; }
        public int? QuestionTypeId { get; set; }
        public string Text { get; set; }
        public ulong CreateUserId { get; set; }
        public ulong? ModifyUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int GuildId { get; set; }

        public virtual Guild Guild { get; set; }
        public virtual QuestionType QuestionType { get; set; }
        public virtual Discorduser CreateUser { get; set; }
        public virtual Discorduser ModifyUser { get; set; }
        public virtual ICollection<AvailableAnswer> AvailableAnswers { get; set; }
        public virtual ICollection<QuizQuestion> QuizQuestions { get; set; }
        public virtual ICollection<QuizParticipationAnswer> QuizParticipationAnswers { get; set; }
    }
}
