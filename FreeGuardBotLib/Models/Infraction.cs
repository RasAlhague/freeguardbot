﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Models;

public class Infraction
{
    public enum PunishmentType
    {
        Warn,
        Kick,
        Ban,
        Timeout,
        PointRevoke
    }

    [Key]
    [Required]
    public int Id { get; set; }
    [Required]
    public int GuildId { get; set; }
    [Required]
    public ulong PunisherId { get; set; }
    [Required]
    public ulong PunishedId { get; set; }
    [Required, MaxLength(255)]
    public string Reason { get; set; }
    [Required]
    public DateTime CreateDate { get; set; }
    [Required]
    public PunishmentType Punishment { get; set; }

    public virtual Guild Guild { get; set; }
    public virtual Discorduser Punished { get; set; }
    public virtual Discorduser Punisher { get; set; }
}
