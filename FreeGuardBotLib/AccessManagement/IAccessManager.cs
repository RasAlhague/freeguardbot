﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.AccessManagement
{
    public interface IAccessManager
    {
        Task<bool> CheckUserAccessAsync(CommandContext cmdContext, Rights botRight);
        Task<bool> CheckUserAccessAsync(InteractionContext ctx, Rights botRight);
        Task<bool> CheckUserAccessAsync(ContextMenuContext ctx, Rights botRight);
        Task<bool> CheckUserAccessAsync(CommandContext cmdContext, IEnumerable<Rights> botRights);
        Task<bool> CheckUserAccessAsync(InteractionContext ctx, IEnumerable<Rights> botRights);
        Task<bool> CheckUserAccessAsync(ContextMenuContext ctx, IEnumerable<Rights> botRights);
    }
}
