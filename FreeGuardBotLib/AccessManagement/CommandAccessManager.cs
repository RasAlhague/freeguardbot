﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeGuardBotLib.AccessManagement
{
    public class CommandAccessManager : IAccessManager
    {
        public async Task<bool> CheckUserAccessAsync(CommandContext cmdContext, Rights botRight)
        {
            return await CheckUserAccessAsync(cmdContext, new List<Rights>() { botRight });
        }

        /// <summary>
        /// Checks wether a user is allowed to access a specific command.
        /// </summary>
        /// <param name="cmdContext">The discord command context.</param>
        /// <param name="botRights">The rights which are required for this command.</param>
        /// <returns>Returns true if rights are sufficient and false if they are insufficient or an exception happens during execution.</returns>
        public async Task<bool> CheckUserAccessAsync(CommandContext cmdContext, IEnumerable<Rights> botRights)
        {
            return await CheckUserAccessAsync(cmdContext.Member, cmdContext.Guild, cmdContext.Client.Logger, botRights);
        }

        /// <summary>
        /// Checks wether a user is allowed to access a specific command.
        /// </summary>
        /// <param name="cmdContext">The discord command context.</param>
        /// <param name="botRights">The rights which are required for this command.</param>
        /// <returns>Returns true if rights are sufficient and false if they are insufficient or an exception happens during execution.</returns>
        public async Task<bool> CheckUserAccessAsync(InteractionContext ctx, IEnumerable<Rights> botRights)
        {
            return await CheckUserAccessAsync(ctx.Member, ctx.Guild, ctx.Client.Logger, botRights);
        }

        public async Task<bool> CheckUserAccessAsync(InteractionContext ctx, Rights botRight)
        {
            return await CheckUserAccessAsync(ctx, new List<Rights>() { botRight });
        }

        public async Task<bool> CheckUserAccessAsync(ContextMenuContext ctx, Rights botRight)
        {
            return await CheckUserAccessAsync(ctx, new List<Rights>() { botRight });
        }

        public async Task<bool> CheckUserAccessAsync(ContextMenuContext ctx, IEnumerable<Rights> botRights)
        {
            return await CheckUserAccessAsync(ctx.Member, ctx.Guild, ctx.Client.Logger, botRights);
        }

        private async Task<bool> CheckUserAccessAsync(DiscordMember member, DiscordGuild dsGuild, ILogger<BaseDiscordClient> logger, IEnumerable<Rights> botRights)
        {
            try
            {
                using FreeGuardDbContext context = new();

                var discordGuildId = dsGuild.Id;
                var userId = member.Id;

                Guild guild = await Guild.GetOrCreateGuildAsync(context, discordGuildId, dsGuild.Name).ConfigureAwait(false);
                Discorduser user = await Discorduser.GetOrCreateDiscorduserAsync(context, userId).ConfigureAwait(false);

                await Guilduser.GetOrCreateGuildUserAsync(context, guild.Id, user.DiscordUserId).ConfigureAwait(false);

                var userRoles = member.Roles;
                List<Role> allowedRoles = await GetAllowedRoles(botRights, context, guild);

                foreach (var userRole in userRoles)
                {
                    if (allowedRoles.Any(x => x.DiscordRoleId == userRole.Id))
                    {
                        return true;
                    }
                }

                if (member.Roles.Any(x => x.CheckPermission(DSharpPlus.Permissions.Administrator) == DSharpPlus.PermissionLevel.Allowed) || member.IsOwner)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An exception occured while trying to check the command access rights.");

                return false;
            }
        }

        private async Task<List<Role>> GetAllowedRoles(IEnumerable<Rights> botRights, FreeGuardDbContext context, Guild guild)
        {
            var allowedRoles = new List<Role>();

            foreach (var botRight in botRights)
            {
                var roles = await context.Botrights
                    .Join(
                        context.Rolerights,
                        r => r.Id,
                        rr => rr.BotRightId,
                        (r, rr) => new
                        {
                            RightId = r.Id,
                            rr.RoleId
                        }
                    )
                    .Join(
                        context.Roles,
                        rr => rr.RoleId,
                        r => r.Id,
                        (rr, r) => new
                        {
                            r.GuildId,
                            rr.RightId,
                            r.DiscordRoleId,
                            r.CreateDate,
                            r.CreateUserId,
                            r.ModifyDate,
                            r.ModifyUserId,
                            r.Id
                        }
                    )
                    .Where(x => x.GuildId == guild.Id && x.RightId == (int)botRight)
                    .Select(x => new Role() { 
                        DiscordRoleId = x.DiscordRoleId,
                        Id = x.Id,
                        CreateDate = x.CreateDate,
                        CreateUserId = x.CreateUserId,
                        ModifyDate = x.ModifyDate,
                        ModifyUserId = x.ModifyUserId,
                        GuildId = x.GuildId
                    })
                    .ToListAsync();

                allowedRoles.AddRange(roles);
            }

            return allowedRoles;
        }
    }
}
