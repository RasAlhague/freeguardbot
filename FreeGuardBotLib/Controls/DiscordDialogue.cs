﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using FreeGuardBotLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeGuardBotLib.Controls
{
    public class DiscordDialogue
    {
        public DiscordChannel DiscordChannel { get; private set; }
        public DiscordDmChannel DiscordDmChannel { get; private set; }

        public DiscordDialogue()
        {
        }

        public async Task InitializeAsync(CommandContext ctx)
        {
            DiscordChannel = ctx.Channel;
            DiscordDmChannel = await ctx.Member.CreateDmChannelAsync();
        }

        public async Task InitializeAsync(InteractionContext ctx)
        {
            DiscordChannel = ctx.Channel;
            DiscordDmChannel = await ctx.Member.CreateDmChannelAsync();
        }

        public void Initialize(DiscordChannel channel, DiscordDmChannel discordDmChannel)
        {
            DiscordChannel = channel;
            DiscordDmChannel = discordDmChannel;
        }

        public async Task<T> SendDialogueAsync<T>(string message, int timeout = 300) where T : IConvertible
        {
            await DiscordChannel.SendMessageAsync(message);
            var dialogueResult = await DiscordChannel.GetNextMessageAsync(x => !x.Author.IsBot, TimeSpan.FromSeconds(timeout));

            if (dialogueResult.TimedOut)
            {
                await DiscordChannel.SendMessageAsync("Input timeout reached!");
                return default(T);
            }

            return (T)Convert.ChangeType(dialogueResult.Result.Content, typeof(T));
        }

        public async Task<T> SendDialogueAsync<T>(string message, Func<T, bool> valueValidator, int tries = 3, int timeout = 300)
        {
            var dialogueResult = default(T);


            for (int i = 0; i < tries; i++)
            {
                await DiscordChannel.SendMessageAsync(message);
                var resultMessage = await DiscordChannel.GetNextMessageAsync(x => !x.Author.IsBot, TimeSpan.FromSeconds(timeout));

                if (resultMessage.TimedOut)
                {
                    await DiscordChannel.SendMessageAsync("Input timeout reached!");
                    return default(T);
                }

                dialogueResult = (T)Convert.ChangeType(resultMessage.Result.Content, typeof(T));

                if (valueValidator(dialogueResult))
                {
                    return dialogueResult;
                }
                else
                {
                    await DiscordChannel.SendMessageAsync($"Invalid input! Number of left tries: {tries - i - 1}.");
                }
            }

            return default(T);
        }

        public async Task<T> SendDmDialogueAsync<T>(string message, int timeout = 300) where T : IConvertible
        {
            await DiscordDmChannel.SendMessageAsync(message);
            var dialogueResult = await DiscordDmChannel.GetNextMessageAsync(x => !x.Author.IsBot, TimeSpan.FromSeconds(timeout));

            if (dialogueResult.TimedOut)
            {
                await DiscordDmChannel.SendMessageAsync("Input timeout reached!");
                return default(T);
            }

            return (T)Convert.ChangeType(dialogueResult.Result.Content, typeof(T));
        }

        public async Task<T> SendDmDialogueAsync<T>(string message, Func<T, bool> valueValidator, int tries = 3, int timeout = 300)
        {
            var dialogueResult = default(T);


            for (int i = 0; i < tries; i++)
            {
                await DiscordDmChannel.SendMessageAsync(message);
                var resultMessage = await DiscordDmChannel.GetNextMessageAsync(x => !x.Author.IsBot, TimeSpan.FromSeconds(timeout));

                if (resultMessage.TimedOut)
                {
                    await DiscordDmChannel.SendMessageAsync("Input timeout reached!");
                    return default(T);
                }

                dialogueResult = (T)Convert.ChangeType(resultMessage.Result.Content, typeof(T));

                if (valueValidator(dialogueResult))
                {
                    return dialogueResult;
                }
                else
                {
                    await DiscordDmChannel.SendMessageAsync($"Invalid input! Number of left tries: {tries - i - 1}.");
                }
            }

            return default(T);
        }
    }
}
